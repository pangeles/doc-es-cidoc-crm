> ### subclass
>A subclass is a class that is a specialization of another class (its superclass). Specialization or the IsA relationship means that: 
>    1 all instances of the subclass are also instances of its superclass, 
>    2 the intension of the subclass extends the intension of its superclass, i.e., its traits are more restrictive than that of its superclass and 
>    3 the subclass inherits the definition of all of the properties declared for its superclass without exceptions (strict inheritance), in addition to having none, one or more properties of its own. 
>A subclass can have more than one immediate superclass and consequently inherits the properties of all of its superclasses (multiple inheritance). The IsA relationship or specialization between two or more classes gives rise to a structure known as a class hierarchy. The IsA relationship is transitive and may not be cyclic. In some contexts (e.g., the programming language C++) the term derived class is used synonymously with subclass. 
>For example:
>Every Person IsA Biological Object, or Person is a subclass of Biological Object. 
>Also, every Person IsA Actor. A Person may die. However, other kinds of Actors, such as companies, don’t die (c.f. 2). 
>Every Biological Object IsA Physical Object. A Physical Object can be moved. Hence, a Person can be moved also (c.f. 3).

### subclase (*subclass*)

Una subclase es una **clase** que es una especialización de otra **clase** (su **superclase**).
La especialización o la relación *IsA* (es un/es una) significa que:

1. todas las **instancias** de la subclase también son instancias de su superclase,
2. la **intensión** de la subclase extiende la intensión de su superclase; es decir, sus rasgos son más restrictivos que los de su superclase, y
3. la subclase hereda la definición de todas las **propiedades** declaradas para su superclase sin excepciones (**herencia estricta**), además de no tener ninguna, una o más propiedades propias.

Una subclase puede tener más de una superclase inmediata y, en consecuencia, hereda las propiedades de todas sus superclases (**herencia múltiple**).
La relación o especialización *IsA* entre dos o más clases da lugar a una estructura conocida como jerarquía de clases.
La relación IsA es transitiva y puede no ser cíclica. En algunos contextos (por ejemplo, “toda Persona tiene dos progenitores” ~~el lenguaje de programación C \++~~), el término “clase derivada” se usa como sinónimo de subclase.

Por ejemplo:

Toda Persona IsA (es un) Objeto Biológico, o bien, Persona es una subclase de Objeto Biológico.

```mermaid
flowchart LR
    id2([Persona]) -- IsA (es un) --> id1([Objeto_Biológico])
```
También se puede representar de esta forma:

```mermaid
 stateDiagram-v2
    direction TB
    bio_object: Objeto_Biológico [clase]
    state bio_object {
        [*] --> s
        s: "Persona [subclase]"
    }
```

Además, cada Persona IsA (es un) Actor. Una Persona puede morir. Sin embargo, otros tipos de Actores, como las empresas, no mueren (cf.2).

```mermaid
flowchart LR
    id2([Persona]) -- IsA (es un) --> id1([Objeto_Biológico])
    id2([Persona]) -- IsA (es un) --> id3([Actor])
    id4([Empresa]) -- IsA (es un) --> id3([Actor])
```

Todo Objeto Biológico IsA (es un) Objeto Físico. Un Objeto Físico puede moverse. Por tanto, una Persona también puede moverse (cf. 3).

```mermaid
flowchart LR
    id2([Persona]) -- IsA (es un) --> id1([Objeto_Biológico])
    id2([Persona]) -- IsA (es un) --> id3([Actor])
    id4([Empresa]) -- IsA (es un) --> id3([Actor])
    id1([Objeto_Biológico]) -- IsA (es un) --> id5([Objeto Físico])
```



| | |
| :--- | :--- |
| *Notas del traductor* | Entendemos el CRM como un modelo de estructuración de datos. El párrafo en inglés hace referencia a un lenguaje de programación concreto, pero “clase” en programación no es exactamente igual que en estructuración de datos. Por tanto, nuestra propuesta para mejorarlo es discutir, modificar o eliminar el ejemplo, ya que la herencia múltiple es común en la estructuración de datos, pero no es ampliamente utilizada en el lenguaje de programación. Se sugiere que el ejemplo esté relacionado con la estructuración de datos, no con la programación. |
