> ### range
>The range is the class that comprises all potential values of a property. That means that instances of the property can link only to instances of its range class. A property must have exactly one range, although the range class may always contain instances that are not the value of the property. The range class is analogous to the grammatical object of a phrase for which the property is analogous to the verb. It is arbitrary which class is selected as domain and which as range, just as the choice between active and passive voice in grammar is arbitrary. Property names in the CIDOC CRM are designed to be semantically meaningful and grammatically correct when read from domain to range. In addition, the inverse property name, normally given in parentheses, is also designed to be semantically meaningful and grammatically correct when read from range to domain.

### rango *(range)*

El rango es la **clase** que comprende todos los valores potenciales de una **propiedad**.
Eso significa que las **instancias** de la propiedad sólo pueden vincularse a instancias de su clase de rango.
Una propiedad debe tener exactamente un rango, aunque la clase de rango siempre puede contener instancias que no son el valor de la propiedad.
La clase de rango es análoga al objeto gramatical de una frase cuya propiedad es análoga al verbo.
Es arbitrario qué clase se selecciona como **dominio** y cuál como rango, al igual que la elección entre las voces activa y pasiva es arbitraria en gramática.
Los nombres de propiedad en CIDOC CRM están diseñados para ser semánticamente significativos y gramaticalmente correctos cuando se leen de un dominio a otro.
Además, el nombre de la propiedad inversa, normalmente entre paréntesis, también está diseñado para ser semánticamente significativo y gramaticalmente correcto cuando se lee de un rango a otro.
