> ### scope note
> A scope note is a textual description of the intension of a class or property.
> Scope notes are not formal modelling constructs, but are provided to help explain the intended meaning and application of the CIDOC CRM’s classes and properties. Basically, they refer to a conceptualisation common to domain experts and disambiguate between different possible interpretations. Illustrative example instances of classes and properties are also regularly provided in the scope notes for explanatory purposes.

### nota de alcance *(scope note)*

Una nota de alcance es una descripción textual de la **intensión** de una **clase** o **propiedad**.
Las notas de alcance no son construcciones formales de modelado, se proporcionan para ayudar a explicar el significado deseado y la aplicación de las clases y propiedades de CIDOC CRM. Básicamente se refieren a una conceptualización común a los expertos en el dominio para desambiguar diferentes interpretaciones posibles.
Las **instancias** ilustrativas que ejemplifican clases y propiedades también se proporcionan regularmente en las notas de alcance con fines explicativos.
