> ### subproperty
>
>A subproperty is a property that is a specialization of another property (its superproperty). Specialization or IsA relationship means that: 
>    1 all instances of the subproperty are also instances of its superproperty, 
>    2 the intension of the subproperty extends the intension of the superproperty, i.e., its traits are more restrictive than that of its superproperty, 
>    3 the domain of the subproperty is the same as the domain of its superproperty or a subclass of that domain,
>    4 the range of the subproperty is the same as the range of its superproperty or a subclass of that range,
>    5 the subproperty inherits the definition of all of the properties declared for its superproperty without exceptions (strict inheritance), in addition to having none, one or more properties of its own.
>A subproperty can have more than one immediate superproperty and consequently inherits the properties of all of its superproperties (multiple inheritance). The IsA relationship or specialization between two or more properties gives rise to the structure we call a property hierarchy. The IsA relationship is transitive and may not be cyclic. 
>Some object-oriented programming languages, such as C++, do not contain constructs that allow for the expression of the specialization of properties as sub-properties.
>Alternatively, a property may be subproperty of the inverse of another property, i.e., reading the property from range to domain. In that case:
>    1 all instances of the subproperty are also instances of the inverse of the other property, 
>    2 the intension of the subproperty extends the intension of the inverse of the other property, i.e., its traits are more restrictive than that of the inverse of the other property, 
>    3 the domain of the subproperty is the same as the range of the other property or a subclass of that range,
>    4 the range of the subproperty is the same as the domain of the other property or a subclass of that domain,
>    5 the subproperty inherits the definition of all of the properties declared for the other property without exceptions (strict inheritance), in addition to having none, one or more properties of its own. The definitions of inherited properties have to be interpreted in the inverse sense of direction of the subproperty, i.e., from range to domain.

### subpropiedad *(subproperty)*

Una subpropiedad es una **propiedad**, que es una especialización de otra propiedad (su **superpropiedad**).
La especialización o relación IsA significa que:

1. todas las **instancias** de la subpropiedad son también instancias de su superpropiedad,
2. la **intensión** de la subpropiedad extiende la intensión de la superpropiedad; es decir, sus rasgos son más restrictivos que los de la superpropiedad,
3. el **dominio** de la subpropiedad es el mismo que el dominio de su superpropiedad o una **subclase** de ese dominio,
4. el **rango** de la subpropiedad es el mismo que el rango de su superpropiedad o una subclase de ese rango, y
5. la subpropiedad hereda la definición de todas las propiedades declaradas para su superpropiedad sin excepciones (**herencia estricta**), además de no tener ninguna, una o más propiedades propias.

Una subpropiedad puede tener más de una superpropiedad inmediata y, en consecuencia, hereda las propiedades de todas sus superpropiedades (**herencia múltiple**).
La relación IsA o especialización entre dos o más propiedades da lugar a la estructura que llamamos jerarquía de propiedades.
La relación IsA es transitiva y puede no ser cíclica.

Algunos lenguajes de programación orientados a objetos, como C \++, no contienen construcciones que permitan la expresión de la especialización de propiedades como subpropiedades.

Por otro lado, una propiedad puede ser subpropiedad de la **inversa de** otra propiedad; es decir, leer la propiedad de rango a dominio. En ese caso:

1. todas las instancias de la subpropiedad son también instancias de la inversa de la otra propiedad,
2. la intensión de la subpropiedad extiende la intensión de la inversa de la otra propiedad; es decir, sus rasgos son más restrictivos que los de la inversa de la otra propiedad,
3. el dominio de la subpropiedad es el mismo que el rango de la otra propiedad o una subclase de ese rango,
4. el rango de la subpropiedad es el mismo que el dominio de la otra propiedad o una subclase de ese dominio,
5. la subpropiedad hereda la definición de todas las propiedades declaradas para la otra propiedad sin excepciones (herencia estricta), además de no tener ninguna, una o más propiedades propias. Las definiciones de propiedades heredadas deben interpretarse en el sentido inverso de la dirección de la subpropiedad, es decir, de rango a dominio.

