> ### property quantifiers
>We use the term "property quantifiers" for the declaration of the allowed number of instances of a certain property that can refer to a particular instance of the range class or the domain class of that property. These declarations are ontological, i.e., they refer to the nature of the real world described and not to our current knowledge. For example, each person has exactly one father, but collected knowledge may refer to none, one or many.

### cuantificadores de propiedad *(property quantifiers)*

 Usamos el término “cuantificadores de propiedad” para la declaración del número permitido de **instancias** de una determinada **propiedad** que puede referirse a una instancia particular de la clase de **rango** o la clase de **dominio** de esa propiedad.
 Estas declaraciones son ontológicas; es decir, se refieren a la naturaleza del mundo real descrito y no a nuestro conocimiento actual.
 Por ejemplo, cada persona tiene exactamente un padre, pero el conocimiento acumulado puede referirse a ninguno, uno o muchos.


| | |
| :--- | :--- |
| *Notas del traductor* | La *cuantificación de propiedad* limita la cantidad de datos que puede almacenar/manejar una propiedad. por ejemplo: la propiedad «fecha de registro» sólo puede contener 1 valor, la fecha de inscripción de bien ej: 01-05-2024. En este ejemplo la cuantificación de propiedad \=1. En otros casos no puede definirse esta cuantificación, por ejemplo con la propiedad «Material» ha de contener todos los materiales que componen el bien: madera, bronce, algodón, etc. |
 |
