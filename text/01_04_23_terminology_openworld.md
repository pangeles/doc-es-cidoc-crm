> ### Open World
>The “Open World Assumption” is a term from knowledge base systems. It characterizes knowledge base systems that assume the information stored is incomplete relative to the universe of discourse they intend to describe. This incompleteness may be due to the inability of the maintainer to provide sufficient information or due to more fundamental problems of cognition in the system’s domain. Such problems are characteristic of cultural information systems. Our records about the past are necessarily incomplete. In addition, there may be items that cannot be clearly assigned to a given class. 
>In particular, absence of a certain property for an item described in the system does not mean that this item does not have this property. For example, if one item is described as Biological Object and another as Physical Object, this does not imply that the latter may not be a Biological Object as well. Therefore, complements of a class with respect to a superclass cannot be concluded in general from an information system using the Open World Assumption. For example, one cannot list “all Physical Objects known to the system that are not Biological Objects in the real world”, but one may of course list “all items known to the system as Physical Objects but that are not known to the system as Biological Objects”. 

### Mundo Abierto *(Open World)*

La “Suposición de Mundo Abierto” es un término de los sistemas de base de conocimiento.
Caracteriza a los sistemas de base de conocimiento que asumen que la información almacenada es incompleta en relación con el universo del discurso que pretenden describir.
Lo incompleto puede deberse a la incapacidad del encargado del mantenimiento para proporcionar información suficiente o bien, a causa de problemas más fundamentales de cognición en el dominio del sistema.
Estos problemas son característicos de los sistemas de información cultural.
Nuestros registros sobre el pasado son necesariamente incompletos.
Además, puede haber elementos que no se puedan asignar claramente a una **clase** determinada.
En particular, la ausencia de una determinada **propiedad** para un elemento descrito en el sistema no significa que este no tenga dicha propiedad.
Por ejemplo, si un elemento se describe como un Objeto Biológico y otro como un objeto físico, esto no implica que este último no pueda ser también un Objeto Biológico.
Por lo tanto, los **complementos** de una clase, respecto a una **superclase**, no se pueden concluir en general a partir de un sistema de información que utilice la Suposición de Mundo Abierto.
Por ejemplo, uno no puede enumerar “todos los Objetos Físicos conocidos por el sistema que no son Objetos Biológicos en el mundo real”, pero uno puede, por supuesto, enumerar “todos los elementos conocidos por el sistema como Objetos Físicos, pero que no son conocidos por el sistema como Objetos Biológicos”.
 