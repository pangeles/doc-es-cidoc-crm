> ### disjoint 
>Classes are disjoint if the intersection of their extensions is an empty set. In other words, they have no common instances in any possible world.

### disjuntos *(disjoint)*

 Las **clases son** disjuntas si la intersección de sus **extensiones** es un conjunto vacío.
 En otras palabras, no tienen **instancias** comunes en ningún mundo posible.
