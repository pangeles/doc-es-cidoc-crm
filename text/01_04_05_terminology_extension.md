> ### extension
>The extension of a class is the set of all real-life instances belonging to the class that fulfil the criteria of its intension. This set is “open” in the sense that it is generally beyond our capabilities to know all instances of a class in the world and indeed that the future may bring new instances about at any time (Open World). An information system may at any point in time refer to some instances of a class, which form a subset of its extension.

###  extensión (*extension*)

La extensión de una **clase** es el conjunto de todas las **instancias** de la vida real que pertenecen a la clase que cumple con los criterios de su **intensión**.
Este conjunto es “abierto” en el sentido, por lo general, está más allá de nuestras capacidades conocer todas las instancias de una clase en el mundo y, de hecho, que el futuro puede traer nuevas instancias en cualquier momento (**Mundo Abierto**).
Un sistema de información puede referirse, en cualquier momento, a algunas instancias de una clase, que forman un subconjunto de su extensión.


![extension de Persona](01_04_05_terminology_extension.svg)

extensión = 3