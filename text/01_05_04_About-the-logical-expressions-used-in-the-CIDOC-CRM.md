> #### About the logical expressions used in the CIDOC CRM
>The present CIDOC CRM specifications are annotated with logical axioms, providing an additional formal expression of the CIDOC CRM ontology. This section briefly introduces the assumptions that are at the basis of the logical expression of the CIDOC CRM (for a fully detailed account of the logical expression of semantic data modelling, see (Reiter,1984)).
>In terms of semantic data modelling, classes and properties are used to express ontological knowledge by means of various kinds of constraints, such as sub-class/sub-property links, e.g., E21 Person is a sub-class of E20 Biological Object, or domain/range constraints, e.g., the domain of P152 has parent is class E21 Person.
>In contrast, first-order logic-based knowledge representation relies on a language for formally encoding an ontology. This language can be directly put in correspondence with semantic data modelling in a straightforward way:
>    • classes are named by unary predicate symbols; conventionally, we use E21 as the unary predicate symbol corresponding to class E21 Person;
>    • properties are named by binary predicate symbols; conventionally, we use P152 as the binary predicate symbol corresponding to property P152 has parent.
>    • properties of properties, “.1 properties” are named by ternary predicate symbols; conventionally, we use P14.1 as the ternary predicate symbol corresponding to property P14.1 in the role of.
>Ontology is expressed in logic by means of logical axioms, which correspond to the constraints of semantic modelling. In the definition of classes and properties of the CIDOC CRM the axioms are placed under the heading ‘In first order logic’. There are several options for writing statements in first order logic. In this document we use a standard compact notation widely used in text books and scientific papers. The definition is given in the table below.
>Table 1: Symbolic Operators in First Order Logic Representation
>| Symbol | Name | reads | Truth value |
>|--------|------|-------|-------------|
>|Operators        |      |       |             |
>| ∧ | conjunction | and | (φ ∧ ψ) is true if and only if both φ and ψ are true|
>| ∨ | disjunction | or | (φ ∨ ψ) is true if and only if at least one of either φ or ψ is true |
>| ¬ | negation | not | ¬φ is true if and only if φ is false |
>| ⇒ | implication | implies, if … then ... | (φ ⇒ ψ) is true if and only if it is not the case that φ is true and ψ is false |
>| ⇔ | equivalence | is equivalent to, if … and only if … | φ ⇔ ψ is true if and only if both φ and ψ are true or both φ and ψ are false |
>| Quantifiers | |||
>| ∃ | existential quantifier | exists, there exists at least one |
>| ∀ | Universal quantifier | forall, for all ||

>For instance, the above sub-class link between E21 Person and E20 Biological Object can be formulated in first order logic as the axiom:
>(∀x) [E21(x) ⇒E20(x)]
>(reading: for all individuals x, if x is a E21 then x is an E20). 
>In the definitions of classes and properties in this document the universal quantifier(s) are omitted for simplicity, so the above axiom is simply written:
>E21(x) ⇒E20(x)
>Likewise, the above domain constraint on property P152 has parent can be formulated in first order logic as the axiom:
>P152(x,y) ⇒E21(x)
>(reading: for all individuals x and y, if x is a P152 of y, then x is an E21).
>Properties of properties, indicated by a '.1' after the property number are described as ternary predicate symbols. For example, the property P14.1 in the role of  is described as the ternary predicate symbol corresponding to property P14 carried out by (performed) :
>P14(x,y) ⇒ E7(x)
>P14(x,y)⇒ E39(y)
>P14(x,y,z) ⇒ [P14(x,y) ∧ E55(z)]
>These basic considerations should be used by the reader to understand the logical axioms that are used into the definition of the classes and properties. Further information about the first order formulation of CIDOC CRM can be found in (Meghini & Doerr, 2018)

### ***Sobre las expresiones lógicas utilizadas en CIDOC CRM***

Las presentes especificaciones de CIDOC CRM están anotadas con axiomas lógicos, proporcionando una expresión formal adicional de la ontología.
Esta sección presenta brevemente los supuestos que están en la base de la expresión lógica de CIDOC CRM (para una descripción completamente detallada de la expresión lógica del modelado de datos semánticos, ver (Reiter, 1984)).

El CIDOC CRM se expresa en términos de modelado de primitivas de datos semánticos.
Como tal, consta de:

- *clases,* que representan las nociones generales en el dominio del discurso, como CIDOC CRM class E21 Person que representa la noción de persona.
- *propiedades,* que representan las relaciones binarias que vinculan a los individuos en el dominio del discurso, como la propiedad P152 *has parent* que vincula a una persona con uno de los padres de la persona.

Las clases y propiedades se utilizan para expresar el conocimiento ontológico por medio de varios tipos de restricciones, como enlaces subclase/subpropiedad.
Por ejemplo, E21 Person es una subclase de E20 Biological Object; o restricciones dominio/rango, por ejemplo, el dominio de *P152 has parent* es clase E21 Person.

En contraste, la representación de conocimiento de primer orden, con una base lógica, recae en el lenguaje formal que codifica una ontología.
Este lenguaje puede ponerse directamente en correspondencia con el modelado semántico de los datos de forma sencilla:

- clases se nombran mediante *símbolos de predicado unario*; convencionalmente usamos E21 como el símbolo de predicado unario correspondiente a la clase E21 Person;
- propiedades se nombran mediante *símbolos de predicado binario*; convencionalmente usamos P152 como el símbolo de predicado binario correspondiente a la propiedad P152 *has parent.*
- propiedades de propiedades; las “propiedades .1” se nombran mediante *símbolos de predicado ternario*; convencionalmente usamos P14.1 como el símbolo de predicado ternario correspondiente a la propiedad *P14.1 in the role of*.

La ontología se expresa en lógica a través de *axiomas* que corresponden a las limitaciones del modelado semántico.
En la definición de clases y propiedades de CIDOC CRM, los axiomas se colocan bajo el encabezado ‘En lógica de primer orden’.
Hay varias opciones para escribir declaraciones en lógica de primer orden. En este documento utilizamos una notación compacta estándar, ampliamente utilizada en libros de texto y artículos científicos.
La definición se da en la siguiente tabla.

Tabla 1: Operadores simbólicos en Representación Lógica de Primer Orden

| Símbolo | Nombre | Se lee como | Valor real |
|---|---|---|---|
| Operadores | | |
| ∧ | conjunción | y | (φ ∧ ψ) es verdadera si y solo si ambos *φ* **y** *ψ* son verdaderos. <br><br>true ∧ true = true <br>false ∧ false = false <br>true ∧ false = false <br>false ∧ true = false |
| ∨ | disjunction | o | (φ ∨ ψ) (φ ∨ ψ) es verdadera si y solo si al menos uno de φ **o** ψ es verdadero. <br><br>true ∨ true = true <br>false ∨ false = false <br>true ∨ false = true <br>false ∨ true = true |
| ¬ | negación | no | ¬φ es verdadera si y solo si *φ* es falsa <br><br>true = false <br>false = true |
| ⇒ | implicación | implica, si ... entonces ...  | ((φ ⇒ ψ) es verdadera si y solo si no es el caso de que φ sea verdadero y ψ sea falso <br><br>true ⇒ true = false <br>false ⇒ false = false <br>true ⇒ false = true <br>false ⇒ true = false |
| ⇔ | equivalencia | es equivalente a, si ...y solo si …  | φ ⇔ ψ es verdadera si y solo si ambos φ y ψ son verdaderos o ambos φ y ψ son falsos <br><br>true ⇔ true = true <br>false ⇔ false = true <br>true ⇔ false = false <br>false ⇔ true = false |
| Cuantificadores |||
| ∃ | cuantificador existencial | existe, existe al menos uno||
| ∀ | cuantificador universal | para todos ||

 Por ejemplo, el vínculo previo de la subclase entre E21 Person y E20 Biological Object se puede formular en lógica de primer orden como el axioma

 ```(∀x) [E21(x) ⇒ E20(x)]```

 (Es decir: para todos los individuos x, si x es un E21, entonces x es un E20).

 En las definiciones de clases y propiedades en este documento, los cuantificadores universales se omiten por simplicidad, por lo que el axioma anterior se escribe simplemente:

 ```E21(x) ⇒ E20(x)```

 Del mismo modo, la restricción de dominio anterior sobre la propiedad *P152 has parent* se puede formular en lógica de primer orden como el axioma:

```P152(x, y) ⇒ E21(x)```

 (Es decir: para todos los individuos x *y* y, si x es una P152 de y, entonces x es una E21).

 El lector debe utilizar estas consideraciones básicas para comprender los axiomas lógicos que se utilizan en la definición de las clases y propiedades.
 Se puede encontrar más información sobre la formulación de primer orden de CIDOC CRM en (Meghini y Doerr, 2018)
