> ### strict inheritance
> Strict inheritance means that there are no exceptions to the inheritance of properties from superclasses to subclasses. For instance, some systems may declare that elephants are grey, and regard a white elephant as an exception. Under strict inheritance it would hold that: if all elephants were grey, then a white elephant could not be an elephant. Obviously not all elephants are grey. To be grey is not part of the intension of the concept elephant but an optional property. The CIDOC CRM applies strict inheritance as a normalization principle.

### herencia estricta *(strict inheritance)*

La **herencia estricta** significa que no hay excepciones a la herencia de **propiedades** de **superclases** a **subclases**.
Por ejemplo, algunos sistemas pueden declarar que los elefantes son grises y considerar al elefante blanco como una excepción.
Según una herencia estricta, se sostendría que: si todos los elefantes fueran grises, entonces un elefante blanco no podría ser un elefante.
Evidentemente, no todos los elefantes son grises. Ser gris no es parte de la **intensión** del concepto elefante, sino una propiedad opcional.
CIDOC CRM aplica la herencia estricta como principio de normalización.
