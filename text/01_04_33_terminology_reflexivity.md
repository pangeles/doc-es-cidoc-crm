> ### reflexivity
>Reflexivity is defined in the standard way found in mathematics or logic: A property P is reflexive if the domain and range are the same class and for all instances x, of this class the following is the case: x is related by P to itself. The intention of a property as described in the scope note will decide whether a property is reflexive or not. An example of a reflexive property is E53 Place. P89 falls within (contains): E53 Place. 

### reflexividad *(reflexivity)*

 La reflexividad se define de la manera estándar que se encuentra en las matemáticas o la lógica: una propiedad P es reflexiva si el dominio y el rango son de la misma clase y para todas las instancias x, de esta clase, se da el siguiente caso: x está relacionado por P consigo mismo.
 La intención de una propiedad como se describe en la nota de alcance decidirá si una propiedad es reflexiva o no.
 Un ejemplo de una propiedad reflexiva es E53 Place. *P89 falls within (contains):* E53 Place.
