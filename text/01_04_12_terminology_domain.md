> ### domain
>The domain is the class for which a property is formally defined. This means that instances of the property are applicable to instances of its domain class. A property must have exactly one domain, although the domain class may always contain instances for which the property is not instantiated. The domain class is analogous to the grammatical subject of the phrase for which the property is analogous to the verb. It is arbitrary which class is selected as the domain and which as the range, just as the choice between active and passive voice in grammar is arbitrary. Property names in the CIDOC CRM are designed to be semantically meaningful and grammatically correct when read from domain to range. In addition, the inverse property name, normally given in parentheses, is also designed to be semantically meaningful and grammatically correct when read from range to domain.

### dominio *(domain)*

El dominio es la **clase** para la que se define formalmente una **propiedad**, de modo que las instancias de una propiedad sólo pueden aplicarse a instancias de su clase de dominio.
Una propiedad debe tener exactamente un dominio, aunque la clase de dominio siempre puede contener instancias para las que la propiedad no es instanciada.
La clase de dominio es análoga al sujeto gramatical de la frase en la que la propiedad es análoga al verbo.
Es arbitrario qué clase se selecciona como dominio y cuál como **rango**, al igual que la elección entre las voces activa y pasiva es arbitraria en gramática.
Los nombres de propiedad en CIDOC CRM están diseñados para ser semánticamente significativos y gramaticalmente correctos cuando se leen de dominio a rango.
Además, el nombre de la propiedad inversa, normalmente entre paréntesis, también está diseñado para ser semánticamente significativo y gramaticalmente correcto cuando se lee de rango a dominio.

