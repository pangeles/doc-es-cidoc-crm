> ### inheritance
>Inheritance of properties from superclasses to subclasses means that if an item x is an instance of a class A, then:
>    1 all properties that must hold for the instances of any of the superclasses of A must also hold for item x, and
>    2 all optional properties that may hold for the instances of any of the superclasses of A may also hold for item x.

### herencia *(inheritance)*

La herencia de **propiedades** de **superclases** hacia las **subclases** significa que si un elemento x es una **instancia** de una **clase** A, entonces:

1. todas las propiedades que deben ser válidas para las instancias de cualquiera de las superclases de A también deben ser válidas para el elemento x; además,
2. todas las propiedades opcionales que pueden ser válidas para las instancias de cualquiera de las superclases de A también pueden ser válidas para el elemento x.
