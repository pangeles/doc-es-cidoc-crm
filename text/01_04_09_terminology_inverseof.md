> ### inverse of 
>The inverse of a property is the reinterpretation of a property from range to domain without more general or more specific meaning, similar to the choice between active and passive voice in some languages. In contrast to some knowledge representation languages, such as RDF and OWL, we regard that the inverse of a property is not a property in its own right that needs an explicit declaration of being inverse of another, but an interpretation implicitly existing for any property. The inverse of the inverse of a property is identical to the property itself, i.e., its primary sense of direction.
>For example:
>“CRM Entity is depicted by Physical Human-Made Thing” is the inverse of “Physical Human-Made Thing depicts CRM Entity” 

### inverso de *(inverse of)*

El “inverso de”, de una propiedad, es la reinterpretación de una **propiedad** de **rango** a **dominio** sin un significado más general o más específico, similar a la elección entre voces activa y pasiva en algunos idiomas.
En contraste con algunos lenguajes de representación del conocimiento, como RDF y OWL, consideramos que el “inverso de”, de una propiedad, no es una propiedad en sí misma, ni necesita una declaración explícita de ser inversa de otra, sino una interpretación implícitamente existente para cualquier propiedad.
El opuesto del “inverso de”, de una propiedad, es idéntico a la propiedad misma; es decir, su sentido primario de dirección.
La inversa de la inversa de una propiedad es idéntica a la propiedad misma, es decir, su sentido principal de dirección.

Por ejemplo:

“CRM Entity is *depicted by* Physical Human-Made Thing” es el inverso de “Physical Human-Made Thing depicts CRM Entity”. |
