> ## Applied Form
>The CIDOC CRM is an ontology in the sense used in computer science. It has been expressed as an object-oriented semantic model, in the hope that this formulation will be comprehensible to both documentation experts and information scientists alike, while at the same time being readily converted to machine-readable formats such as RDF Schema or OWL. A CRM conformant documentation system can be implemented using RDF Schema or OWL, but also in Relational or Object-Oriented schema. CIDOC CRM instances can be encoded in RDF, JSON LD, XML, OWL and others.
>More specifically, the CIDOC CRM is expressed in terms of the primitives of semantic data modelling. As such, it consists of:
>-   _classes_, which represent general notions in the domain of discourse, such as the CIDOC CRM class E21 Person which represents the notion of person;
>-   _properties_, which represent the binary relations that link the individuals in the domain of discourse, such as the CIDOC CRM property P152 has parent linking a person to one of the person’s parent.
>-   _properties of properties_, such as the property _P14.1 in the role of_, of the CIDOC CRM property P14 carried out by (see also section “About Types”).  
>They do not appear in the property hierarchy list, but are included as part of their base property declaration and are referred to in the class declarations. They all have the implicit quantification “many to many” (see also section “Property Quantifiers”)
>  Although the definition of the CIDOC CRM provided here is complete, it is an intentionally compact and concise presentation of the CIDOC CRM’s 81 classes and 160 unique properties. It does not attempt to articulate the inheritance of properties by subclasses throughout the class hierarchy (this would require the declaration of several thousand properties, as opposed to 160). However, this definition does contain all of the information necessary to infer and automatically generate a full declaration of all properties, including inherited properties.

## Forma de aplicación

 El CIDOC CRM es una ontología en el sentido utilizado en informática. S
 e ha expresado como un modelo semántico orientado a objetos, con la esperanza de que esta formulación sea comprensible tanto para los expertos en documentación como para los científicos de la información y, al mismo tiempo, se convierta fácilmente a formatos legibles por máquinas como RDF Schema o OWL.
 Se puede implementar en esquemas relacionales u orientados a objetos.
 Las instancias de CIDOC CRM también se pueden codificar en RDF, JSON LD, XML, OWL, entre otros.

 Aunque la definición de CIDOC CRM proporcionada aquí es completa, es una presentación intencionalmente compacta y concisa de las 81 clases y 160 propiedades únicas de CIDOC CRM.
 No intenta articular la herencia de propiedades por subclases en toda la jerarquía de clases (esto requeriría la declaración de varios miles de propiedades, a diferencia de 160).
 Sin embargo, esta definición contiene toda la información necesaria para inferir y automáticamente generar una declaración completa de todas las propiedades, incluidas las heredadas.
