> ### interoperability
>Interoperability means the capability of different information systems to communicate some of their contents. In particular, it may mean that
>    1  two systems can exchange information, and/or 
>    2  multiple systems can be accessed with a single method. 
>Generally, syntactic interoperability is distinguished from semantic interoperability. Syntactic interoperability means that the information encoding of the involved systems and the access protocols are compatible, so that information can be processed as described above without error. However, this does not mean that each system processes the data in a manner consistent with the intended meaning. For example, one system may use a table called “Actor” and another one called “Agent”. With syntactic interoperability, data from both tables may only be retrieved as distinct, even though they may have exactly the same meaning. To overcome this situation, semantic interoperability has to be added. The CIDOC CRM relies on existing syntactic interoperability and is concerned only with adding semantic interoperability.

### interoperabilidad *(interoperability)*

 Interoperabilidad significa la capacidad de diferentes sistemas de información para comunicar algunos de sus contenidos.
 En particular, puede significar que

 1. dos sistemas pueden intercambiar información, y/o
 2. que se puede acceder a varios sistemas con un solo método.

 Generalmente, se debe distinguir la interoperabilidad sintáctica de la **interoperabilidad semántica**.
 La interoperabilidad sintáctica significa que la codificación de la información de los sistemas involucrados y los protocolos de acceso son compatibles, de modo que la información se puede procesar como se describe anteriormente, sin errores.
 Sin embargo, esto no significa que cada sistema procese los datos de una manera consistente con el significado pretendido.
 Por ejemplo, un sistema puede usar una tabla llamada “Actor” y otra llamada "Agente".
 Con la interoperabilidad sintáctica, los datos de ambas tablas solo se pueden recuperar como distintos, aunque tengan exactamente el mismo significado.
 Para superar esta situación, se debe agregar la interoperabilidad semántica.
 CIDOC CRM se basa en la interoperabilidad sintáctica existente y solo se preocupa por agregar la *interoperabilidad semántica.*
