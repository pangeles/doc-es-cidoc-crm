# Mentions légales

## Directeur de la publication

## Directeur de la rédaction

## Hébergeur
CNRS/ TGIR Huma-Num - [https://www.huma-num.fr](https://www.huma-num.fr)

## Propriété intellectuelle
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Ce travail est sous licence <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## Liens hypertextes
La mise en place de liens hypertextes par des tiers vers des pages ou des documents diffusés sur le site du CNRS, est autorisée sous réserve que les liens ne contreviennent pas aux intérêts du CNRS, et, qu’ils garantissent la possibilité pour l’utilisateur d’identifier l’origine et l’auteur du document.
