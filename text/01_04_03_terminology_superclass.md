> ### superclass
>A superclass is a class that is a generalization of one or more other classes (its subclasses), which means that it subsumes all instances of its subclasses, and that it can also have additional instances that do not belong to any of its subclasses. The intension of the superclass is less restrictive than any of its subclasses. This subsumption relationship or generalization is the inverse of the IsA relationship or specialization.
>For example:
>“Biological Object subsumes Person” is synonymous with “Biological Object is a superclass of Person”. It needs fewer traits to identify an item as a Biological Object than to identify it as a Person.

### superclase (*superclass*)

 Una superclase es una **clase** que es una generalización de una o más clases (sus **subclases**),
 lo que significa que subsume todas las **instancias** de sus subclases y que también puede tener instancias adicionales que no pertenecen a ninguna de sus subclases.
 La **intensión** de la superclase es menos restrictiva que cualquiera de sus subclases.
 Esta relación de subsunción o generalización es la relación inversa de IsA, o su especialización.

 Por ejemplo:

 “Objeto Biológico subsume Persona” es sinónimo de “Objeto Biológico es una superclase de Persona”.
 Se necesitan menos rasgos para identificar un ítem como un Objeto Biológico que para identificarlo como una Persona.

```mermaid
 stateDiagram-v2
    direction TB
    bio_object: Objeto_Biológico [superclase]
    state bio_object {
        direction BT
        s --> [*]
        s: "Persona [clase]"
    }
```

| | |
| :--- | :--- |
| *Notas del traductor* |  En el párrafo: “La **intensión** de la superclase es menos restrictiva que cualquiera de sus subclases. Esta relación de subsunción o generalización es la inversa de la relación IsA, o su especialización”, genera los siguientes comentarios: la relación IsA se dirige desde la subclase a la superclase; de lo más concreto a lo más generalizado. La subclase hereda los parámetros de la superclase. Ejemplo de Alex: Persona (clase) / subclase: delgado, rubio, etc. <br> <br> Subsunción: RAE. Considerar algo como parte de un conjunto más amplio o como caso particular sometido a un principio o norma general. |
