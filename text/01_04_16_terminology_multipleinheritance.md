> ### multiple inheritance
>Multiple inheritance means that a class A may have more than one immediate superclass. The extension of a class with multiple immediate superclasses is a subset of the intersection of all extensions of its superclasses. The intension of a class with multiple immediate superclasses extends the intensions of all its superclasses, i.e., its traits are more restrictive than any of its superclasses. If multiple inheritance is used, the resulting “class hierarchy” is a directed graph and not a tree structure. If it is represented as an indented list, there are necessarily repetitions of the same class at different positions in the list.
>For example:
>Person is both an Actor and a Biological Object.

### herencia múltiple *(multiple inheritance)*

La **herencia múltiple** significa que una **clase** A puede tener más de una **superclase** inmediata.
La **extensión** de una clase con múltiples superclases inmediatas es un subconjunto de la intersección de todas las extensiones de sus superclases.
La **intensión** de una clase con múltiples superclases inmediatas extiende las connotaciones de todas sus superclases;
es decir, sus rasgos son más restrictivos que cualquiera de sus superclases. Si se usa herencia múltiple, la “jerarquía de clases” resultante es un grafo dirigido y no una estructura de árbol.
Si se representa intencionalmente como una lista, necesariamente hay repeticiones de la misma clase en diferentes posiciones de la lista.

Por ejemplo:

Una persona es tanto un Actor como un Objeto Biológico.


```mermaid
flowchart BT
    id2([persona]) -- IsA (es un) --> id1([Actor])
    id2([persona]) -- IsA (es un) --> id3([Objeto Biológic])
```
