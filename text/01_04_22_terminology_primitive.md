> ### primitive 
>The term primitive as used in knowledge representation characterizes a concept that is declared and its meaning is agreed upon, but that is not defined by a logical deduction from other concepts. For example, mother may be described as a female human with child. Then mother is not a primitive concept. Event however is a primitive concept. 
>Most of the CIDOC CRM is made up of primitive concepts.

### primitiva *(primitive)*

 El término primitiva, tal como se utiliza en la representación del conocimiento, caracteriza a un concepto que se declara y cuyo significado es acordado, pero que no se define mediante una deducción lógica de otros conceptos.
 Por ejemplo, “madre” puede describirse como un humano femenino con un hijo.
 Entonces, madre no es una conceptualización primitiva.
 Sin embargo, “evento” es una conceptualización primitiva.
 La mayor parte del modelo CIDOC CRM se compone de conceptualizaciones primitivas.
