> ### class
>A class is a category of items that share one or more common traits serving as criteria to identify the items belonging to the class. These properties need not be explicitly formulated in logical terms, but may be described in a text (here called a scope note) that refers to a common conceptualisation of domain experts. The sum of these traits is called the intension of the class. A class may be the domain or range of none, one or more properties formally defined in a model. The formally defined properties need not be part of the intension of their domains or ranges: such properties are optional. An item that belongs to a class is called an instance of this class. A class is associated with an open set of real-life instances, known as the extension of the class. Here “open” is used in the sense that it is generally beyond our capabilities to know all instances of a class in the world and indeed that the future may bring new instances about at any time (Open World). Therefore, a class cannot be defined by enumerating its instances. A class plays a role analogous to a grammatical noun, and can be completely defined without reference to any other construct (unlike properties, which must have an unambiguously defined domain and range). In some contexts, the terms individual class, entity or node are used synonymously with class. 
>For example: 
>Person is a class. To be a Person may actually be determined by DNA characteristics, but we all know what a Person is. A Person may have the property of being a member of a Group, but it is not necessary to be member of a Group in order to be a Person. We shall never know all Persons of the past. There will be more Persons in the future. 

### clase (*class*)

Una clase es una categoría de ítems que comparten uno o más rasgos en común, y que sirven como criterios para identificar a los ítems que le pertenecen.
Las **propiedades** de una clase no necesitan formularse explícitamente, en términos lógicos, pero pueden describirse en un texto (aquí llamado **nota de alcance**), que se refiere a una conceptualización común de los expertos en el dominio.
La suma de estos rasgos se llama **intensión** de la clase. Una clase puede ser el dominio o rango de ninguna, una o más propiedades definidas formalmente en un modelo.

Es decir, las propiedades definidas formalmente no necesitan ser parte de la intensión de su **dominio** o **rango**: tales propiedades son opcionales.
Un ítem que pertenece a una clase se denomina **instancia** de esta clase.
Una clase está asociada con un conjunto abierto de instancias de la vida real, conocido como la extensión de la clase.
Aquí “abierto” se usa en el sentido de que generalmente está más allá de nuestras capacidades conocer todas las instancias de una clase en el mundo y, de hecho, que el futuro puede traer nuevas instancias en cualquier momento (**Mundo Abierto**).
Por lo tanto, una clase no se puede definir enumerando sus instancias.
Una clase juega un papel análogo a un sustantivo gramatical.
Puede definirse completamente sin hacer referencia a ninguna otra construcción (a diferencia de las propiedades, que deben tener un dominio y un rango definidos sin ambigüedades).
En algunos contextos, los términos clase individual, entidad o nodo se utilizan como sinónimo de clase.

Por ejemplo:

Persona es una clase. Ser una Persona puede estar determinado por las características del ADN, pero todos sabemos qué es una Persona.
Una Persona puede tener la propiedad de ser miembro de un Grupo, pero no es necesario ser miembro de un Grupo para ser una Persona.
Nunca conoceremos a todas las Personas del pasado. Habrá más Personas en el futuro.


#### **Persona**

---

```mermaid
    erDiagram
        Persona {
            int id
            string Nombre
            string Apellidos
            int Grupo
        }
```

| | |
| :--- | :---|
| *Referencias* | [https://encyclopaedia.herdereditorial.com/wiki/Extensión_/_Intensión](https://encyclopaedia.herdereditorial.com/wiki/Extensi%C3%B3n_/_Intensi%C3%B3n) <br> [https://dle.rae.es/intensión](https://dle.rae.es/intensi%C3%B3n) <br> [https://encyclopaedia.herdereditorial.com/wiki/Clase_(lógica)](https://encyclopaedia.herdereditorial.com/wiki/Clase_\(l%C3%B3gica\)) |