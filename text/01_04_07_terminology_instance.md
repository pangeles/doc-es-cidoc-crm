> ### instance
>An instance of a class is a real-world item that fulfils the criteria of the intension of the class. Note, that the number of instances declared for a class in an information system is typically less than the total in the real world. For example, you are an instance of Person, but you are not mentioned in all information systems describing Persons.
>For example:
>The painting known as the “The Mona Lisa” is an instance of the class E22 Human-Made Object.
>An instance of a property is a factual relation between an instance of the domain and an instance of the range of the property that matches the criteria of the intension of the property.
>For example:
>The Mona Lisa has former or current owner. The Louvre is an instance of the property P51 has former or current owner (is former or current owner of).

### instancia *(instance)*
 Una instancia de una **clase** es un ítem del mundo real que cumple los criterios de la **intensión** de la **clase**.
 Tómese en cuenta que el número de **instancias** declaradas para una clase en un sistema de información suele ser menor que el total del mundo real.
 Por ejemplo, usted es una instancia de **Persona**, pero no se le menciona en todos los sistemas de información que describen Personas.

 Por ejemplo:

 La pintura conocida como “La Mona Lisa” es una instancia de la clase Human Made Object ―Objeto Creado por Humanos―.

 Una instancia de una **propiedad** es una relación fáctica entre una instancia del **dominio** y una instancia del **rango** de la propiedad que coincide con los criterios de la **intensión** de la propiedad.

 Por ejemplo: La Mona Lisa *has former or current owner.* El Louvre es una instancia de la propiedad **P51;** es decir, *P51 has former or current owner (is former or current owner of).*


```mermaid
flowchart LR
    id2([La Mona Lisa :: E22]) -- has former or current owner :: P51 (tiene propietario anterior o actual) --> id1([El Louvre])
```

| | |
| :--- | :--- |
| *Notas del traductor* |  Preguntar a los redactores de CRM si en el ejemplo no convendría hacer explícito el rango: <br><br> `crm:E22_Human-Made_Object crm:P51_has_former_or_current_owner crm:E74_Group.`<br><br> Justificando sobre todo, porque la definición de los términos “Dominio” y “Rango” está más adelante, lo cual puede llevar a abordar un siguiente término sin haber comprendido completamente el anterior. Pudiendo resultar de mucha ayuda, que en los ejemplos que acompañan las definiciones, se puedan ir marcando progresivamente los términos que se van abordando.  |
