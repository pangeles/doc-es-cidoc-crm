> # Volume A: Definition of the CIDOC Conceptual Reference Model
>Produced by the CIDOC CRM 
>Special Interest Group
>Version 7.1.2
>June 2022
>Editors: Chryssoula Bekiari, George Bruseker, Erin Canning, Martin Doerr, 
>Philippe Michon, Christian-Emil Ore, Stephen Stead, Athanasios Velios
>CC BY 4.0 2022 Individual Contributors to CIDOC CRM 7.1.2
>Definition of the CIDOC Conceptual Reference Model version 7.1.2


# Volume A: Définition du modèle conceptuel de référence CIDOC
Produit par le CIDOC CRM <br>
Groupe d'intérêt spécial<br>
Version 7.1.2<br>
Juin 2022<br>
Éditeur·ices: Chryssoula Bekiari, George Bruseker, Erin Canning, Martin Doerr, <br>
Philippe Michon, Christian-Emil Ore, Stephen Stead, Athanasios Velios<br>
Traducteur·ices: Vincent Alamercery, Frédéric Bricaud, Bertrand David, Anais Guillem, Juliette Hueber, Raphaëlle Krummeich, Olivier Marlet, Bulle Tuil Leonetti, Muriel Van Ruymbeke <br>
CC BY 4.0 2022 contributeur·ices individuel·les au CIDOC CRM 7.1.2<br>
