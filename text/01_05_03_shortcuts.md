> ### Shortcuts
>Some properties are declared as shortcuts of longer, more comprehensively articulated paths that connect the same domain and range classes as the shortcut property via one or more intermediate classes. For example, the property E18 Physical Thing. _P52 has current owner (is current owner of)_: E39 Actor, is a shortcut for a fully articulated path from E18 Physical Thing through E8 Acquisition to E39 Actor. An instance of the fully-articulated path always implies an instance of the shortcut property. However, the inverse may not be true; an instance of the fully-articulated path cannot always be inferred from an instance of the shortcut property inside the frame of the actual KB
>The class E13 Attribute Assignment allows for the documentation of how the assignment of any property came about, and whose opinion it was, even in cases of properties not explicitly characterized as “shortcuts”.

### ***Atajos***

 Algunas propiedades se declaran como accesos directos de rutas más largas y articuladas que conectan las mismas clases de dominio y rango que la propiedad de atajo, a través de una o más clases intermedias.
 Por ejemplo, la propiedad E18 Physical Thing. *P52 has current owner (is current owner of):* E39 Actor, es un atajo para un camino completamente articulado, desde E18 Physical Thing, por medio de E8 Acquisition, hasta E39 Actor.
 Una instancia de la ruta completamente articulada siempre implica una instancia de la propiedad de atajo.
 Sin embargo, lo inverso puede no ser cierto; una instancia de la ruta completamente articulada no siempre se puede inferir de una instancia de la propiedad de atajo dentro del marco de la actual Base de Conocimiento o *Knowledge Base* (KB), en inglés.

 La clase E13 Attribute Assignment permite la documentación de cómo se produjo la asignación de cualquier propiedad, y de quién fue la opinión, incluso en los casos de propiedades no caracterizadas explícitamente como “atajos”.
