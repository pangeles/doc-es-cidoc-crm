>## Terminology
>The following definitions of key terminology used in this document are provided both as an aid to readers unfamiliar with object-oriented modelling terminology, and to specify the precise usage of terms that are sometimes applied inconsistently across the object-oriented modelling community for the purpose of this document. Where applicable, the editors have tried to consistently use terminology that is compatible with that of the Resource Description Framework (RDF),1 a recommendation of the World Wide Web Consortium. The editors have tried to find a language, which is comprehensible to the non-computer expert and precise enough for the computer expert so that both understand the intended meaning. 
>[3](#sdfootnote3anc)Information about the Resource Description Framework (RDF) can be found at http://www.w3.org/RDF/

## Terminología

Las definiciones que se proporcionan a continuación forman la terminología clave utilizada en este documento y una ayuda para los lectores que no están familiarizados con la terminología del diseño y modelado orientado a objetos.
Así mismo ―para propósitos de este documento― se especifica el uso preciso de términos que a veces se aplican de manera inconsistente en la comunidad del diseño y modelado orientado a objetos.
En su caso, los editores han intentado utilizar, de forma coherente, terminología compatible con la del Marco de Descripción de Recursos (Resource Description Framework, RDF),[^3] por recomendación del Consorcio *World Wide Web*.
Los editores han intentado encontrar un lenguaje que sea comprensible para los que no son expertos en informática, y lo suficientemente preciso para que el experto en informática comprenda el significado pretendido.

[^3]: Puede encontrar información sobre Resource Description Framework (RDF) en http://www.w3.org/RDF/
