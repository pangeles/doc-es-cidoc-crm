> ### property
>A property serves to define a relationship of a specific kind between two classes. The property is characterized by an intension, which is conveyed by a scope note. A property plays a role analogous to a grammatical verb, in that it must be defined with reference to both its domain and range, which are analogous to the subject and object in grammar (unlike classes, which can be defined independently). It is arbitrary, which class is selected as the domain, just as the choice between active and passive voice in grammar is arbitrary. In other words, a property can be interpreted in both directions, with two distinct, but related interpretations. Properties may themselves have properties that relate to other classes (This feature is used in this model only in order to describe dynamic subtyping of properties). Properties can also be specialized in the same manner as classes, resulting in IsA relationships between subproperties and their superproperties.
>In some contexts, the terms attribute, reference, link, role or slot are used synonymously with property.
>For example:
>“Physical Human-Made Thing depicts CRM Entity” is equivalent to “CRM Entity is depicted by Physical Human-Made Thing”.

### propiedad *(property)*

 Una propiedad sirve para definir una relación de un tipo específico entre dos **clases**.
 La propiedad se caracteriza por una **intensión**, que se transmite mediante una **nota de alcance**.
 Una propiedad juega un rol análogo al de un verbo gramatical, en el sentido de que debe definirse tanto en referencia a su **dominio** como a su **rango**,
 que son semejantes al sujeto y al objeto en gramática (a diferencia de las clases, que pueden definirse de manera independiente).
 Es arbitrario qué clase se selecciona como dominio, así como es arbitraria la elección entre voces activa y pasiva en gramática.
 En otras palabras, una propiedad se puede interpretar en ambas direcciones, con dos interpretaciones distintas pero relacionadas.
 Las propiedades pueden tener, a su vez, propiedades que se relacionen con otras clases (esta característica se usa en este modelo solo para describir subtipos dinámicos de propiedades).
 Las propiedades también se pueden especializar de la misma manera que las clases, resultando en relaciones IsA entre **subpropiedades** y sus **superpropiedades**.

 En algunos contextos, los términos atributo, referencia, enlace, función o espacio se utilizan como sinónimo de propiedad.

 Por ejemplo:

 “Physical Human-Made Thing *depicts* CRM Entity” \[Cosa Física hecha por Humanos *representa* la entidad CRM\] es equivalente a “CRM Entity *is depicted by* Physical Human-Made Thing” \[la Entidad CRM *está representada* por Cosa Física Hecha por Humanos”\].


| | |
| :--- | :--- |
| *Notas del traductor* | Entre corchetes, en el ejemplo, vemos una posible solución para la traducción de entidades y propiedades, siempre y cuando estas hayan sido traducidas adecuadamente. |