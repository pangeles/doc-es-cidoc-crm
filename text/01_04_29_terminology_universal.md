> ### universal
>The fundamental ontological distinction between universals and particulars can be informally understood by considering their relationship with instantiation: particulars are entities that have no instances in any possible world; universals are entities that do have instances. Classes and properties (corresponding to predicates in a logical language) are usually considered to be universals. (after Gangemi et al. 2002, pp. 166-181).

### universal *(universal)*

 La distinción ontológica fundamental entre universales y particulares puede entenderse informalmente considerando su relación con la instanciación:
 los particulares son entidades que no tienen **instancias** en ningún mundo posible;
 los universales son entidades que sí tienen instancias. Las **clases** y las **propiedades** (correspondientes a predicados en un lenguaje lógico) generalmente se consideran universales (según Gangemi *et al.* 2002, págs. 166-181).
