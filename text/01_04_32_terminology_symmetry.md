> ### symmetry
>Symmetry is defined in the standard way found in mathematics or logic: A property P is symmetric if the domain and range are the same class and for all instances x, y of this class the following is the case: If x is related by P to y, then y is related by P to x. The intention of a property as described in the scope note will decide whether a property is symmetric or not. An example of a symmetric property is E53 Place. P122 borders with: E53 Place. The names of symmetric properties have no parenthetical form, because reading in the range-to-domain direction is the same as the domain-to-range reading. 

### simetría *(symmetry)*

 La simetría se define de la manera estándar en que se encuentra en las matemáticas o la lógica: una propiedad P es simétrica si el dominio y el rango son de la misma clase y para todas las instancias x, y de esta clase ocurre lo siguiente: si x está relacionado por P con y, entonces y está relacionado por P con x.
 La intención de una propiedad como se describe en la nota de alcance determinará si una propiedad es simétrica o no. Un ejemplo de propiedad simétrica es E53 Place.
 *P122 borders with**:*** E53 Place**.** Los nombres de las propiedades simétricas no tienen forma parentética, porque la lectura en la dirección de rango a dominio es la misma que la lectura de dominio a rango.