> ### Inheritance and Transitivity
>CIDOC CRM is formulated as a class system with inheritance. A property P with domain A and range B will also be a property between any possible subclasses of A and of B. In many cases there will be a common subclass C of both A and B. In these cases, when the property is restricted to C, that is, with C as domain and range, the restricted property could be transitive. For instance, an E73 Information Object can be incorporated into an E90 Symbolic Object and thus an information object can be incorporated in another information object.
>In the definition of CIDOC CRM the transitive properties are explicitly marked as such in the scope notes. All unmarked properties should be considered as not transitive.

###  ***Herencia y transitividad***

CIDOC CRM está formulado como un sistema de clases con herencia.
Una propiedad P con dominio A y rango B también será una propiedad entre cualquier posible subclase de A y de B.
En muchos casos habrá una subclase C común tanto de A como de B.
En estos casos, cuando la propiedad está restringida a C, es decir, con C como dominio y rango, la propiedad restringida podría ser transitiva.
Por ejemplo, E73 Information Object puede incorporarse en un E90 Symbolic Object y, por lo tanto, un objeto de información puede incorporarse en otro objeto de información.

En la definición de CIDOC CRM las propiedades transitivas se marcan explícitamente como tales en las notas de alcance.
Todas las propiedades no marcadas deben considerarse no transitivas.
