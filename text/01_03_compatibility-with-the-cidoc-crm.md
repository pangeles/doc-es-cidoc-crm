
> ## Compatibility with the CIDOC CRM
>Users intending to take advantage of the semantic interoperability offered by the CIDOC CRM should ensure conformance with the relevant data structures. Conformance pertains either to data to be made accessible in an integrated environment or intended for transport to other environments. Any encoding of data in a formal language that preserves the relations of the classes, properties, and inheritance rules defined by this International Standard, is regarded as conformant.
>Conformance with the CIDOC CRM does not require complete matching of all local documentation structures, nor that all concepts and structures present in this International Standard be implemented. This International Standard is intended to allow room both for extensions, needed to capture the full richness of cultural documentation, and for simplification, in the interests of economy. A system will be deemed partially conformant if it supports a subset of subclasses and sub properties defined by this International Standard. Designers of the system should publish details of the constructs that are supported.
>The focus of the CIDOC CRM is the exchange and mediation of structured information. It does not require the interpretation of unstructured (free text) information into a structured, logical form. Unstructured information is supported, but falls outside the scope of conformance considerations.
>Any documentation system will be deemed conformant with this International Standard, regardless of the internal data structures it uses; if a deterministic logical algorithm can be constructed, that transforms data contained in the system into a directly compatible form without loss of meaning.
>No assumptions are made as to the nature of this algorithm. "Without loss of meaning" signifies that designers and users of the system are satisfied that the data representation corresponds to the semantic definitions provided by this International Standard.

## Compatibilidad de CIDOC CRM
Los usuarios que deseen aprovechar la interoperabilidad semántica que ofrece CIDOC CRM deben garantizar la coherencia con las estructuras de datos relevantes.
La conformidad se refiere a que los datos sean accesibles en un entorno integrado o que estén destinados a ser transportados a otros entornos.
Cualquier codificación de datos en un lenguaje formal que preserve las relaciones de las clases, las propiedades y las reglas de herencia definidas por este Estándar Internacional, se considera de conformidad con el modelo.

La conformidad con CIDOC CRM no requiere una coincidencia completa de todas las estructuras locales de la documentación, ni que se implementen todos los conceptos y estructuras presentes en el modelo.
El Estándar Internacional está destinado a dar cabida tanto a las extensiones que capturan la riqueza total de la documentación cultural como a las que simplifican, en aras de su economía.
Un sistema se considerará parcialmente de conformidad si admite un subconjunto de subclases y subpropiedades definidas por el estándar.
Los diseñadores del sistema deben publicar detalles de las estructuras generadas compatibles.

El enfoque de CIDOC CRM es el intercambio y la mediación de información estructurada. Por lo tanto, no requiere interpretar información no estructurada (texto libre) en una forma lógica y estructurada.
Se admite la información no estructurada, pero queda fuera del alcance de las consideraciones de conformidad.

Cualquier sistema de documentación se considerará conforme con este Estándar Internacional, independientemente de las estructuras de datos internas que utilice, si se puede construir un algoritmo lógico determinista que transforme los datos contenidos en el sistema, en una forma directamente compatible sin pérdida de significado.

No se hace ninguna suposición sobre la naturaleza de este algoritmo. “Sin pérdida de significado” quiere decir que los diseñadores y usuarios del sistema están satisfechos de que la representación de sus datos coincide con las definiciones semánticas proporcionadas por este Estándar Internacional. |
