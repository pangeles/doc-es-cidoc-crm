> ### shortcut
>A shortcut is a formally defined single property that represents a deduction or join of a data path in the CIDOC CRM. The scope notes of all properties characterized as shortcuts describe in words the equivalent deduction. Shortcuts are introduced for the cases where common documentation practice refers only to the deduction rather than to the fully developed path. For example, museums often only record the dimension of an object without documenting the Measurement that observed it. The CIDOC CRM declares shortcuts explicitly as single properties in order to allow the user to describe cases in which he has less detailed knowledge than the full data path would need to be described. For each shortcut, the CIDOC CRM contains in its schema the properties of the full data path explaining the shortcut.

### atajo *(shortcut)*

Un atajo es una **propiedad** única definida formalmente y que representa una deducción o unión de una ruta de datos en CIDOC CRM.
Las **notas de alcance** de todas las propiedades caracterizadas como atajos describen en palabras la deducción equivalente.
Se introducen atajos para los casos en que la práctica de documentación común se refiere solo a la deducción en lugar de a la ruta completamente desarrollada.
Por ejemplo, los museos a menudo sólo registran la dimensión de un objeto sin documentar quién realizó la medición.
CIDOC CRM declara los accesos directos explícitamente como propiedades únicas para permitir que el usuario describa casos en los que tiene un conocimiento menos detallado de lo que sería necesario describir en una ruta de datos completa.
Para cada atajo, el esquema de CIDOC CRM contiene las propiedades de una ruta completa de datos que explica el acceso directo.


```mermaid
    flowchart BT
    id3([Ptolomeo XV César :: E21]) --  was born (nació) :: P98i --> id4([Nacimiento de Ptolomeo XV César:: E67])
    id4 --  by mother (de madre) :: P96 --> id5([Cleopatra VII :: E21])
    id4 --  from father (de padre) :: P97 --> id6([Julius Caesar :: E21])
    id1([Ptolomeo XV César :: E21]) --  has parent (tiene padre) :: P152 --> id2([Julius Caesar:: E21])
    id1 --  has parent (tiene padre) :: P152 --> id7([Cleopatra VII:: E21])
```