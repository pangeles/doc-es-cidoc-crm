> ### monotonic reasoning
>Monotonic reasoning is a term from knowledge representation. A reasoning form is monotonic if an addition to the set of propositions making up the knowledge base never determines a decrement in the set of conclusions that may be derived from the knowledge base via inference rules. In practical terms, if experts enter subsequently correct statements to an information system, the system should not regard any results from those statements as invalid, when a new one is entered. The CIDOC CRM is designed for monotonic reasoning and so enables conflict-free merging of huge stores of knowledge. 

### raisonnement monotone

 El razonamiento monotónico es un término para la representación del conocimiento.
 Una forma de razonamiento es monotónica si una adición al conjunto de proposiciones, que componen la base de conocimiento, nunca determina una disminución en el conjunto de conclusiones que pueden derivarse de la base de conocimiento, a través de reglas de inferencia.
 En términos prácticos, si los expertos introducen declaraciones subsecuentemente correctas en un sistema de información, el sistema no debe considerar ningún resultado de esas declaraciones como inválido, al momento de ingresar uno nuevo.
 CIDOC CRM está diseñado para un razonamiento monotónico y, por lo tanto, permite la fusión sin conflictos de grandes almacenes de conocimiento.
