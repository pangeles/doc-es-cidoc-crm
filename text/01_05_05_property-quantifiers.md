> ### Property Quantifiers
>Quantifiers for properties are provided for the purpose of semantic clarification only, and should **not** be treated as implementation recommendations. The CIDOC CRM has been designed to accommodate alternative opinions and incomplete information, and therefore **all** properties should be implemented as optional and repeatable for their domain and range (“many to many (0,n:0,n)”). Therefore, the term “cardinality constraints” is avoided here, as it typically pertains to implementations.
>The following table lists all possible property quantifiers occurring in this document by their notation, together with an explanation in plain words. In order to provide optimal clarity, two widely accepted notations are used redundantly in this document, a verbal and a numeric one. The verbal notation uses phrases such as “one to many”, and the numeric one, expressions such as “(0,n:0,1)”. While the terms “one”, “many” and “necessary” are quite intuitive, the term “dependent” denotes a situation where a range instance cannot exist without an instance of the respective property. In other words, the property is “necessary” for its range. (Meghini, C. & Doerr, M., 2018)

>**many to many (0,n:0,n)**
>Unconstrained: An individual domain instance and range instance of this property can have zero, one or more instances of this property. In other words, this property is optional and repeatable for its domain and range.
>**one to many**
>**(0,n:0,1)**
>An individual domain instance of this property can have zero, one or more instances of this property, but an individual range instance cannot be referenced by more than one instance of this property. In other words, this property is optional for its domain and range, but repeatable for its domain only. In some contexts, this situation is called a “fan-out”.
>**many to one**
>**(0,1:0,n)**
>An individual domain instance of this property can have zero or one instance of this property, but an individual range instance can be referenced by zero, one or more instances of this property. In other words, this property is optional for its domain and range, but repeatable for its range only. In some contexts, this situation is called a “fan-in”.
>**many to many, necessary (1,n:0,n)**
>An individual domain instance of this property can have one or more instances of this property, but an individual range instance can have zero, one or more instances of this property. In other words, this property is necessary and repeatable for its domain, and optional and repeatable for its range.
>**one to many, necessary**
>**(1,n:0,1)**
>An individual domain instance of this property can have one or more instances of this property, but an individual range instance cannot be referenced by more than one instance of this property. In other words, this property is necessary and repeatable for its domain, and optional but not repeatable for its range. In some contexts, this situation is called a “fan-out”.
>**many to one, necessary**
>**(1,1:0,n)**
>An individual domain instance of this property must have exactly one instance of this property, but an individual range instance can be referenced by zero, one or more instances of this property. In other words, this property is necessary and not repeatable for its domain, and optional and repeatable for its range. In some contexts, this situation is called a “fan-in”.
>**one to many, dependent**
>**(0,n:1,1)**
>An individual domain instance of this property can have zero, one or more instances of this property, but an individual range instance must be referenced by exactly one instance of this property. In other words, this property is optional and repeatable for its domain, but necessary and not repeatable for its range. In some contexts, this situation is called a “fan-out”.
>**one to many, necessary, dependent**
>**(1,n:1,1)**
>An individual domain instance of this property can have one or more instances of this property, but an individual range instance must be referenced by exactly one instance of this property. In other words, this property is necessary and repeatable for its domain, and necessary but not repeatable for its range. In some contexts, this situation is called a “fan-out”.
>**many to one, necessary, dependent**
>**(1,1:1,n)**
>An individual domain instance of this property must have exactly one instance of this property, but an individual range instance can be referenced by one or more instances of this property. In other words, this property is necessary and not repeatable for its domain, and necessary and repeatable for its range. In some contexts, this situation is called a “fan-in”.
>**one to one**
>**(1,1:1,1)**
>An individual domain instance and range instance of this property must have exactly one instance of this property. In other words, this property is necessary and not repeatable for its domain and for its range.

>The CIDOC CRM defines some dependencies between properties and the classes that are their domains or ranges. These can be one or both of the following:
>-   the property is necessary for the domain
>-   the property is necessary for the range, or, in other words, the range is dependent on the property.
>The possible kinds of dependencies are defined in the table above. Note that if a dependent property is not specified for an instance of the respective domain or range, it means that the property exists, but the value on one side of the property is unknown. In the case of optional properties, the methodology proposed by the CIDOC CRM does not distinguish between a value being unknown or the property not being applicable at all. For example, one may know that an object has an owner, but the owner is unknown. In a CIDOC CRM instance this case cannot be distinguished from the fact that the object has no owner at all. Of course, such details can always be specified by a textual note.
>Note that the quantification of all properties of properties, “.1” properties, is “many-to-many” and, therefore, does not appear explicitly in their definitions.

### ***Cuantificadores de propiedad***

Los cuantificadores para propiedades se presentan únicamente con fines de aclaración semántica y no deben tratarse como recomendaciones de implementación.
CIDOC CRM ha sido diseñado para acomodar opiniones alternativas e información incompleta y, por lo tanto, todas las propiedades deben implementarse como opcionales y repetibles para su dominio y rango (“muchos a muchos (0,n:0,n)”).
Por lo tanto, el término “restricciones de cardinalidad” aquí se evita, ya que normalmente se refiere a implementaciones.

La siguiente tabla enumera todos los posibles cuantificadores de propiedad que aparecen en este documento por su notación, junto con una explicación en palabras sencillas.
Con el fin de proporcionar una claridad óptima, en este documento se utilizan de forma redundante dos notaciones ampliamente aceptadas, una verbal y otra numérica. La notación verbal utiliza frases como “uno a muchos” y, la numérica, expresiones como “(0,n:0,1)”.
Si bien los términos “uno”, “muchos” y “necesario” son bastante intuitivos, el término “dependiente” denota una situación en la que una instancia de rango no puede existir sin una instancia de la propiedad respectiva.
En otras palabras, la propiedad es “necesaria” para su rango. (Meghini y Doerr, 2018\)

| Cuantififcador | Definición |
|---|---|
| **muchos a muchos (0,n:0,n)** | Sin restricciones: una instancia individual de dominio y una instancia de rango de esta propiedad puede tener cero, una o más instancias de esta propiedad. En otras palabras, esta propiedad es opcional y repetible para su dominio y rango. |
| **uno a muchos (0,n:0,1)**| Una instancia individual de dominio de esta propiedad puede tener cero, una o más instancias de esta propiedad, pero una instancia individual de rango no puede ser referenciada por más de una instancia de esta propiedad. En otras palabras, esta propiedad es opcional para su dominio y rango, pero repetible únicamente para su dominio. En algunos contextos, esta situación se denomina “fan-out”. |
| **muchos a uno (0,1:0,n)** | Una instancia individual de dominio de esta propiedad puede tener cero o una instancia de esta propiedad, pero una instancia individual de rango puede ser referenciada por cero, una o más instancias de esta propiedad. En otras palabras, esta propiedad es opcional para su dominio y rango, pero repetible únicamente para su rango. En algunos contextos, esta situación se denomina “fan-in”. |
| **muchos a muchos, necesario (1,n:0,n)** | Una instancia individual de dominio de esta propiedad puede tener una o más instancias de esta propiedad, pero una instancia individual de rango puede tener cero, una o más instancias de esta propiedad. En otras palabras, esta propiedad es necesaria y repetible para su dominio, y opcional y repetible para su rango. |
| **uno a muchos, necesario  (1,n:0,1)** | Una instancia individual de dominio de esta propiedad puede tener una o más instancias de esta propiedad, pero una instancia individual de rango no puede ser referenciada por más de una instancia de esta propiedad. En otras palabras, esta propiedad es necesaria y repetible para su dominio y, opcional, pero no repetible para su rango. En algunos contextos, esta situación se denomina “fan-out”. |
| **muchos a uno, necesario  (1,1:0,n)** | Una instancia individual de dominio de esta propiedad debe tener exactamente una instancia de esta propiedad, pero una instancia individual de rango puede ser referenciada por cero, una o más instancias de esta propiedad. En otras palabras, esta propiedad es necesaria y no repetible para su dominio, y opcional y repetible para su rango. En algunos contextos, esta situación se denomina “fan-in”. |
| **uno a muchos, dependiente (0,n:1,1)** | Una instancia individual de dominio de esta propiedad puede tener cero, una o más instancias de esta propiedad, pero una instancia individual de rango debe ser referenciada por exactamente una instancia de esta propiedad. En otras palabras, esta propiedad es opcional y repetible para su dominio, pero necesaria y no repetible para su rango. En algunos contextos, esta situación se denomina “fan-out”. |
| **uno a muchos, necesario, dependiente  (1,n:1,1)** | Una instancia individual de dominio de esta propiedad puede tener una o más instancias de esta propiedad, pero una instancia individual de rango debe ser referenciada por exactamente una instancia de esta propiedad. En otras palabras, esta propiedad es necesaria y repetible para su dominio, y necesaria pero no repetible para su rango. En algunos contextos, esta situación se denomina “fan-out”. |
| **muchos a uno, necesario, dependiente  (1,1:1,n)** | na instancia individual de dominio de esta propiedad debe tener exactamente una instancia de esta propiedad, pero una instancia individual de rango puede ser referenciada por una o más instancias de esta propiedad. En otras palabras, esta propiedad es necesaria y no repetible para su dominio, y necesaria y repetible para su rango. En algunos contextos, esta situación se denomina “fan-in”. |
| **uno a uno (1,1:1,1)** | Una instancia individual de dominio y una instancia de rango de esta propiedad deben tener exactamente una instancia de esta propiedad. En otras palabras, esta propiedad es necesaria y no repetible para su dominio y para su rango. |

CIDOC CRM define algunas dependencias entre propiedades y clases que son sus dominios o rangos.
Estas pueden ser una o ambas de las siguientes combinaciones:

    A. la propiedad es necesaria para el dominio
    B. la propiedad es necesaria para el rango o, en otras palabras, el rango depende de la propiedad

Los posibles tipos de dependencia se definen en la tabla anterior.
Tenga en cuenta que si no se especifica una propiedad dependiente para una instancia del dominio o rango respectivo, significa que la propiedad existe, pero el valor en un lado de la propiedad es desconocido.
En el caso de las propiedades opcionales, la metodología propuesta por CIDOC CRM no distingue entre un valor desconocido o que la propiedad no es aplicable en absoluto.
Por ejemplo, uno puede saber que un objeto tiene un dueño, pero el dueño es desconocido.
En una instancia de CIDOC CRM, este caso no se puede distinguir del hecho de que el objeto no tiene ningún propietario.
Por supuesto, estos detalles siempre se pueden especificar mediante una nota textual.

| | |
| :--- | :--- |
| *References* |  Sobre “fan in” (abanico de entrada) y “fan out” (abanico de salida) [mundogeek](http://mundogeek.net/archivos/2012/04/01/principios-de-diseno-fan-in-y-fan-out/), [apiumhub](https://apiumhub.com/es/tech-blog-barcelona/azure-patron-fan-out-fan-in/#:~:text=El%20patr%C3%B3n%20%C2%ABfan%2Dout%2F,diferentes%20actividades%20de%20forma%20concurrente). |

