> ### intension
>The intension of a class or property is its intended meaning. It consists of one or more common traits shared by all instances of the class or property. These traits need not be explicitly formulated in logical terms, but may just be described in a text (here called a scope note) that refers to a conceptualisation common to domain experts. In particular, the so-called primitive concepts, which make up most of the CIDOC CRM, cannot be further reduced to other concepts by logical terms. 

### intensión (*intension*)

La intensión de una **clase** o **propiedad** es su significado previsto.
Consiste en uno o más rasgos comunes compartidos por todas las **instancias** de una clase o propiedad. Estos rasgos no necesitan formularse explícitamente en términos lógicos, sino que pueden describirse en un texto (aquí llamado **nota de alcance**) que se refiere a una conceptualización común a los expertos del dominio.
En particular, los denominados conceptos **primitivas**, que constituyen la mayor parte de CIDOC CRM, no pueden reducirse a otros conceptos mediante términos lógicos.

| | |
| :--- | :--- |
| *Notas del traductor* | Revisar a profundidad el término conceptos *primitivas* |