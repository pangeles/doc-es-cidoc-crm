> ### endurant, perdurant
>“The difference between enduring and perduring entities (which we shall also call endurants and perdurants) is related to their behaviour in time. Endurants are wholly present (i.e., all their proper parts are present) at any time they are present. Perdurants, on the other hand, just extend in time by accumulating different temporal parts, so that, at any time they are present, they are only partially present, in the sense that some of their proper temporal parts (e.g., their previous or future phases) may be not present. E.g., the piece of paper you are reading now is wholly present, while some temporal parts of your reading are not present any more. Philosophers say that endurants are entities that are in time, while lacking however temporal parts (so to speak, all their parts flow with them in time). Perdurants, on the other hand, are entities that happen in time, and can have temporal parts (all their parts are fixed in time).” (Gangemi et al. 2002, pp. 166-181). 

### endurantes, perdurantes *(endurant, perdurant)*

“La diferencia entre entidades durables y perdurables (que también llamaremos *endurantes* y *perdurantes*) está relacionada con su comportamiento en el tiempo.
Los endurantes están completamente presentes (es decir, todas las partes que le son propias están presentes) en cualquier momento en que estén presentes.
Los perdurantes, por otro lado, simplemente se extienden en el tiempo acumulando diferentes partes temporales, de modo que, en cualquier momento en que están presentes, están solo parcialmente presentes, en el sentido de que algunas de sus partes temporales propias (por ejemplo, sus fases anterior o futura) pueden no estar presentes siempre.
Por ejemplo, la hoja de papel que está leyendo ahora está completamente presente, mientras que algunas de las partes temporales de su lectura ya no estarán presentes.
Los filósofos dicen que los endurantes son entidades que están en el tiempo; sin embargo, carecen de partes temporales (por así decirlo, todas sus partes fluyen con ellas en el tiempo).
Los perdurantes, por otro lado, son entidades que ocurren en el tiempo y pueden tener partes temporales (todas sus partes están fijas en el tiempo)”. (Gangemi et al. 2002, págs. 166-181).

