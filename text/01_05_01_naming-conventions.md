> ### Naming Conventions
>The following naming conventions have been applied throughout the CIDOC CRM:
> -  Classes are identified by numbers preceded by the letter “E” (historically classes were sometimes referred to as “Entities”), and are named using noun phrases (nominal groups) using title case (initial capitals). For example, E63 Beginning of Existence.
> -  Properties are identified by numbers preceded by the letter “P,” and are named in both directions using verbal phrases in lower case. Properties with the character of states are named in the present tense, such as “has type”, whereas properties related to events are named in past tense, such as “carried out.” For example, _P126 employed (was employed in)_.
> -  Property names should be read in their non-parenthetical form for the domain-to-range direction, and in parenthetical form for the range-to-domain direction. Reading a property in range-to-domain direction is equivalent to the inverse of that property. Following a current notational practice in OWL knowledge representation language, we represent inverse properties in this text by adding a letter “i” following the identification number and the parenthetical form of the full property name, such as _P59i is located on or within_, which is the inverse of _P59 has section (is located on or within)._
> -  Properties with a range that is a subclass of E59 Primitive Value (such as E1 CRM Entity_. P3 has note:_ E62 String, for example) have no parenthetical name form, because reading the property name in the range-to-domain direction is not regarded as meaningful.
> -  Properties that have identical domain and range are either symmetric or transitive. Instantiating a symmetric property implies that the same relation holds for both the domain-to-range and the range-to-domain directions. An example of this is E53 Place_. P122 borders with:_ E53 Place. The names of symmetric properties have no parenthetical form, because reading in the range-to-domain direction is the same as the domain-to-range reading. Transitive asymmetric properties, such as E4 Period_._ _P9 consist of (forms part of):_ E4 Period, have a parenthetical form that relates to the meaning of the inverse direction.
> -  The choice of the domain of properties, and hence the order of their names, are established in accordance with the following priority list:
>    - Temporal Entity and its subclasses
>    - Thing and its subclasses
>    - Actor and its subclasses
>    - Other
>    - Properties of properties are identified by “P”, followed by the number of the base property extended with “.1” and are named in one direction using a verbal phrase in lower case in the present tense. For example: the property _P62.1 mode of depiction_ of the property _P62 depicts (is depicted by)_

### ***Convenciones de las nomenclaturas***

 Se han aplicado las siguientes convenciones de nomenclatura en todo CIDOC CRM:

 - Las clases se identifican con números precedidos por la letra “E” (históricamente, las clases a veces se denominaban “Entidades”) y se nombran usando frases nominales (grupos nominales), usando mayúsculas y minúsculas (mayúsculas al inicio). Por ejemplo, E63 Beginning of Existence.
 - Las propiedades se identifican con números precedidos por la letra "P" y se nombran en ambas direcciones utilizando frases verbales en minúsculas. Las propiedades con el carácter de estados se nombran en tiempo presente, como “has type” \[tiene tipo\], mientras que las propiedades relacionadas con eventos se nombran en tiempo pasado, como “carried out” \[llevado a cabo\]. Por ejemplo, *P126 employed (was employed in)* \[P126 se empleó en (fue empleado en)\].
 - Los nombres de propiedad deben leerse en forma no parentética para la dirección de dominio a rango y, en forma parentética, para la dirección de rango a dominio. Leer una propiedad en la dirección de rango a dominio es equivalente a la inversa de esa propiedad. Siguiendo una práctica de notación actual en el lenguaje de representación del conocimiento de OWL, representamos propiedades inversas en este texto agregando una letra “i” seguido del número de identificación y la forma parentética del nombre completo de la propiedad, como *P59i is located on or within* \[P59i se encuentra en o dentro de\], que es la inversa de *P59 has section (is located on or within)* \[P59 tiene sección (está ubicada en o dentro de)\].
 - Las propiedades con un rango que es una subclase de E59 Primitive Value (como E1 CRM Entity. *P3 has note:* E62 String, por ejemplo) no tienen nombre en forma parentética porque leer el nombre de la propiedad en la dirección del rango al dominio no es considerado significativo.
 - Las propiedades que tienen un dominio y un rango idénticos son simétricas o transitivas. Instanciar una propiedad simétrica implica que la misma relación es válida para las direcciones de dominio a rango y de rango a dominio. Un ejemplo de esto es E53 Place. *P122 borders with:* E53 Place. Los nombres de las propiedades simétricas no tienen forma entre paréntesis, porque la lectura en la dirección de rango a dominio es la misma que la lectura de dominio a rango. Propiedades asimétricas transitivas, como E4 Period. *P9 consist of (forms part of):* E4 Period, tienen una forma parentética que se relaciona con el significado de la dirección inversa.
 - La elección del dominio de las propiedades, y por tanto el orden de sus nombres, se establece de acuerdo con la siguiente lista de prioridades:
 	- Entidad Temporal y sus subclases
 	- Cosa y sus subclases
 	- Actor y sus subclases
 	- Otro
