> ### complement
>The complement of a class A with respect to one of its superclasses B is the set of all instances of B that are not instances of A. Formally, it is the set-theoretic difference of the extension of B minus the extension of A. Compatible extensions of the CIDOC CRM should not declare any class with the intension of them being the complement of one or more other classes. To do so will normally violate the desire to describe an Open World. For example, for all possible cases of human gender, male should not be declared as the complement of female or vice versa. What if someone is both or even of another kind? 

### complemento *(complement)*

El complemento de una clase A, con respecto a una de sus **superclases B,** es el conjunto de todas las **instancias** de B que no son instancias de A.
Formalmente, es la diferencia teórica de conjuntos de la **extensión** de B, menos la extensión de A.
Las extensiones compatibles de CIDOC CRM no deben declarar ninguna **clase** con la **intensión** de que sean el complemento de una o más clases.
Hacerlo normalmente violaría el deseo de describir un **Mundo Abierto**.
Por ejemplo, para todos los casos posibles de género humano, el hombre no debe declararse como complemento de la mujer o viceversa.
¿Qué pasa si alguien es ambos o incluso de otro tipo?


```mermaid
 stateDiagram-v2
    direction TB
    bio_object: Objeto_Biológico [superclase]
    state bio_object {
        direction BT
         s --> [*]
        s: "Clase A"
        s: "Persona2"
        s: "Persona3"
        c --> [*]
        c: "(Complemento de la clase A)"
        c: "Persona4"
        c: "Persona5"
        c: "Gato1"
    }
```
