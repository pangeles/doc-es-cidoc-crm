> ### semantic interoperability
>Semantic interoperability means the capability of different information systems to communicate information consistent with the intended meaning. In more detail, the intended meaning encompasses 
>    1 the data structure elements involved, 
>    2 the terminology appearing as data and 
>    3 the identifiers used in the data for factual items such as places, people, objects etc.
>Obviously, communication about data structure must be resolved first. In this case consistent communication means that data can be transferred between data structure elements with the same intended meaning or that data from elements with the same intended meaning can be merged. In practice, the different levels of generalization in different systems do not allow the achievement of this ideal. Therefore, semantic interoperability is regarded as achieved if elements can be found that provide a reasonably close generalization for the transfer or merge. This problem is being studied theoretically as the query containment problem. The CIDOC CRM is only concerned with semantic interoperability on the level of data structure elements. 

### interoperabilidad semántica *(semantic interoperability)*

 La **interoperabilidad** semántica significa la capacidad de diferentes sistemas de información para comunicar información consistente con el significado previsto.
 Más detalladamente, el significado previsto abarca

 1. los elementos de la estructura de datos involucrados,
 2. la terminología que aparece como datos, y
 3. los identificadores utilizados en los datos para elementos fácticos como lugares, personas, objetos, etcétera.

 Obviamente, la comunicación sobre la estructura de datos debe resolverse primero.
 En este caso, la comunicación consistente significa que los datos se pueden transferir entre elementos de la estructura de datos, con el mismo significado previsto, o que datos de elementos con el mismo significado previsto se pueden fusionar.
 En la práctica, los diferentes niveles de generalización en diferentes sistemas no permiten la consecución de este ideal.
 Por lo tanto, la interoperabilidad semántica se considera lograda si se pueden encontrar elementos que proporcionen una generalización razonablemente cercana para la transferencia o fusión.
 Este problema se está estudiando teóricamente como el problema de **contención de las consultas**.
 CIDOC CRM solo se ocupa de la interoperabilidad semántica en el nivel de los elementos de la estructura de datos.
