> ## Objectives of the CIDOC CRM
>The primary role of the CIDOC CRM is to enable the exchange and integration of information from heterogeneous sources for the reconstruction and interpretation of the past at a human scale, based on all kinds of material evidence, including texts, audio-visual material and oral tradition. It starts from, but is not limited to, the needs of museum documentation and research based on museum holdings. It aims at providing the semantic definitions and clarifications needed to transform disparate, localised information sources into a coherent global resource, be it within a larger institution, in intranets or on the Internet, and to make it available for scholarly interpretation and scientific evaluation. These goals determine the constructs and level of detail of the CIDOC CRM.
>More specifically, it defines, in terms of a formal ontology, the **underlying** **semantics** of database **schemata** and **structured** documents used in the documentation of cultural heritage and scientific activities. In particular it defines the semantics related to the study of the past and current state of our world, as it is characteristic for museums, but also or other cultural heritage institutions and disciplines. It does **not** define any of the **terminology** appearing typically as data in the respective data structures; it foresees, however, the characteristic relationships for its use. It does **not** aim at proposing what cultural heritage institutions should document. Rather, it explains the logic of what they actually currently document, and thereby enables **semantic interoperability.**
>The CIDOC CRM intends, moreover, to provide a model of the intellectual structure of the respective kinds of mentioned documentation in logical terms. As such, it has not been optimised for implementation specific storage and processing factors. Actual system implementations may lead to solutions where elements and links between relevant elements of our conceptualizations are no longer explicit in a database or other structured storage system. For instance, the birth event that connects elements such as father, mother, birth date, birth place may not appear in the database, in order to save storage space or response time of the system. The CIDOC CRM provides a conceptual and technical means to explain how such apparently disparate entities are semantically and logically interconnected, and how the ability of the database to answer certain intellectual questions is affected by the omission of such elements and links.
>The CIDOC CRM aims to support the following specific functionalities:
> -   Inform developers of information systems as a guide to good practice in conceptual modelling, in order to effectively structure and relate information assets of cultural documentation.
> -   Serve as a common language for domain experts and IT developers to formulate requirements and to agree on system functionalities with respect to the correct handling of cultural contents.
> -   To serve as a formal language for the identification of common information contents in different data formats; in particular, to support the implementation of automatic data transformation algorithms from local to global data structures without loss of meaning. The latter being useful for data exchange, data migration from legacy systems, data information integration and mediation of heterogeneous sources.
> -   To support associative queries against integrated resources by providing a global model of the basic classes and their associations to formulate such queries.
> -   It is further believed that advanced natural language algorithms and case-specific heuristics can take significant advantage of the CIDOC CRM to resolve free text information into a formal logical form, if that is regarded beneficial. The CIDOC CRM is not thought, however, to be a means to replace scholarly text, rich in meaning, by logical forms, but only a means to identify related data.
>Users of the CIDOC CRM should be aware that the definition of data entry systems requires support of community-specific terminology, guidance to what should be documented and in which sequence, and application-specific consistency controls. The CIDOC CRM does not provide such notions.
>By its very structure and formalism, the CIDOC CRM is extensible and users are encouraged to create extensions for the needs of more specialized communities and applications.

## Objetivos de CIDOC CRM

 El papel principal de CIDOC CRM es permitir el intercambio y la integración de información de fuentes heterogéneas para la reconstrucción e interpretación del pasado a una escala humana,
 con base en todo tipo de evidencia material, incluyendo textos, material audiovisual y tradición oral. Inicia con, pero no se limita a, las necesidades de documentación e investigación de los museos,
 sustentadas en sus bienes. Su objetivo es

 - proporcionar las definiciones semánticas y las aclaraciones necesarias para transformar fuentes de información dispar y locales en un recurso global coherente, ya sea dentro de una gran organización,
 en intranets o en Internet, y ponerlo a disposición para su evaluación científica e interpretación académica. Estos objetivos determinan la construcción y el nivel de detalle de CIDOC CRM.

Más específicamente, CRM define, en términos de una ontología formal, la **semántica subyacente** de los esquemas de bases de datos,
así como los documentos **estructurados** utilizados en la documentación del patrimonio cultural y de las actividades científicas.
En particular, define la semántica relacionada con el estudio de los estados ―pasado y actual― de nuestro mundo, actividad característica de los museos y otras instituciones y disciplinas del patrimonio cultural.
CIDOC CRM:

- **No** define ninguna **terminología** que típicamente aparece en las respectivas estructuras de datos, aunque prevé las relaciones características para su uso.
- **No** propone qué deben documentar las instituciones del patrimonio cultural.

CIDOC CRM, explica la lógica de lo que actualmente documentan las instituciones del patrimonio cultural y, por lo tanto, facilita la **interoperabilidad semántica**.

Además, CIDOC CRM pretende proporcionar, en términos lógicos, un modelo de la estructura intelectual de los respectivos tipos de documentación mencionada.
Pero, como tal, no contempla su optimización en el desarrollo de soluciones de procesamiento y almacenamiento específicas de su implementación.
Las implementaciones reales del sistema pueden conducir a soluciones donde los elementos y los vínculos entre los elementos relevantes de nuestras conceptualizaciones ya no son explícitos en una base de datos u otro sistema de almacenamiento estructurado.
Por ejemplo, el evento  “nacimiento”, que conecta elementos como padre, madre, fecha de nacimiento y lugar de nacimiento, puede no aparecer en la base de datos para ahorrar espacio de almacenamiento o tiempo de respuesta del sistema.
CIDOC CRM proporciona un medio conceptual y técnico para explicar cómo estas entidades, aparentemente dispares, están interconectadas semántica y lógicamente, y cómo la capacidad de la base de datos responde a ciertas preguntas intelectuales que se ven afectadas por la omisión de tales elementos y vínculos.

CIDOC CRM tiene como objetivo soportar las siguientes funcionalidades específicas:

- Servir como guía de buenas prácticas para los desarrolladores de sistemas de información, sobre cómo estructurar y relacionar eficazmente la documentación cultural y su modelado conceptual.
- Servir de lenguaje común para que los expertos en el dominio y los desarrolladores de tecnologías de la información (TIC) formulen requisitos y acuerden las funcionalidades del sistema con respecto al correcto manejo de la información de la información de los contenidos culturales.
- Servir como un lenguaje formal para identificar contenidos de información comunes en diferentes formatos de datos; en particular, para apoyar la implementación de algoritmos de transformación automática de datos, desde estructuras de datos locales a globales, sin pérdida de significado. Estos procesos son útiles para el intercambio de datos, la migración de datos de sistemas heredados, la integración de información y la mediación de fuentes heterogéneas.
- Apoyar las consultas federadas en contra de recursos integrados[^1], proporcionando un modelo global de las clases básicas y sus asociaciones para formular dichas consultas.
- Se cree, de considerarlos beneficiosos, que los algoritmos avanzados de lenguaje natural y la heurística de casos específicos, pueden aprovechar significativamente CIDOC CRM para resolver la información de texto libre de manera lógica y formal. Sin embargo, CIDOC CRM no se propone como un medio para reemplazar el texto académico ―rico en significado― por formas lógicas, sino sólo un medio para identificar datos relacionados.

Los usuarios de CIDOC CRM deben ser conscientes que la definición para “sistemas de entrada de datos” requiere del apoyo de la terminología de cada comunidad específica; orientación sobre lo que debe documentarse y en qué secuencia; así como controles de coherencia específicos de la aplicación. CIDOC CRM no proporciona tales nociones.

Por su propia estructura y formalismo, CIDOC CRM es extensible y anima a los usuarios a crear extensiones para las necesidades de comunidades especializadas y aplicaciones.

| | |
| :--- | :--- |
| *Notas del traductor*  | Comentario sobre acervo/fondo vs solo usar colección.”museum holdings” generó debate sobre porqué no usar colecciones o acervos, y cambiarlo “bien” (sin otro término que lo acompañe) |