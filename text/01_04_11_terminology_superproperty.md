> ### superproperty

>A superproperty is a property that is a generalization of one or more other properties (its subproperties), which means that it subsumes all instances of its subproperties, and that it can also have additional instances that do not belong to any of its subproperties. The intension of the superproperty is less restrictive than any of its subproperties. The subsumption relationship or generalization is the inverse of the IsA relationship or specialization. A superproperty may be a generalization of the inverse of another property.

### superpropiedad *(superproperty)*

Una superpropiedad es una **propiedad** que es una generalización de una o más propiedades (sus **subpropiedades**), lo que significa que subsume a todas las **instancias** de sus subpropiedades y que también puede tener instancias adicionales que no pertenecen a ninguna de sus subpropiedades.
La intensión de la superpropiedad es menos restrictiva que cualquiera de sus subpropiedades.
La relación de subsunción o generalización es el opuesto  de la relación o especialización IsA.
Una superpropiedad puede ser una generalización de la **inversa de** otra propiedad.


| | |
| :--- | :--- |
| *Notas del traductor* | Ha de tenerse en consideración que la frase «semánticamente significativa y gramaticalmente correcta cuando se lee de rango a dominio» sólo es aplicable en el idioma inglés (genera confusión al momento de construir relaciones). |
