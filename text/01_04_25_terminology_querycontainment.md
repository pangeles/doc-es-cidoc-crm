> ### query containment
>Query containment is a problem from database theory: A query X contains another query Y, if for each possible population of a database the answer set to query X contains also the answer set to query Y. If query X and Y were classes, then X would be superclass of Y. 

### contención de las consultas *(query containment)*

 La contención de las consultas es un problema de teoría de bases de datos:
 una consulta X contiene a otra consulta Y;
 si para cada población posible de una base de datos el conjunto de respuestas para la consulta X contiene también el conjunto de respuestas para la consulta Y.
 Si las consultas X y Y fueran clases, entonces X sería **superclase** de Y.
