h # hold content before work with
s/entities\/e.*:# \(.*\)/#\L\1/g; # remove un-needed text and keep title and switch Lower first character
s/\s/_/g; # replace space or tabulation with undercore _
s/\.//g; # remove dot
s/·//g # remove ·
s/['’]//g; # and remove quotes ' or ’
G # append a new line from hold space (memorize at 1st line)
s/entities\/e.*:# \(.*\)/class="w3-bar-item w3-button w3-padding-very-small" title="\1">\1<\/a>/g; # define displayed title and add CSS class and final tag </a>
s/\(^#.*\)\n\(.*\)/<a href="entités\1" \2/g; # merge two lines in one and add begin tag <a href…
