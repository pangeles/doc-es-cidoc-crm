h # hold intial text
s/entities\/e.*:# //g; # remove unnecessary text
s/\(^P.*\)/s\/\1/g; # add /s at the beginning of the line
s/\s/_/g; # replace space by underscore, create dest text
G # add lines from hold space to create source (src) text
#s/entities\/e.*:# \(.*\)/\1/g; # remove unnecessary text and keep necessary text
#s/^s\/\(P.*\)\n\(.*\)/s\/\2\/\1\/g\r/g; # switch texts (src/dest)
