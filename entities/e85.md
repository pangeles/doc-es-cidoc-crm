> # E85 Joining 

> | | |
> | --- | --- |
> | Subclass of: | 	E7 Activity
> | Superclass of: |
> | Scope note: | 	This class comprises the activities that result in an instance of E39 Actor becoming a member of an instance of E74 Group. This class does not imply initiative by either party. It may be the initiative of a third party.<br>Typical scenarios include becoming a member of a social organisation, becoming employee of a company, marriage, the adoption of a child by a family and the inauguration of somebody into an official position. 
> | Examples: |	<br>The election of Sir Isaac Newton as Member of Parliament for the University of Cambridge to the Convention Parliament of 1689 (Gleick,2003)<br>The inauguration of Mikhail Sergeyevich Gorbachev as leader of the Union of Soviet Socialist Republics (USSR) in 1985 (Butson, 1986)<br>The implementation of the membership treaty between EU and Denmark  January 1. 1993
> | In First Order Logic: E85(x) ⊃ E7(x)
> | Properties: P143 joined (was joined by): E39 Actor<br>P144 joined with (gained member by) E74 Group<br>	(P144.1 kind of member: E55_Type)

# E85_Intégration

| | |
|---|---|
| Sous-classe de |  |
| Super-classe de |  |
| Note d’application : |  |
| Exemples : |  |
| Logique du premier ordre : |  |
| Propriétés : |  |

