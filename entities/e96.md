> # E96 Purchase

> | | |
> | --- | --- |
> | Subclass of: | E8_Acquisition 
> | Superclass of: |
> | Scope note: | This class comprises transfers of legal ownership from one or more instances of E39 Actor to one or more different instances of E39 Actor, where the transferring party is completely compensated by the payment of a monetary amount. In more detail, a purchase agreement establishes a fixed monetary obligation at its initialization on the receiving party, to the giving party. An instance of E96 Purchase begins with the contract or equivalent agreement and ends with the fulfilment of all contractual obligations. In the case that the activity is abandoned before both parties have fulfilled these obligations, the activity is not regarded as an instance of E96 Purchase.<br>This class is a very specific case of the much more complex social business practices of exchange of goods and the creation and satisfaction of related social obligations. Purchase activities which define individual sales prices per object can be modelled by instantiating E96 Purchase for each object individually and as part of an overall instance of E96 Purchase transaction.
> | Examples: | the purchase of 10 okka of nails by the captain A. Syrmas on 18th September 1895 in Thessaloniki (Syrmas, 1896)
> | In First Order Logic: | E96(x) ⇒ E8(x)
> | Properties: | P179 had sales price (was sales price of): E97 Monetary Amount

# E96_Achat
 
| | |
|---|---|
| Sous-classe de |  E8_Acquisition
| Super-classe de |  |
| Note d’application : |  Cette classe comprend les transferts légaux de propriété d’une ou plusieurs instances de E39_Acteur·rice·x à une ou plusieurs instances de E39_Acteur·rice·x, lorsque la partie cédante est intégralement indémnisée par le paiement d’un montant financier. De manière plus précise, l’accord d’acquisition/de vente établit une contrepartie financière fixée à l’origine, de la partie acquéreuse à la partie vendeuse. Une instance de E96_Achat débute avec le contrat ou l’accord équivalent et se termine avec l’exécution de toutes les obligations contractuelles. Dans le cas où l’activité est abandonnée avant que les deux parties aient exécuté ces obligations, l’activité n’est pas considérée comme une instance de E96.<br>Cette classe est un cas très particulier au sein des pratiques commerciales sociales beaucoup plus complexes d'échange de biens, de création et de satisfaction des obligations sociales afférentes. Les activités d’achat qui définissent des prix de ventes individuelles par objet peuvent être modélisées par une instanciation de E96_Achat pour chaque objet, individuellement et dans le cadre d'une instance globale de transaction E96_Achat.
| Exemples : | l’achat de 10 oques de clous par le capitaine A. Syrmas le 19/9/1895 à Thessalonique
| Logique du premier ordre : E96(x) ⊃ E8(x)
| Propriétés : P179_a_eu_pour_prix_de_vente_(a_été_le_prix_de_vente_de): E97 Valeur monétaire
