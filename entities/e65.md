> # E65 Creation

> | | |
> | --- | --- |
> | Subclass of: | E7 Activity<br>E63 Beginning of Existence
> | Superclass of: | E83 Type Creation
> | Scope note: | This class comprises events that result in the creation of conceptual items or immaterial products, such as legends, poems, texts, music, images, movies, laws, types etc.
> | Examples: | the framing of the U.S. Constitution (Farrand, 1913)<br>the drafting of U.N. resolution 1441 (United Nations Security Council, 2002)
> | In First Order Logic: | E65(x) ⇒ E7(x)<br>E65(x) ⇒ E63(x)
> | Properties: | P94 has created (was created by): E28 Conceptual Object

# E65_Création

| | |
|---|---|
| Sous-classe de : |  E7_Activité<br>E63_Début_d’existence |
| Super-classe de : | E83 Création de type |
| Note d’application : | Cette classe comprend les événements qui aboutissent à la création d’objets conceptuels ou de produits immatériels, tels que les légendes, les poèmes, les textes, la musique, les images, les films, les lois, les types etc. |
| Exemples : | L’élaboration de la Constitution des États-Unis d’Amérique (Farrand, 1913).<br>La rédaction de la résolution 1441 du Conseil de sécurité des Nations unies (Conseil de sécurité des Nations unies, 2002).  |
| Logique du premier ordre : | E65(x) ⇒ E7(x)<br>E65(x) ⇒ E63(x)  |
| Propriétés : | P94 a créé (a été créé par): E28_Objet_conceptuel  |

