> # E1 CRM Entity test
> 
> | | |
> |---|---|
> | Subclass of: | 
> | Superclass of: | E2 Temporal Entity<br>E52 Time-Span<br>E53 Place<br>E54_Dimension<br>E59 Primitive Value<br>E77 Persistent Item<br>E92 Spacetime Volume
> | Scope note: | This class comprises all things in the universe of discourse of the CIDOC Conceptual Reference Model.<br>It is an abstract concept providing for three general properties:<br>Identification by name or appellation, and in particular by a preferred identifier<br>Classification by type, allowing further refinement of the specific subclass an instance belongs to<br>Attachment of free text and other unstructured data for the expression of anything not captured by formal properties<br>All other classes within the CIDOC CRM are directly or indirectly specialisations of E1 CRM Entity. 
> | Examples: | the earthquake in Lisbon 1755 (E5) (Chester, 2001)
> | In First Order Logic: | E1(x)
> | Properties: | P1 is identified by (identifies): E41_Appellation<br>P2 has type (is type of): E55_Type<br>P3 has note: E62 String<br>(P3.1 has type: E55_Type)<br>P48 has preferred identifier (is preferred identifier of): E42 Identifier<br>P137 exemplifies (is exemplified by): E55_Type<br>(P137.1 in the taxonomic role: E55_Type)|

# E1_Entité_CRM
| | |
|---|---|
| Super-classe de : |  E2_Entité_temporelle ; E52_Laps_de_temps ; E53_Lieu ; E54_Dimension ; E77_Entité_persistante ; E92_Volume_spatio-temporel |
| Note d'application : | Cette classe comprend toutes les choses dans l'univers de discours du Modele Conceptuel de Référence du CIDOC (CIDOC CRM). C'est un concept abstrait conférant trois propriétés générales aux choses : 1. Identification par un nom ou une appellation, et, en particulier, par un identifiant privilégié. 2. Classification par type, permettant d'affiner davantage la sous-classe spécifique à laquelle appartient une instance 3. Association d'un texte libre pour exprimer tout ce qui n'est pas pris en compte par des propriétés formelles. A l'exception de E59_Valeur_primitive, toutes les autres classes du CRM CIDOC sont directement ou indirectement des spécialisations de l'Entité CRM E1. |
| Exemples : | le tremblement de terre de Lisbonne 1755 (E5) (Chester, 2001) |
| Logique du premier ordre : | E1(x) |
| Propriétés : | P1 est identifiée par (identifie) : E41_Appellation ; P2 a pour type (est du type) : E55_Type ; P3_a_pour_note : E62_Chaîne_de_caractères (string) (P3.1_a_pour_type : E55_Type) ; P48_a_pour_identifiant_préférentiel_(est_l’identifiant_préférentiel_de) : E42_Identifiant ; P137 exemplifie (est exemplifié par) : E55_Type (P137.1 pour la fonction taxonomique : E55_Type)|
