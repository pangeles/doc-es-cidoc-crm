> # E39 Actor
>
> | | |
> | --- | --- |
> | Subclass of: | E77 Persistent Item
> | Superclass of: | E21 Person<br>E74 Group
> | Scope note: | This class comprises people, either individually or in groups, who have the potential to perform intentional actions of kinds for which someone may be held responsible.
> | Examples: | London and Continental Railways (E40)<br>the Governor of the Bank of England in 1975 (E21)<br > Sir Ian McKellan (E21) (Gibson, 1986)
> | In First Order Logic: |  E39(x) ⊃ E77(x)
> | Properties: | P74 has current or former residence (is current or former residence of): E53 Place<br>P75 possesses (is possessed by): E30 Right<br>P76 has contact point (provides access to): E41_Appellation

# E39_Acteur·rice·x

| | |
|---|---|
| Sous-classe de : | E77_Entité_persistante |
| Super-classe de : | E21_Personne<br>E74_Groupe |
| Note d’application : | Cette classe comprend des personnes physiques ou morales, individuellement ou en groupes, qui ont la possibilité de réaliser de manière intentionnelle des actions pour lesquelles elles peuvent être tenues pour responsables. |
| Exemples : | London and Continental Railways (E40)<br>le Gouverneur de la Banque d’Angleterre en 1975 (E21)<br>Sir Ian McKellan (E21) (Gibson, 1986)|
| Logique du premier ordre : | E39(x) ⊃ E77(x)|
| Propriétés : | P74 a pour résidence actuelle ou antérieure (est résidence actuelle ou antérieure de): E53_Lieu<br>P75 est détenteur de (est détenu par): E30_Droit<br>P76 a pour coordonnées (permet de contacter): E41_Appellation.|
