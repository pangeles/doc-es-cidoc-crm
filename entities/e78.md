> # E78 Curated Holding

> | | |
> | --- | --- |
> | Subclass of: | 	E24 Physical Human-Made Thing
> | Superclass of: |
> | Scope note: |	This class comprises aggregations of instances of E18 Physical Thing that are assembled and maintained (“curated” and “preserved,” in museological terminology) by one or more instances of E39 Actor over time for a specific purpose and audience, and according to a particular collection development plan. Typical instances of curated holdings are museum collections, archives, library holdings and digital libraries. A digital library is regarded as an instance of E18 Physical Thing because it requires keeping physical carriers of the electronic content.<br>Items may be added or removed from an E78 Curated Holding in pursuit of this plan. This class should not be confused with the E39 Actor maintaining the E78 Curated Holding often referred to with the name of the E78 Curated Holding (e.g. “The Wallace Collection decided…”).<br>Collective objects in the general sense, like a tomb full of gifts, a folder with stamps or a set of chessmen, should be documented as instances of E19 Physical Object, and not as instances of E78 Curated Holding. This is because they form wholes either because they are physically bound together or because they are kept together for their functionality.
> | Examples: | 	<br>the John Clayton Herbarium (Blake, 1918), (Natural History Museum, 2021)<br>the Wallace Collection (Ingamells, 1990)<br>Mikael Heggelund Foslie’s coralline red algae Herbarium at Museum of Natural History and Archaeology, Trondheim, Norway (Woelkerling et al., 2005)<br>The Digital Collections of the Munich DigitiZation Center (MDZ) accessible via https://www.digitale-sammlungen.de/ at least in January 2018.
> | In First Order Logic: E78(x) ⊃ E24(x)
> | Properties: P109 has current or former curator (is current or former curator of): E39 Actor

# E78_Collection

| | |
|---|---|
| Sous-classe de | E24_Chose_matérielle_anthropique
| Super-classe de |
| Note d’application : | Cette classe comprend les agrégations d’instances de E18_Chose_matérielle qui sont assemblées et entretenues (« conservées » et « préservées », selon la terminologie muséale) par une ou plusieurs instances de E39_Acteur·rice·x dans le temps, pour un but et un public spécifiques, et selon un plan particulier de développement de la collection. Les collections de musées, les archives, les fonds de bibliothèques et les bibliothèques numériques sont des exemples typiques de collections conservées. Une bibliothèque numérique est considérée comme une instance de E18_Chose_matérielle parce qu’elle nécessite de conserver les supports physiques du contenu électronique.<br>Des entités peuvent être ajoutées ou retirées d’une E78_Collection au fur et à mesure de son évolution. Cette classe ne doit pas être confondue avec E39_Acteur·rice·x qui conserve la E78_Collection à laquelle on fait souvent référence par le nom de la E78_Collection (par exemple « la Wallace Collection » a décidé…).<br>Les objets collectifs au sens général, comme une tombe pleine de présents, un classeur de timbres ou un jeu d’échec, doivent être documentés comme des instances de E19_Objet_matériel, et non comme des instances de E78_Collection. En effet, ils forment des ensembles car ils sont physiquement liés ou gardés ensemble pour leur fonctionnalité.
| Exemples : | l’herbier John Clayton (Blake, 1918), (Musée d’histoire naturelle, 2021)<br>la Wallace Collection (Ingamells, 1990)<br>algues rouges coralliennes de l’herbier Mikael Heggelund Foslie du Musée d’histoire naturelle et d’archéologie, Trondheim, Norvège (Woelkerling et al., 2005)<br>les collections numériques du Centre de numérisation de Munich (Munich DigitiZation Center, MDZ) accessibles via https://www.digitale-sammlungen.de/ dernière consultation janvier 2018. |
| Logique du premier ordre : | E78(x) ⇒ E24(x) |
| Propriétés : | P109_a_pour_conservateur·rice·x_actuel·le_ou_antérieur·e_(est_conservateur·rice·x_actuel·le_ou_antérieur·e_de) : E39_Acteur·rice·x
