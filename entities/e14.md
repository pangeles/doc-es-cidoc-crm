> # E14 Condition Assessment

> | | |
> | --- | --- |
> | Subclass of: | E13 Attribute Assignment
> | Superclass of: |
> | Scope note: | This class describes the act of assessing the state of preservation of an object during a particular period.<br>The condition assessment may be carried out by inspection, measurement or through historical research. This class is used to document circumstances of the respective assessment that may be relevant to interpret its quality at a later stage, or to continue research on related documents. 
> | Examples: | last year’s inspection of humidity damage to the frescos in the St. George chapel in our village (fictitious)<br>the condition assessment of the endband cores of MS Sinai Greek 418 by Nicholas Pickwoad in November 2003 (Honey & Pickwoad, 2010)<br>the condition assessment of the cover of MS Sinai Greek 418 by Nicholas Pickwoad in November 2003 (Honey & Pickwoad, 2010)
> | In First Order Logic: |  E14(x) ⇒ E13(x)
> | Properties: | P34 concerned (was assessed by): E18 Physical Thing<br>P35 has identified (identified by): Ε3 Condition State

# E14_Évaluation_d’état_physique

| | |
|---|---|
| Sous-classe de | E13_Affectation_d’un_attribut |
| Super-classe de |  |
| Note d’application : | Cette classe décrit l’acte qui consiste à évaluer l’état de conservation d’un objet durant une période donnée.<br>Cette évaluation peut être effectuée par un examen, des mesures ou par une recherche historique. Cette classe est utilisée pour documenter les causes de l’état donné d’un objet, qui peuvent être pertinentes pour interpréter la qualité de l’objet à un moment postérieur, ou pour poursuivre les recherches sur des documents s’y rapportant.<br>|
| Exemples : | L’inspection faite l’an dernier des dommages causés par l’humidité sur les fresques de la chapelle Saint-George de notre village. |
| Logique du premier ordre : | E14(x) ⊃ E13(x) |
| Propriétés : | P34_a_concerné_(a_été_évalué_par): E18_Chose_matérielle<br>P35_a_identifié_(a_été_identifié_par): E3_État_physique |

