> # E67 Birth

> | | |
> | --- | --- |
> | Subclass of: | E63 Beginning of Existence
> | Superclass of: |
> | Scope note: | This class comprises the births of human beings. E67 Birth is a biological event focussing on the context of people coming into life. (E63 Beginning of Existence comprises the coming into life of any living being).<br>Twins, triplets etc. are typically brought into life by the same instance of E67 Birth. The introduction of E67 Birth as a documentation element allows the description of a range of family relationships in a simple model. Suitable extensions may describe more details and the complexity of motherhood with the intervention of modern medicine. In this model, the biological father is not seen as a necessary participant in the birth.
> | Examples: | the birth of Alexander the Great (Stoneman, 2004)
> | In First Order Logic: | E67(x) ⇒ E63(x)
> | Properties: | P96 by mother (gave birth): E21 Person<br>P97 from father (was father for): E21 Person<br>P98 brought into life (was born): E21 Person

# E67_Naissance

| | |
|---|---|
| Sous-classe de | E63_Début_d’existence
| Super-classe de |  |
| Note d’application : | Cette classe comprend les naissances d’êtres humains. E67_Naissance est un événement biologique qui se concentre sur le contexte où les gens viennent au monde. (E63_Début_d’existence comprend la venue au monde de tout être vivant).<br>Les jumeaux, triplets etc. sont en général mis au monde par la même instance de E67_Naissance. L’introduction de E67_Naissance comme élément de documentation permet la description d’un éventail de relations familiales dans un simple modèle. Des extensions adaptées peuvent décrire plus de détails et de complexité le fait de devenir mère avec l’intervention de la médecine moderne. Dans ce modèle, le père biologique n’est pas vu comme un participant nécessaire dans la naissance.  
| Exemples : | La naissance d’Alexandre le Grand (Stoneman, 2004)
| Logique du premier ordre : | E67(x) ⇒ E63(x)
| Propriétés : | P96_de_mère_(a_donné_naissance_à) : E21_Personne<br>P97_de_père_(a_été_père_pour) : E21_Personne<br>P98_a_donné_vie_à_(est_né) : E21_Personne



