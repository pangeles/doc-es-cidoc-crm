> # E12_Production

> | | |
> | --- | --- |
> | Subclass of: | E11_Modification<br>E63 Beginning of Existence
> | Superclass of: |
> | Scope note: | This class comprises activities that are designed to, and succeed in, creating one or more new items.<br>It specializes the notion of modification into production. The decision as to whether or not an object is regarded as new is context sensitive. Normally, items are considered “new” if there is no obvious overall similarity between them and the consumed items and material used in their production. In other cases, an item is considered “new” because it becomes relevant to documentation by a modification. For example, the scribbling of a name on a potsherd may make it a voting token. The original potsherd may not be worth documenting, in contrast to the inscribed one.<br>This entity can be collective: the printing of a thousand books, for example, would normally be considered a single event.<br>An event should also be documented using an instance of E81_Transformation if it results in the destruction of one or more objects and the simultaneous production of others using parts or material from the originals. In this case, the new items have separate identities and matter is preserved, but identity is not.
> | Examples: | the construction of the SS Great Britain (Gregor, 1971)<br>the first casting of the Little Mermaid from the harbour of Copenhagen (Dewey, 2003)<br>Rembrandt’s creating of the seventh state of his etching “Woman sitting half dressed beside a stove”, 1658, identified by Bartsch Number 197 (E12, E65, E81) (Hind, 1923)
> | In First Order Logic: |  E12(x) ⇒ E11(x)<br>E12(x) ⇒ E63(x)
> | Properties: | P108 has produced (was produced by): E24 Physical Human-Made Thing<br>P186 produced thing of product type (is produced by): E99 Product Type

# E12_Production

| | |
|---|---|
| Sous-classe de | E11_Modification<br>E63_Début_d’existence |
| Super-classe de |  |
| Note d’application : | Cette classe comprend des activités qui sont ont pour but et qui parviennent à créer un ou plusieurs nouveaux items.<br><br>Elle spécialise la notion de modification en celle de production. Décider si un objet est considéré comme nouveau relève du contexte. Normalement,les items sont considérés comme "nouveaux" s'il n'y a pas de similarité entre eux et les items et les matériaux utilisés pour leur production. Dans d'autres cas, un item est considéré comme "nouveau" parce qu'il dévient pertinent pour la documentation par le biais d'une modification. Par exemple, l'inscription d'un nom sur un tesson peut être transformée en bulletin de vote. Le tesson d'origine ne vaut peut-être pas la peine d'être documenté, contrairement à celui qui est marqué par l'inscription.<br><br>Cette entité peut être collective: par exemple, l'impression de mille livres serait normalement considérée comme un seul événement. Il convient également de faire appel à E81_Transformation pour documenter un événement s'il débouche sur la destruction d'un ou de plusieurs objets et simultanément sur la production de nouveaux objets en utilisant des parties ou des matériaux des objets de départ. Dans ce cas, les nouveaux items ont des identités différentes; la matière est préservée, mais l'identité non.<br>|
| Exemples : | la construction du navire SS Great Britain (Gregor, 1971)<br> le premier moulage de la Petite Sirène au port de Copenhague (Dewey, 2003)<br> Rembrandt créant le septième état de sa gravure “Woman sitting half dressed beside a stove”, 1658, identifiée par le numéro de catalogue Bartsch 197 (E12,E65,E81) (Hind, 1923)|
| Logique du premier ordre : | E12(x) ⊃ E11(x)<br>E12(x) ⊃ E63(x) |
| Propriétés : | P108 a produit (a été produit par): E24_Chose_matérielle_anthropique manuellement[Note: à choisir par rapport à la première version: "Chose Matérielle Fabriquée"]<br>P186 a produit un chose d'un type de produit [Note: Traduction à confirmer], (est produit par): E99_Type_de_produit |
