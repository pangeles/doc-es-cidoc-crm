> # E30 Right

> | | |
> | --- | --- |
> | Subclass of: |	E89 Propositional Object
> | Superclass of: |
> | Scope Note: |	This class comprises legal privileges concerning material and immaterial things or their derivatives.<br>These include reproduction and property rights. 
> | Examples: | copyright held by ISO on ISO/CD 21127<br>ownership of the “Mona Lisa” by the museum of the Louvre, Paris, France<br><br>
> | In First Order Logic: | E30(x) ⇒ E89(x)
> | Properties: |


# E30_Droit

| | |
|---|---|
| Sous-classe de |  |
| Super-classe de |  |
| Note d’application : | Cette classe comprend les droits exclusifs se rapportant aux choses matérielles et immatérielles ou à leurs dérivés.<br>Cela recouvre notamment les droits de reproduction et de propriété. |
| Exemples : | droits d’auteur détenus par ISO sur ISO/CD 21127<br>propriété de “Mona Lisa” par le Louvre, Paris, France|
| Logique du premier ordre : | E30(x) ⊃ E89(x)|
| Propriétés : |  |

