> # E95 Spacetime Primitive 

> | | |
> | --- | --- |
> | Subclass of: |	E41_Appellation<br>E59 Primitive Value
> | Superclass of: |	
> | Scope Note: |	This class comprises instances of E59 Primitive Value for spacetime volumes that should be implemented with appropriate validation, precision and reference systems to express geometries being limited and varying over time on or relative to Earth, or any other stable constellations of matter, relevant to cultural and scientific documentation. An instance of E95 Spacetime Primitive may consist of one expression including temporal and spatial information such as in GML or a different form of expressing spacetime in an integrated way such as a formula containing all 4 dimensions.<br>An instance of E95 Spacetime Primitive defines an instance of E92 Spacetime Volume in the sense of a declarative spacetime volume as defined in CRMgeo (Doerr & Hiebel 2013), which means that the identity of the instance of E92 Spacetime Volume is derived from its geometric and temporal definition. This declarative spacetime volume allows for the application of all E92 Spacetime Volume properties to relate phenomenal spacetime volumes of periods and physical things to propositions about their spatial and temporal extents.<br>Instances of E92 Spacetime Volume defined by P169 that use different spatiotemporal referring systems are always regarded as different instances of the E92 Spacetime Volume.<br>It is possible for a spacetime volume to be defined by phenomena causal to it, such as an expanding and declining realm, a settlement structure or a battle, or other forms of identification rather than by an instance of E95 Spacetime Primitive. Any spatiotemporal approximation of such a phenomenon by an instance of E95 Spacetime Primitive constitutes an instance of E92 Spacetime Volume in its own right.<br>E95 Spacetime Primitive is not further elaborated upon within this model. Compatibility with OGC standards are recommended.
> | Examples: Spatial and temporal information in KML for the maximum extent of the Byzantine Empire ``` <Placemark><br>	<name> Byzantine Empire </name><br>	<styleUrl>#style_1</styleUrl><br>	<TimeSpan><br>		<begin>330</begin><br>		<end>1453</end><br>	</TimeSpan><br><Polygon><altitudeMode>clampToGround</altitudeMode><outerBoundaryIs><LinearRing><br><coordinates>18.452787460,40.85553626,0 17.2223187,40.589098,........0 17.2223,39.783<br></coordinates><br></Polygon><br></Placemark> ```
> | In First Order Logic: E95(x) ⊃ E41(x)<br>E95(x) ⊃ E59(x)
> | Properties: P169 defines spacetime volume (spacetime volume  is defined by): E92 Spacetime Volume

# E95

| | |
|---|---|
| Sous-classe de |  |
| Super-classe de |  |
| Note d’application : |  |
| Exemples : |  |
| Logique du premier ordre : |  |
| Propriétés : |  |

