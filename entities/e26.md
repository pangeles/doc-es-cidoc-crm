> # E26 Physical Feature 

> | | |
> | --- | --- |
> | Subclass of: | E18 Physical Thing
> | Superclass of: | E25 Human-Made Feature<br>E27_Site
> | Scope Note: | This class comprises identifiable features that are physically attached in an integral way to particular physical objects.<br>Instances of E26 Physical Feature share many of the attributes of instances of E19 Physical Object. They may have a one-, two- or three-dimensional geometric extent, but there are no natural borders that separate them completely in an objective way from the carrier objects. For example, a doorway is a feature but the door itself, being attached by hinges, is not.<br>Instances of E26 Physical Feature can be features in a narrower sense, such as scratches, holes, reliefs, surface colours, reflection zones in an opal crystal or a density change in a piece of wood. In the wider sense, they are portions of particular objects with partially imaginary borders, such as the core of the Earth, an area of property on the surface of the Earth, a landscape or the head of a contiguous marble statue. They can be measured and dated, and it is sometimes possible to state who or what is or was responsible for them. They cannot be separated from the carrier object, but a segment of the carrier object may be identified (or sometimes removed) carrying the complete feature.<br>This definition coincides with the definition of "fiat objects" (Smith & Varzi, 2000, pp.401-420), with the exception of aggregates of “bona fide objects”. 
> | Examples: | the cave of Dirou, Mani, Greece (Psimenos. 2005)<br>the temple in Abu Simbel before its removal, which was carved out of solid rock (E25) (Hawass, 2000)<br>Albrecht Duerer’s signature on his painting of Charles the Great (E25) (Strauss, 1974)<br>the damage to the nose of the Great Sphinx in Giza (Temple, 2009)<br>Michael Jackson’s nose prior to plastic surgery
> | In First Order Logic: | E26(x) ⇒ E18(x)
> | Properties: |


# E26_Caractéristique_matérielle

| | |
|---|---|
| Sous-classe de | E18_Chose_matérielle |
| Super-classe de | E25_Caractéristique_anthropique<br>E27_Site |
| Note d’application : | Cette classe comprend les caractéristiques identifiables qui sont physiquement attachées de manière indissociable à des objets physiques particuliers.<br>Les instances de E26_Caractéristique_matérielle partagent de nombreux attributs avec les instances de E19_Objet_matériel. Elles peuvent avoir une étendue géométrique à une, deux ou trois dimensions, mais il n’existe pas de limites naturelles qui les séparent complètement de manière objective des objets qui les portent. Par exemple, une embrasure de porte est une caractéristique, mais la porte elle-même, fixée par des charnières, ne l’est pas.<br>Les instances de E26_Caractéristique_matérielle peuvent être des caractéristiques dans un sens plus étroit, telles que des rayures, des trous, des reliefs, des couleurs de surface, des reflets dans un cristal opale ou un changement de densité dans un morceau de bois. Plus largement, il s’agit de portions particulieres d’objets ayant des limites en partie imaginaires, tels que le noyau de la Terre, la surface d’une propriété foncière, un paysage ou la tête d’une statue de marbre. Elles peuvent être mesurées et datées, et il est parfois possible de spécifier qui ou quoi en est ou en était responsable. Elles ne peuvent pas être séparées de l’objet qui les porte, mais une partie de cet objet peut être identifiée (ou parfois enlevée) et porter la caractéristique entière.<br>Cette définition coïncide avec la définition des "_fiat objects_" (Smith & Varzi, 2000, pp.401-420), à l’exception des agrégats "_bona fide objects_". |
| Exemples : | la grotte de Dirou, Mani, Grèce (Psimenos, 2005)<br>le temple d’Abu Simbel taillé dans la roche, avant son déménagement (Hawass, 2000)<br>la signature d’Albrecht Dürer sur sa peinture de Charles le Grand (E25) (Strauss, 1974)<br>les détériorations du nez du Grand Sphinx à Gizeh (Temple, 2009)<br>le nez de Michael Jackson avant sa chirurgie plastique |
| Logique du premier ordre : E26(x) ⇒ E18(x) |
| Propriétés : |  |

