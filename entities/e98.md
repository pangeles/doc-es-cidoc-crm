> # E98 Currency

> | | |
> | --- | --- |
> | Subclass of: | E58 Measurement Unit
> | Superclass of: |
> | Scope note: | This class comprises the units in which a monetary system, supported by an administrative authority or other community, quantifies and arithmetically compares all monetary amounts declared in the unit. The unit of a monetary system must describe a nominal value which is kept constant by its administrative authority and an associated banking system if it exists, and not by market value. For instance, one may pay with grams of gold, but the respective monetary amount would have been agreed as the gold price in US dollars on the day of the payment. Under this definition, British Pounds, U.S. Dollars, and European Euros are examples of currency, but “grams of gold” is not. One monetary system has one and only one currency. Instances of this class must not be confused with coin denominations, such as “Dime” or “Sestertius”. Non-monetary exchange of value in terms of quantities of a particular type of goods, such as cows, do not constitute a currency.	
> | Examples: | “As” [Roman mid republic]<br>“Euro”, (Temperton, 1997)<br>“US Dollar” (Rose, 1978)
> | In First Order Logic: | E98(x) ⇒ E58(x)
> | Properties: |

# E98_Devise

| | |
|---|---|
| Sous-classe de | E58_Unité_de_mesure |
| Super-classe de |  |
| Note d’application : | Cette classe comprend les unités dans lesquelles un système monétaire, maintenu par une autorité administrative ou d'autres communautés, quantifie et compare de façon arithmétique tous les montants monétaires déclarés dans l'unité. L’unité d’un système monétaire doit décrire une valeur nominale qui doit être maintenue constante par son autorité administrative et le système bancaire associé s’il existe, et non la valeur du marché. Par exemple, on peut payer en grammes d’or mais la valeur monétaire correspondante sera établie en fonction du prix de l’or en dollars américains au jour du paiement. Selon cette définition, la livre britannique, le dollar américain ou l’euro européen sont des exemples de devise mais des « grammes d’or » ne le sont pas. Un système monétaire a une seule et unique devise. Les instances de cette classe ne doivent pas être confondues avec la dénomination des monnaies, comme « dime » ou « sesterce ». Les échanges de valeurs non monétaires de quantités de biens spécifiques, comme des vaches, n’en font pas une devise. |
| Exemples : | « As » [Roman mid republic]<br>« Euro », (Temperton, 1997)<br>« US Dollar » (Rose, 1978) |
| Logique du premier ordre : | E98(x) ⇒ E58(x) |
| Propriétés : |  |

