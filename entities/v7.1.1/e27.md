> # E27 Site

> | | |
> | --- | --- |
> | Subclass of: | E26 Physical Feature
> | Superclass of: |
> | Scope Note: | This class comprises pieces of land or sea floor. <br> In contrast to the purely geometric notion of E53 Place, this class describes constellations of matter on the surface of the Earth or other celestial body, which can be represented by photographs, paintings and maps.<br> Instances of E27 Site are composed of relatively immobile material items and features in a particular configuration at a particular location. 
> | Examples: | the Amazon river basin (Hegen, 1966) <br> Knossos (Evans, 1921) <br> the Apollo 11 landing site (Siegler and Smrekar, 2014) <br> Heathrow Airport (Wicks, 2014) <br> the submerged harbour of the Minoan settlement of Gournia, Crete (Watrous, 2012) <br> the island of Crete
> | In First Order Logic: | E27(x) ⇒ E26(x)
> | Properties: |
