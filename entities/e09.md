> # E9 Move
>
> | | |
> |---|---|
> | Subclass of: | E7 Activity
> | Superclass of: |
> | Scope note: | This class comprises changes of the physical location of the instances of E19 Physical Object.<br>Note, that the class E9 Move inherits the property P7 took place at (witnessed): E53 Place. This property should be used to describe the trajectory or a larger area within which a move takes place, whereas the properties P26 moved to (was destination of), P27 moved from (was origin of) describe the start and end points only. Moves may also be documented to consist of other moves (via P9 consists of (forms part of)), in order to describe intermediate stages on a trajectory. In that case, start and end points of the partial moves should match appropriately between each other and with the overall event.
> | Examples: | the relocation of London Bridge from the UK to the USA. (Wildfang, 2005)<br>the movement of the exhibition “Tutankhamun: Treasures of the Golden Pharaoh” between 15th September and 2nd November 2019.
> | In First Order Logic: |  E9(x) ⇒ E7(x)
> | Properties: | P25 moved (moved by): E19 Physical Object<br>P26 moved to (was destination of): E53 Place<br>P27 moved from (was origin of): E53 Place

# E9_Déplacement

| | |
|---|---|
| Sous-classe de | E7_Activité
| Super-classe de |  |
| Note d’application : | Cette classe comprend les changements de d’emplacement matériel des instances de E19_Objet_matériel.<br>A noter, la classe E9_Déplacement hérite de la propriété P7 a eu lieu dans (a été témoin de) : E53_Lieu. Cette propriété devrait être utilisée pour décrire la trajectoire ou une plus large zone dans laquelle un déplacement a lieu, alors que les propriétés P26_a_déplacé_vers_(a_été_destination_de) et P27_a_déplacé_de_(a_été_point_de_départ_de) décrivent les points de départ et d’arrivée uniquement. Les déplacements peuvent également être décrits comme consistant en d'autres déplacements (via P9 consiste en (fait partie de)), dans le but de décrire les étapes intermédiaires d’une trajectoire. Dans ce cas, les points de départ et d’arrivée des déplacements partiels devront correspondre de manière adéquate entre eux et avec l’évènement dans son ensemble. |
| Exemples : | le déménagement du pont de Londres depuis le Royaume Uni vers les États-Unis d’Amérique (Wildfang, 2005)<br>Le déplacement de l’exposition « Tutânkhamon : le Trésor du Pharaon » entre le 15 septembre et le 2 novembre 2019. |
| Logique du premier ordre : | E9(x) ⇒ E7(x)
| Propriétés : |  P25_a_déplacé_(a_été_déplacé_par): E19_Objet_matériel<br>P26_a_déplacé_vers_(a_été_destination_de) : E53_Lieu<br>P27_a_déplacé_de_(a_été_point_de_départ_de) : E53_Lieu
