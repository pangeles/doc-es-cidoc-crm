> # E79 Part Addition 

> | | |
> | --- | --- |
> | Subclass of: | 	E11_Modification
> | Superclass of: |
> | Scope note: | 	This class comprises activities that result in an instance of E24 Physical Human-Made Thing being increased, enlarged or augmented by the addition of a part.<br>Typical scenarios include the attachment of an accessory, the integration of a component, the addition of an element to an aggregate object, or the accessioning of an object into a curated instance of E78 Curated Holding. Objects to which parts are added are, by definition, human-made, since the addition of a part implies a human activity. Following the addition of parts, the resulting human-made assemblages are treated objectively as single identifiable wholes, made up of constituent or component parts bound together either physically (for example the engine becoming a part of the car), or by sharing a common purpose (such as the 32 chess pieces that make up a chess set). This class of activities forms a basis for reasoning about the history and continuity of identity of objects that are integrated into other objects over time, such as precious gemstones being repeatedly incorporated into different items of jewellery, or cultural artifacts being added to different museum instances of E78 Curated Holding over their lifespan.
> | Examples: |	<br>the setting of the koh-i-noor diamond into the crown of Queen Elizabeth the Queen Mother (Dalrymple, 2017)<br>the addition of the painting “Room in Brooklyn” by Edward Hopper to the collection of the Museum of Fine Arts, Boston
> | In First Order Logic: E79(x) ⊃ E11(x)
> | Properties: P110 augmented (was augmented by): E24 Physical Human-Made Thing<br>P111 added (was added by): E18 Physical Thing

# E79_Ajout_d'élément

| | |
|---|---|
| Sous-classe de | E11_Modification
| Super-classe de |  |
| Note d’application : | Cette classe comprend les activités qui résultent en une instance de E24_Chose_matérielle_anthropique étendue, élargie ou augmentée par l’ajout d’une partie.<br>Les scenarii courants incluent l’attache d’un accessoire, l’intégration d’un composant, l’addition d’un élément à un objet agrégat, ou le passage d’un objet en une instance de E78_Collection. Les objets dont les parts sont ajoutées sont –– soit de manière physique (par exemple le moteur devenant une partie de la voiture), soit en partageant un objectif commun (telles que les 32 pièces composant un jeu d’échec). Cette classe d’activités forme la base logique pour l’histoire et la continuité d’identité des objets qui sont intégrés dans d’autres objets au cours du temps, comme des pierres précieuses qui sont intégrées dans différents bijoux, ou des biens patrimoniaux qui sont ajoutés à différentes instances de musées E78_collection au cours de leur durée de vie.   
| Exemples : | La mise en place du diamant koh-i-noor dans la couronne de la Reine Elizabeth, la reine-mère (Dalrymple, 2017)<br>L’addition de la toile “Room in Brooklyn” d’Edward Hopper à la collection du Museum of Fine Arts de Boston.
| Logique du premier ordre : E79(x) ⊃ E11(x)
| Propriétés : P110_a_augmenté_(a_été_augmenté_par) : E24_Chose_matérielle_anthropique<br>P111_a_ajouté_(a_été_ajouté·e_par) : E18_Chose_matérielle

