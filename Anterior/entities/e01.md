> # E1 CRM Entity
> 
> | | |
> |---|---|
> | Subclass of: | |
> | Superclass of: | E2 Temporal Entity<br>E52 Time-Span<br>E53 Place<br>E54 Dimension<br>E59 Primitive Value<br>E77 Persistent Item<br>E92 Spacetime Volume| 
> | Scope note: |  This class comprises all things in the universe of discourse of the CIDOC Conceptual Reference Model. <br>It is an abstract concept providing for three general properties:<br>- Identification by name or appellation, and in particular by a preferred identifier<br>- Classification by type, allowing further refinement of the specific subclass an instance belongs to <br>- Attachment of free text and other unstructured data for the expression of anything not captured by formal properties<br>All other classes within the CIDOC CRM are directly or indirectly specialisations of E1 CRM Entity.| 
> | Examples: | - the earthquake in Lisbon 1755 (E5) (Chester, 2001)|
> | In First Order Logic: | E1(x) |
> | Properties: | P1 is identified by (identifies): E41 Appellation<br>P2 has type (is type of): E55 Type<br>P3 has note: E62 String<br>	(P3.1 has type: E55 Type)<br>P48 has preferred identifier (is preferred identifier of): E42 Identifier<br>P137 exemplifies (is exemplified by): E55 Type<br>	(P137.1 in the taxonomic role: E55 Type)|

# E1_Entidad_CRM
| | |
|---|---|
| Superclase de: | E2_Entidad_Temporal<br>E52_Periodo_de_Tiempo<br>E53_Lugar<br>E54_Dimensiones<br>E59_Valor_Primitivo<br>E77_Ítem Persistente<br>E92_Volumen_del_Espaciotiempo|
| Nota de alcance: | Esta clase comprende todas las cosas del universo del discurso del Modelo de Referencia Conceptual CIDOC. <br>Se trata de un concepto abstracto que proporciona tres propiedades generales:<br><br>- Identificación por nombre o denominación, y en particular por un identificador preferente<br>- Clasificación por tipo, que permite afinar la subclase específica a la que pertenece una instancia <br>- Incorporación de texto libre y otros datos no estructurados para la expresión de todo lo que no se recoge en las propiedades formales<br>Todas las demás clases dentro de CIDOC CRM son directa o indirectamente especializaciones de E1 CRM Entity. |
| Ejemplos: |  - el terremoto de Lisboa de 1755 (E5) (Chester, 2001)|
| En la Lógica de Primer Orden: | E1(x) |
| Propiedades: | P1 is identified by (identifies): E41 Appellation<br>P2 has type (is type of): E55 Type<br>P3 has note: E62 String<br>	(P3.1 has type: E55 Type)<br>P48 has preferred identifier (is preferred identifier of): E42 Identifier<br>P137 exemplifies (is exemplified by): E55 Type<br>	(P137.1 in the taxonomic role: E55 Type)|
