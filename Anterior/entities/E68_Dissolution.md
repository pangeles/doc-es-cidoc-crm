﻿**E68 Dissolution**	[E68 Disolución]

Subclase de:

[E64](https://docs.google.com/document/d/1APuQ_Kpn_r8ISTTrfqU32d7jzm6IYbzw3GQwQyhG7D4/edit#heading=h.4anzqyu) End of Existence	[E64 Fin de la existencia]

Nota de alcance

Esta clase comprende los eventos que resultan en la terminación formal o informal de una instancia de E74 Group. 

Si la disolución fue deliberada, el evento Disolución también debe ser instanciado como una instancia de E7 Activity.

Ejemplos: 

- la caída del Imperio Romano (Whittington, 1964)
- la liquidación de Enron Corporation (Atlas, 2001)

En la Lógica de Primer Orden:

E68(x) ⇒ E64(x)

Propiedades:

[P99](https://docs.google.com/document/d/1APuQ_Kpn_r8ISTTrfqU32d7jzm6IYbzw3GQwQyhG7D4/edit#heading=h.zdd80z) dissolved (was dissolved by): [E74](https://docs.google.com/document/d/1APuQ_Kpn_r8ISTTrfqU32d7jzm6IYbzw3GQwQyhG7D4/edit#heading=h.4fsjm0b) Group


