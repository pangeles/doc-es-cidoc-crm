> # E3 Condition State
>
> | | |
> |---|---|
>| Subclass of: | E2 Temporal Entity
> | Superclass of: |
> | Scope note: | This class comprises the states of objects characterised by a certain condition over a time-span.<br>An instance of this class describes the prevailing physical condition of any material object or feature during a specific instance of E52 Time Span. In general, the time-span for which a certain condition can be asserted may be shorter than the real time-span, for which this condition held.<br>The nature of that condition can be described using P2 has type. For example, the instance of E3 Condition State “condition of the SS Great Britain between 22 September 1846 and 27 August 1847” can be characterized as an instance “wrecked” of E55 Type.  
> | Examples: | - the "reconstructed" state of the “Amber Room” in Tsarskoje Selo from summer 2003 until now (Owen, 2009)<br> - the "ruined" state of Peterhof Palace near Saint Petersburg from 1944 to 1946 (Maddox, 2015)<br> - the state of my turkey in the oven at 14:30 on 25 December, 2002 [P2 has type: E55_Type “still not cooked”] (fictitious)<br> - the topography of the leaves of Sinai Printed Book 3234.2361 on the 10th of July 2007 [described as: of type "cockled"] (fictitious)
> | In First Order Logic: | E3(x) ⇒ E2(x)
> | Properties: | P5 consists of (forms part of): Ε3 Condition State |

# E3_Estado_Físico

| | |
|---|---|
| Subclase de: | E2_Entidad_Temporal  |   	
| Nota de alcance: | Esta clase comprende los estados de los objetos caracterizados por una determinada condición durante un lapso de tiempo.<br>Una instancia de esta clase describe la condición física predominante de cualquier objeto o característica material durante una instancia específica de E52 Time Span. En general, el lapso de tiempo para el que se puede afirmar una determinada condición puede ser más corto que el lapso de tiempo real en el que se mantuvo dicha condición.<br>La naturaleza de esa condición puede describirse mediante P2 has type. Por ejemplo, la instancia de E3 Condition State "estado del SS Great Britain entre el 22 de septiembre de 1846 y el 27 de agosto de 1847" puede caracterizarse como una instancia del E55 Type «naufragio». |  
| Exemples : | - el estado "reconstruido" de la "Sala de Ámbar" en Tsarskoje Selo desde el verano de 2003 hasta ahora (Owen, 2009)<br/>  - el estado de "ruina" del Palacio de Peterhof, cerca de San Petersburgo, entre 1944 y 1946 (Maddox, 2015)<br/>  - el estado de mi pavo en el horno desde las 14:30 del 25 de diciembre de 2002 [P2 has type: E55 Type "aún no cocinado"] (ficticio)<br/>  - la topografía de las hojas del Libro impreso Sinaí 3234.2361 el 10 de julio de 2007 [descrito como: del tipo "arrugado"] (ficticio)|
| En la Lógica de Primer Orden: | E3(x) ⊃ E2(x) |
| Propiedades: | P5 consists of (forms part of): Ε3 Condition State|
