```mermaid
classDiagram
direction TB
E13  <|-- E14 : IsA
E14 : Condition Assessment
E14 :  3-Traduit en attente de revision (63 mots)
E14 <.. P34 : has domain
P34 : concerned 
P34 :  3-Traduit en attente de revision (56 mots)
E14 <.. P35 : has domain
P35 : has identified 
P35 :  3-Traduit en attente de revision (21 mots)
E13  <|-- E15 : IsA
E15 : Identifier Assignment
E15 :  4-En cours de revision (211 mots)
E15 <.. P142 : has domain
P142 : used constituent 
P142 :  3-Traduit en attente de revision (31 mots)
E15 <.. P37 : has domain
P37 : assigned 
P37 :  3-Traduit en attente de revision (39 mots)
E15 <.. P38 : has domain
P38 : deassigned 
P38 :  3-Traduit en attente de revision (56 mots)
E13  <|-- E16 : IsA
E16 : Measurement
E16 :  3-Traduit en attente de revision (400 mots)
E16 <.. P39 : has domain
P39 : measured 
P39 :  5-Valide (51 mots)
E16 <.. P40 : has domain
P40 : observed dimension 
P40 :  3-Traduit en attente de revision (103 mots)
E13  <|-- E17 : IsA
E17 : Type Assignment
E17 :  3-Traduit en attente de revision (76 mots)
E17 <.. P41 : has domain
P41 : classified 
P41 :  2-En cours de traduction (77 mots)
E17 <.. P42 : has domain
P42 : assigned 
P42 :  2-En cours de traduction (111 mots)
```
