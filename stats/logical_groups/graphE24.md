```mermaid
classDiagram
direction TB
E24  <|-- E22 : IsA
E24  <|-- E25 : IsA
E25 : HumanMade Feature
E25 :  4-En cours de revision (35 mots)
E24  <|-- E78 : IsA
E78 : Curated Holding
E78 :  3-Traduit en attente de revision (189 mots)
E78 <.. P109 : has domain
P109 : has current or former curator 
P109 :  3-Traduit en attente de revision (50 mots)
```
