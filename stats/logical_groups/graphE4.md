```mermaid
classDiagram
direction TB
E4  <|-- E5 : IsA
E5 : Event
E5 :  5-Valide (363 mots)
E5 <.. P12 : has domain
P12 : occurred in the presence of 
P12 :  4-En cours de revision (168 mots)
E5 <.. P11 : has domain
P11 : had participant 
P11 :  5-Valide (147 mots)
```
