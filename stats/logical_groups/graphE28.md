```mermaid
classDiagram
direction TB
E28  <|-- E90 : IsA
E28  <|-- E89 : IsA
E89 : Propositional Object
E89 :  3-Traduit en attente de revision (117 mots)
E89 <.. P67 : has domain
P67 : refers to 
P67 :  1-A traduire (78 mots)
E89 <.. P129 : has domain
P129 : is about 
P129 :  1-A traduire (56 mots)
E89 <.. P148 : has domain
P148 : has component 
P148 :  1-A traduire (30 mots)
E28  <|-- E55 : IsA
E55 : Type
E55 :  5-Valide (101 mots)
E55 <.. P127 : has domain
P127 : has broader term 
P127 :  3-Traduit en attente de revision (54 mots)
E55 <.. P150 : has domain
P150 : defines typical parts of 
P150 :  3-Traduit en attente de revision (77 mots)
```
