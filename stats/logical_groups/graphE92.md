```mermaid
classDiagram
direction TB
E92  <|-- E4 : IsA
E92  <|-- E93 : IsA
E93 : Presence
E93 :  1-A traduire (183 mots)
E93 <.. P166 : has domain
P166 : was a presence of 
P166 :  1-A traduire (43 mots)
E93 <.. P164 : has domain
P164 : is temporally specified by 
P164 :  1-A traduire (144 mots)
E93 <.. P167 : has domain
P167 : was within 
P167 :  1-A traduire (107 mots)
E93 <.. P195 : has domain
P195 : was a presence of 
P195 :  1-A traduire (101 mots)
E93 <.. P197 : has domain
P197 : covered parts of 
P197 :  1-A traduire (146 mots)
```
