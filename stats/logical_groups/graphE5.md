```mermaid
classDiagram
direction TB
E5  <|-- E7 : IsA
E7 : Activity
E7 :  5-Valide (58 mots)
E7 <.. P14 : has domain
P14 : carried out by 
P14 :  3-Traduit en attente de revision (42 mots)
E7 <.. P16 : has domain
P16 : used specific object 
P16 :  3-Traduit en attente de revision (140 mots)
E7 <.. P33 : has domain
P33 : used specific technique 
P33 :  3-Traduit en attente de revision (86 mots)
E7 <.. P15 : has domain
P15 : was influenced by 
P15 :  3-Traduit en attente de revision (41 mots)
E7 <.. P17 : has domain
P17 : was motivated by 
P17 :  3-Traduit en attente de revision (46 mots)
E7 <.. P134 : has domain
P134 : continued 
P134 :   (75 mots)
E7 <.. P19 : has domain
P19 : was intended use of 
P19 :  3-Traduit en attente de revision (65 mots)
E7 <.. P20 : has domain
P20 : had specific purpose 
P20 :  3-Traduit en attente de revision (106 mots)
E7 <.. P21 : has domain
P21 : had general purpose 
P21 :  3-Traduit en attente de revision (68 mots)
E7 <.. P125 : has domain
P125 : used object of type 
P125 :  3-Traduit en attente de revision (74 mots)
E7 <.. P32 : has domain
P32 : used general technique 
P32 :  3-Traduit en attente de revision (63 mots)
E7 <.. P134i : has domain
P134i : was continued by
P134i :   (75 mots)
E5  <|-- E63 : IsA
E63 : Beginning of Existence
E63 :  2-En cours de traduction (52 mots)
E63 <.. P92 : has domain
P92 : brought into existence 
P92 :  1-A traduire (55 mots)
E5  <|-- E64 : IsA
E64 : End of Existence
E64 :  4-En cours de revision (79 mots)
E64 <.. P93 : has domain
P93 : took out of existence 
P93 :  1-A traduire (133 mots)
```
