```mermaid
classDiagram
direction TB
E77  <|-- E70 : IsA
E70 : Thing
E70 :  4-En cours de revision (65 mots)
E70 <.. P43 : has domain
P43 : has dimension 
P43 :  1-A traduire (126 mots)
E70 <.. P101 : has domain
P101 : had as general use 
P101 :  3-Traduit en attente de revision (155 mots)
E70 <.. P130 : has domain
P130 : shows features of 
P130 :  1-A traduire (269 mots)
E77  <|-- E39 : IsA
E39 : Actor
E39 :  5-Valide (27 mots)
E39 <.. P74 : has domain
P74 : has current or former residence 
P74 :  1-A traduire (41 mots)
E39 <.. P75 : has domain
P75 : possesses 
P75 :  1-A traduire (26 mots)
E39 <.. P76 : has domain
P76 : has contact point 
P76 :  1-A traduire (36 mots)
```
