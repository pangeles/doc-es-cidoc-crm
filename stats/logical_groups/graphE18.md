```mermaid
classDiagram
direction TB
E18  <|-- E19 : IsA
E19 : Physical Object
E19 :  5-Valide (128 mots)
E19 <.. P55 : has domain
P55 : has current location 
P55 :  5-Valide (137 mots)
E19 <.. P54 : has domain
P54 : has current permanent location 
P54 :  5-Valide (106 mots)
E19 <.. P57 : has domain
P57 : has number of parts
P57 :  5-Valide (110 mots)
E19 <.. P56 : has domain
P56 : bears feature 
P56 :  5-Valide (161 mots)
E18  <|-- E24 : IsA
E24 : Physical HumanMade Thing
E24 :  5-Valide (252 mots)
E24 <.. P62 : has domain
P62 : depicts 
P62 :  5-Valide (131 mots)
E24 <.. P65 : has domain
P65 : shows visual item 
P65 :  5-Valide (226 mots)
E18  <|-- E26 : IsA
E26 : Physical Feature
E26 :  5-Valide (224 mots)
```
