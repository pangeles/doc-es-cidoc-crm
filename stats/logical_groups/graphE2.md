```mermaid
classDiagram
direction TB
E2  <|-- E3 : IsA
E3 : Condition State
E3 :  3-Traduit en attente de revision (109 mots)
E2  <|-- E4 : IsA
E4 : Period
E4 :  5-Valide (960 mots)
E4 <.. P7 : has domain
P7 : took place at 
P7 :  4-En cours de revision (233 mots)
E4 <.. P8 : has domain
P8 : took place on or within 
P8 :  1-A traduire (118 mots)
```
