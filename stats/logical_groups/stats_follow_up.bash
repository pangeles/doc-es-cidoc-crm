#!bin/bash
# Suivre la validation des groupes fonctionnels
# ! lignes de commandes !
# cp CIDOC_traduction_Index.... .csv CIDOC_Traduction_Index.csv
# bash stats_follow_up.bash CIDOC_Traduction_Index.csv
echo "Nom du fichier googlesheet import CSV utilisé : $1"
filename="traduction_follow_up_status.csv"
echo $filename
## Construction du fichier de travail googlesheet importé en CSV (on supprime la première ligne)
unset epstatus
unset epnumber
sed -i '1d' $1
((m=0))
while IFS=","
read -a tabarray
 do
  epstatus+=(${tabarray[0]})
  epnumber+=(${tabarray[2]})
  echo "${tabarray[2]};${tabarray[0]}" >> $filename
  echo "$m ${tabarray[2]};${tabarray[0]}"
((m+=1))
 done <"$1"
 sed -i '243,277d' $filename
 sed -i 's/ /_/g' $filename
 head $filename
 tail $filename
