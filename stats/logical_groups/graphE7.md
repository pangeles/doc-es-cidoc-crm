```mermaid
classDiagram
direction TB
E7  <|-- E8 : IsA
E8 : Acquisition 
E8 :  3-Traduit en attente de revision (87 mots)
E8 <.. P22 : has domain
P22 : transferred title to 
P22 :  3-Traduit en attente de revision (79 mots)
E8 <.. P23 : has domain
P23 : transferred title from 
P23 :  3-Traduit en attente de revision (52 mots)
E8 <.. P24 : has domain
P24 : transferred title of 
P24 :  3-Traduit en attente de revision (29 mots)
E7  <|-- E9 : IsA
E9 : Move
E9 :  1-A traduire (123 mots)
E9 <.. P25 : has domain
P25 : moved 
P25 :  3-Traduit en attente de revision (49 mots)
E9 <.. P26 : has domain
P26 : moved to 
P26 :  3-Traduit en attente de revision (104 mots)
E9 <.. P27 : has domain
P27 : moved from 
P27 :  3-Traduit en attente de revision (105 mots)
E7  <|-- E10 : IsA
E10 : Transfer of Custody
E10 :  2-En cours de traduction (54 mots)
E10 <.. P28 : has domain
P28 : custody surrendered by 
P28 :  3-Traduit en attente de revision (75 mots)
E10 <.. P29 : has domain
P29 : custody received by 
P29 :  3-Traduit en attente de revision (76 mots)
E10 <.. P30 : has domain
P30 : transferred custody of 
P30 :  3-Traduit en attente de revision (63 mots)
E7  <|-- E11 : IsA
E11 : Modification
E11 :  5-Valide (256 mots)
E11 <.. P31 : has domain
P31 : has modified 
P31 :  3-Traduit en attente de revision (17 mots)
E11 <.. P33 : has domain
P33 : used specific technique 
P33 :  3-Traduit en attente de revision (86 mots)
E11 <.. P126 : has domain
P126 : employed 
P126 :  3-Traduit en attente de revision (51 mots)
E7  <|-- E13 : IsA
E13 : Attribute Assignment
E13 :  3-Traduit en attente de revision (345 mots)
E13 <.. P177 : has domain
P177 : assigned property type
P177 :  3-Traduit en attente de revision (129 mots)
E13 <.. P140 : has domain
P140 : assigned attribute to 
P140 :  3-Traduit en attente de revision (52 mots)
E13 <.. P141 : has domain
P141 : assigned 
P141 :  3-Traduit en attente de revision (51 mots)
E7  <|-- E65 : IsA
E65 : Creation
E65 :  5-Valide (27 mots)
E65 <.. P94 : has domain
P94 : has created 
P94 :  2-En cours de traduction (69 mots)
E7  <|-- E66 : IsA
E66 : Formation
E66 :  1-A traduire (96 mots)
E66 <.. P151 : has domain
P151 : was formed from 
P151 :  1-A traduire (34 mots)
E66 <.. P95 : has domain
P95 : has formed 
P95 :  1-A traduire (18 mots)
E7  <|-- E85 : IsA
E85 : Joining
E85 :  1-A traduire (74 mots)
E85 <.. P143 : has domain
P143 : joined 
P143 :  1-A traduire (71 mots)
E85 <.. P144 : has domain
P144 : joined with 
P144 :  1-A traduire (133 mots)
E7  <|-- E86 : IsA
E86 : Leaving
E86 :  1-A traduire (70 mots)
E86 <.. P145 : has domain
P145 : separated 
P145 :  1-A traduire (22 mots)
E86 <.. P146 : has domain
P146 : separated from 
P146 :  1-A traduire (52 mots)
E7  <|-- E87 : IsA
E87 : Curation Activity
E87 :  3-Traduit en attente de revision (125 mots)
E87 <.. P147 : has domain
P147 : curated 
P147 :  1-A traduire (32 mots)
```
