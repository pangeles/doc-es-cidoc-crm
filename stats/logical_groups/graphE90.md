```mermaid
classDiagram
direction TB
E90  <|-- E73 : IsA
E73 : Information Object
E73 :  1-A traduire (152 mots)
E73 <.. P165 : has domain
P165 : incorporates 
P165 :  1-A traduire (275 mots)
E90  <|-- E41 : IsA
E41 : Appellation
E41 :  3-Traduit en attente de revision (328 mots)
E41 <.. P139 : has domain
P139 : has alternative form 
P139 :  1-A traduire (153 mots)
```
