```mermaid
classDiagram
direction TB
E41  <|-- E42 : IsA
E42 : Identifier
E42 :  1-A traduire (91 mots)
E41  <|-- E35 : IsA
E41  <|-- E95 : IsA
E95 : Spacetime Primitive
E95 :  1-A traduire (287 mots)
E41  <|-- E94 : IsA
E94 : Space Primitive
E94 :  2-En cours de traduction (256 mots)
E41  <|-- E61 : IsA
E61 : Time Primitive
E61 :  1-A traduire (220 mots)
```
