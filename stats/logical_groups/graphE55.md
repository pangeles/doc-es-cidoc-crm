```mermaid
classDiagram
direction TB
E55  <|-- E56 : IsA
E56 : Language
E56 :  5-Valide (82 mots)
E55  <|-- E57 : IsA
E57 : Material
E57 :  3-Traduit en attente de revision (147 mots)
E55  <|-- E58 : IsA
E58 : Measurement Unit
E58 :  3-Traduit en attente de revision (86 mots)
E55  <|-- E99 : IsA
E99 : Product Type
E99 :  3-Traduit en attente de revision (229 mots)
E99 <.. P187 : has domain
P187 : has production plan 
P187 :  3-Traduit en attente de revision (100 mots)
E99 <.. P188 : has domain
P188 : requires production tool 
P188 :  3-Traduit en attente de revision (99 mots)
```
