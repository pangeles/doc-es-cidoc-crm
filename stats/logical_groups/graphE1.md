```mermaid
classDiagram
direction TB
E1  <|-- E2 : IsA
E1 : CRM Entity
E1 :  3-Traduit en attente de revision (87 mots)
P1 ..> E1 : has domain
P1 : is identified by 
P1 :  3-Traduit en attente de revision (175 mots)
P48 ..> E1 : has domain
P48 : has preferred identifier 
P48 :  1-A traduire (103 mots)
P2 ..> E1 : has domain
P2 : has type 
P2 :  3-Traduit en attente de revision (165 mots)
P137 ..> E1 : has domain
P137 : exemplifies 
P137 :  1-A traduire (87 mots)
P3 ..> E1 : has domain
P3 : has note
P3 :  3-Traduit en attente de revision (143 mots)
E2 : Temporal Entity
E2 :  3-Traduit en attente de revision (152 mots)
E2 <.. P4 : has domain
P4 : has timespan 
P4 :  1-A traduire (76 mots)
E2 <.. P173 : has domain
P173 : starts before or with the end of 
P173 :  1-A traduire (43 mots)
E2 <.. P174 : has domain
P174 : starts before the end of 
P174 :  1-A traduire (41 mots)
E2 <.. P184 : has domain
P184 : ends before or with the end of 
P184 :  1-A traduire (43 mots)
E2 <.. P185 : has domain
P185 : ends before the end of 
P185 :  1-A traduire (41 mots)
E2 <.. P182 : has domain
P182 : ends before or with the start of 
P182 :  1-A traduire (43 mots)
E2 <.. P175 : has domain
P175 : starts before or with the start of 
P175 :  1-A traduire (43 mots)
E2 <.. P176 : has domain
P176 : starts before the start of 
P176 :  1-A traduire (41 mots)
E2 <.. P183 : has domain
P183 : ends before the start of 
P183 :  1-A traduire (41 mots)
E1  <|-- E77 : IsA
E77 : Persistent Item
E77 :  3-Traduit en attente de revision (411 mots)
E1  <|-- E52 : IsA
E52 : TimeSpan
E52 :  3-Traduit en attente de revision (285 mots)
E52 <.. P170i : has domain
P170i : time is defined by 
P170i :   (60 mots)
E52 <.. P79 : has domain
P79 : beginning is qualified by
P79 :  5-Valide (46 mots)
E52 <.. P80 : has domain
P80 : end is qualified by
P80 :  5-Valide (50 mots)
E52 <.. P81 : has domain
P81 : ongoing throughout
P81 :  3-Traduit en attente de revision (122 mots)
E52 <.. P82 : has domain
P82 : at some time within
P82 :  3-Traduit en attente de revision (112 mots)
E52 <.. P86 : has domain
P86 : falls within 
P86 :  3-Traduit en attente de revision (61 mots)
E52 <.. P191 : has domain
P191 : had duration 
P191 :  3-Traduit en attente de revision (62 mots)
E1  <|-- E53 : IsA
E53 : Place
E53 :  3-Traduit en attente de revision (234 mots)
E53 <.. P168i : has domain
P168i : place is defined by 
P168i :   (60 mots)
E53 <.. P89 : has domain
P89 : falls within 
P89 :  3-Traduit en attente de revision (45 mots)
E53 <.. P121 : has domain
P121 : overlaps with
P121 :  2-En cours de traduction (88 mots)
E53 <.. P122 : has domain
P122 : borders with
P122 :  2-En cours de traduction (86 mots)
E53 <.. P157 : has domain
P157 : is at rest relative to 
P157 :  3-Traduit en attente de revision (110 mots)
E53 <.. P59i : has domain
P59i : is located on or within
P59i :   (96 mots)
E53 <.. P156i : has domain
P156i : is occupied by
P156i :   (382 mots)
E53 <.. P171 : has domain
P171 : at some place within 
P171 :  1-A traduire (119 mots)
E53 <.. P172 : has domain
P172 : contains 
P172 :  1-A traduire (103 mots)
E53 <.. P189 : has domain
P189 : approximates 
P189 :  1-A traduire (255 mots)
E1  <|-- E54 : IsA
E54 : Dimension
E54 :  2-En cours de traduction (309 mots)
E54 <.. P90 : has domain
P90 : has value
P90 :  1-A traduire (19 mots)
E54 <.. P91 : has domain
P91 : has unit 
P91 :  1-A traduire (16 mots)
E1  <|-- E92 : IsA
E92 : Spacetime Volume
E92 :  1-A traduire (139 mots)
E92 <.. P169i : has domain
P169i : spacetime volume is defined by 
P169i :   (19 mots)
E92 <.. P132 : has domain
P132 : spatiotemporally overlaps with
P132 :  2-En cours de traduction (160 mots)
E92 <.. P10 : has domain
P10 : falls within 
P10 :  1-A traduire (42 mots)
E92 <.. P133 : has domain
P133 : is separated from
P133 :  1-A traduire (163 mots)
E92 <.. P160 : has domain
P160 : has temporal projection
P160 :  1-A traduire (43 mots)
E92 <.. P161 : has domain
P161 : has spatial projection
P161 :  1-A traduire (298 mots)
E1  <|-- E59 : IsA
E59 : Primitive Value
E59 :  2-En cours de traduction (256 mots)
```
