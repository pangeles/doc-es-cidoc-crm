```mermaid
classDiagram
direction TB
E31  <|-- E32 : IsA
E32 : Authority Document
E32 :  3-Traduit en attente de revision (20 mots)
E32 <.. P71 : has domain
P71 : lists 
P71 :  1-A traduire (23 mots)
```
