#!bin/bash
#=========================================================================================
# SCRIPT DE FORMATION DES GROUPES LOGIQUES POUR L'ORGANISATION ET LE SUIVI DE TRADUCTION
#=========================================================================================
# licence cc by sa Raphaëlle Krummeich
# contact : raphaelle.krummeich@univ-rouen.fr
# [WORK IN PROGRESS]
# Ce travail s'inscrit dans le cadre du groupe international de traduction en français du
# manuel du Cidoc Conceptual Reference Model.
#=========================================================================================

#=========================================================================================
# ORGANISATION DES DONNÉES D'ENTRÉE
#=========================================================================================
# Les données d'entrée du script sont issues de plusieurs sources :
# - le manuel du Cidoc CRM au format .docx dont les tables 3 (CIDOC CRM Class Hierarchy) & 4
# (CIDOC CRM Property Hierarchy) des pages 50 et suivantes,
# - la version XML d'une partie de ce manuel accessible ici : https://cidoc-crm.org/html/cidoc_crm_v7.1.1.xml
# - l'index de traduction issu du dépôt gitlab du projet.
#=========================================================================================

#  echo "Saisir le nom de fichier du tableau hiérarchique des entités : "
#  read nomdufichier
  nomdufichier="entities_hierarchy.csv"
#  echo "Saisir le nom de fichier du tableau hiérarchique des propriétés :"
#  read nomdufichierproprietes
  nomdufichierproprietes="properties_hierarchy.csv"
  #  echo "Saisir le séparateur du fichier csv (, ou ;)" :
  #  read separateur
  separateur=";"
#  echo "Saisir le fichier XML de la description des entitiés et propriétés :"
  #  read nomdufichierxml
  nomdufichierxml="workfile.xml"
#  echo "Saisir le fichier CSV de l'indexation en traduction des entitiés et propriétés :"
  # read nomdufichierindex
  nomdufichierindex="CIDOC_Traduction_Index_nov22.csv"
  echo "Les données sont les suivantes :"
  echo "Le fichier des entités : $nomdufichier"
  echo "Le fichier des propriétés : $nomdufichierproprietes"
  echo "Le séparateur des données : $separateur"
  echo "Le fichier contenant la description des entitiés et propriétés : $nomdufichierxml"
  echo "Le fichier contenant l'indexation en traduction des entitiés et propriétés : $nomdufichierindex"

#=========================================================================================
# FONCTIONS UTILES
#=========================================================================================

  _contains(){ # Vrai si il y a la présence d'une chaîne de caractère "$2" dans une liste $1

    echo $1 | tr ' ' '\n' | grep -F -x -q "$2"
  }

  build_file_xml () { # Construit le fichier à deux colonnes des concepts et notes descriptives issues du fichier XML


        file="$1"
        sed -i[old] 's/<p>/ /g; s/<\/p>/ /g; s/<ul>/ /g; s/<\/ul>/ /g; s/<li>/ /g; s/<\/li>/ /g; s/<em>/ /g; s/<\/em>/ /g; s/<\/scopeNote>//g' "$file"
        grep -oP '<fullName>(.*)</fullName' "$file" | cut -d ">" -f 2 | cut -d "<" -f 1 | awk '{print $1}' FS=" " > outputx
        grep -oP '<scopeNote>(.*)' "$file" | cut -d ">" -f 2 | cut -d "<" -f 1 > outputy
        cat -n outputx > xoutput
        cat -n outputy > youtput
        join xoutput youtput > xyoutput
        cp xyoutput "output.$file"
        rm outputx outputy xoutput youtput xyoutput
    }


  build_file_properties (){ # Construit le fichier associant entité et propriété dont elle est le domaine

      declare -a datain=($@)
      file="${datain[0]}"
      SEP="${datain[1]}"


      declare -a data
      declare -a varP
      declare -a varE
      declare -a numEdomnumP


    numEdomnumP=($(awk '{print $3}' FS="$SEP" $file | sed 's/[[:space:]].*//g'))
    data=($(echo ${datain[@]} | sed 's/'"$file"'//g;s/;//g;s/^[[:space:]]*//g'))
    ((np=${#data[@]} + 1))
      for (( i=1; i < $np; i++))
       do
        varP=($(echo ${data[@]} | awk -v col="$i" '{print $col}' FS=" "))
        varE=($(echo ${numEdomnumP[@]} | awk -v col="$i" '{print $col}' FS=" "))
        echo "$varE $varP" >> output
       done
        awk '!a[$0]++' output > "output.$file"
        rm output

}

  build_file_index (){ # Construit le fichier associant concept et index de traduction issu du fichier $1

      file=$1
      SEP=$2

      declare -a varC
      declare -a varIndex
      declare -a data
      declare -a dataindex


      data=($(awk '{print $2}' FS="$SEP" $file))
      dataindex=($(awk '{print $1}' FS="$SEP" $file | sed 's/[[:space:]]/_/g'))
      ((np=${#data[@]} + 1))
      for (( i=1; i < $np; i++))
       do
        varC=($(echo ${data[@]} | awk -v col="$i" '{print $col}' FS=" "))
        varIndex=($(echo ${dataindex[@]} | awk -v col="$i" '{print $col}' FS=" "))
        echo "$varC $varIndex" >> outindex
       done
        awk '!a[$0]++' outindex > "output.$file"
        rm outindex

    }

    function scopenote_extract(){ # Fonction d'extraction de la note descriptive du fichier $2 du concept $1

    local STR=$1
    local filename=$2
    declare -a scopenote
    local checkE
    local checkP

    checkE="^E"
    checkP="^P"
    if [[ $STR =~ $checkE ]]
    then
     scopenote=($(grep -m 1 "\(^[[:digit:]]*[[:space:]]*$STR\)" "$filename" | sed 's/\(^[[:digit:]]*[[:space:]]*E[[:digit:]]*[[:space:]]*\)//g'))
    elif [[ $STR =~ $checkP ]]
    then
     scopenote=($(grep -m 1 "\(^[[:digit:]]*[[:space:]]*$STR\)" "$filename" | sed 's/\(^[[:digit:]]*[[:space:]]*P[[:digit:]]*[[:space:]]*\)//g'))
    else
      echo "$STR n'est pas dans le fichier XML $filename ou contient une erreur"
    fi
    echo ${scopenote[@]}

  }

    function calcul_rang(){ # Fonction de calcul du rang du concept à partir du fichier $2 (compte les - hors mots composés)

    local SEP=$1
    local filename=$2
    local STR1
    local STR2

    declare -a line

    while IFS="$SEP"
          read -a line
    do
      STR1="${line[@]}"
      ((nstr2=$(expr length `echo "${line[@]}" | sed "s/[[:space:]]-//g"`)))
      ((nstr1=${#STR1}))
      ((num= $nstr1 - $nstr2))
      ((num= $num / 2 + 1))
      echo $num
    done <"$filename"
  }

  function group_entities(){ # Fonction de calcul des groupes logiques d'entités

    declare -a vector=("$@")
    declare -a rang
    declare -a num
    declare -a list


    list=("")
    ((nl=${#vector[@]}))
    ((nl=$nl / 2))
    for (( i=0; i < $nl; i++ )); do
      ((j=$i + $nl))
      rang+=(${vector[$i]})
      num+=(${vector[$j]})
    done
    ((k=0))
    ((n=0))
    while [[ $k -le $nl ]]; do
      STR=${num[$k]}
      if [[ $k -ne $nl ]]; then
        ((l= $k + 1))
        ((li = $k + 1))
        if [[ $l -gt $nl ]]; then
          break
        fi
        ((m=0))
        while [[ $l -le $nl ]]; do
          if _contains "${mylist}" "${STR}"; then
            break
          fi
          ((rg = ${rang[$k]} + 1))
          if [[ ${rang[$l]} -eq $rg ]]; then
            if [[ $l -eq $li ]]; then
              echo "$STR"
            fi
              echo "${num[$l]}"
              ((m+=1))
              ((l+=1))
          elif [[ ${rang[$l]} -gt ${rang[$k]} ]]; then
            ((l+=1))
          else
            if [[ m -ne 0 ]]; then
              echo ";"
              list+=("${num[$k]}")
              mylist=${list[@]}
              ((n+=1))
            fi
            ((l= $nl + 1))
          fi
        done
        ((k+=1))
      else
        break
      fi
    done
    echo $n

  }

  function edomain_properties(){ # Fonction de calcul de la liste des propriétés dont l'entité $1 est le domaine

    e="$1"
    fileout="$2"
    declare -a listp

    list=$(grep "$e" $fileout | sed 's/^E[[:digit:]]*[[:space:]]//g' | tr '\n' ' ')
    ll=${#list}
    if [[ $ll -gt 0 ]];
    then
      echo ${list[@]}
    fi
  }

#=========================================================================================
# PROGRAMME PRINCIPAL
#=========================================================================================

  declare -a numC
  declare -a indexC
  declare -a numE
  declare -a nomE
  declare -a rangE
  declare -a groupE
  declare -a numP
  declare -a nomP
  declare -a edomainp
  declare -A nomentity
  declare -A nomproperty
  declare -A translation_index
  declare -a already_there

  numP=($(awk '{print $1}' FS="$separateur" $nomdufichierproprietes))
  nomP=($(awk '{print $0}' FS="$separateur" $nomdufichierproprietes | sed 's/^P[[:digit:]]*//g; s/^i//g;s/^;//g; s/[[:space:]]*-//g; s/^[[:space:]]*//g; s/;E.*//g; s/[[:space:]]/_/g' ))

  ((i=0))
  for num in ${numP[@]}; do
      nomproperty[$num]=$(echo ${nomP[$i]} | sed 's/_/ /g; s/(.*//g')
      ((i+=1))
  done

  build_file_properties "$nomdufichierproprietes $separateur ${numP[@]}"
  cp "output.$nomdufichierproprietes" output

  numE=($(awk '{print $1}' FS="$separateur" $nomdufichier))
  nomE=($(awk '{print $0}' FS="$separateur" $nomdufichier | sed 's/^E[[:digit:]]*//g;s/-//g;s/;//g;s/[[:space:]]/_/g' ))

  ((i=0))
  for num in ${numE[@]}; do
      nomentity[$num]=$(echo ${nomE[$i]} | sed 's/_/ /g')
      ((i+=1))
  done

  build_file_index $nomdufichierindex $separateur
  cp "output.$nomdufichierindex" outindex

  numC=($(awk '{print $1}' outindex))
  indexC=($(awk '{print $2}' outindex))

  ((i=0))
  for num in ${numC[@]}; do
      translation_index[$num]=$(echo ${indexC[$i]} | sed 's/_/ /g')
      ((i+=1))
  done


  rangE=($(calcul_rang $separateur $nomdufichier))
  groupE=($(group_entities ${rangE[@]} ${numE[@]}))

  ((el=${#groupE[@]} - 1))
  ((nn=${groupE[$el]} + 1))
  ((ng=${groupE[$el]}))
  echo -e "Il y a $ng groupes logiques."

  build_file_xml "$nomdufichierxml"
  cp "output.$nomdufichierxml" outputxml

  already_list=""
  for (( i=1; i < $nn ; i++)); do
    var=($(echo ${groupE[@]} | awk -v col="$i" '{print $col}' FS=";"))
    ((nvar=${#var[@]}))
    var0="${var[0]} "
    vari="${var[0]}"
    echo "Groupe "$vari""
    milestone="milestone$vari.csv"
    edomainp=($(edomain_properties "$var0" output))
    nedomain=${#edomainp[@]}
    mdfile="graph$vari.md"
    echo "\`\`\`mermaid" > $mdfile
    echo -e "classDiagram\ndirection TB" >> $mdfile
    for (( j=1; j < $nvar; j++)); do
      varj="${var[$j]} "
      varij="${var[$j]}"
           echo "$var0 <|-- $varij : IsA" >> $mdfile
      if [[ $j -eq 1 ]]; then
        if ! _contains "${already_list}" "$vari";then
           already_there+=($vari)
           already_list=${already_there[@]}
        scopee=($(scopenote_extract $var0 outputxml))
        nscope=${#scopee[@]}
        echo -e "$vari : ${nomentity[$vari]}\n$vari : ${translation_index[$vari]} ($nscope mots)" >> $mdfile
        echo "$vari;${translation_index[$vari]};$nscope" >> $milestone
         if [[ $nedomain -ne 0 ]]; then
           for (( k=0; k < $nedomain; k++)); do
            pei=${edomainp[$k]}
            pe=$(echo "${edomainp[$k]}" | sed 's/i//')
            scopep=($(scopenote_extract $pe outputxml))
            nscope=${#scopep[@]}
            echo $pei
            echo -e "$pei ..> $vari : has domain\n$pei : ${nomproperty[$pei]}\n$pei : ${translation_index[$pe]} ($nscope mots)" >> $mdfile
            echo "$pei;${translation_index[$pei]};$nscope" >> $milestone
           done
         fi
  #      else
  #           echo "in the already_list $vari"
        fi
        if ! _contains "${already_list}" "$varij";then
           already_there+=($varij)
           already_list=${already_there[@]}
        echo $varij
        scopee=($(scopenote_extract $varj outputxml))
        nscope=${#scopee[@]}
        echo -e "$varij : ${nomentity[$varij]}\n$varij : ${translation_index[$varij]} ($nscope mots)" >> $mdfile
        echo "$varij;${translation_index[$varij]};$nscope" >> $milestone
        edomainp=($(edomain_properties "$varj" output))
        nedomain=${#edomainp[@]}
         if [[ $nedomain -ne 0 ]]; then
           for (( k=0; k < $nedomain; k++)); do
            pei=${edomainp[$k]}
            pe=$(echo "${edomainp[$k]}" | sed 's/i//')
            scopep=($(scopenote_extract $pe outputxml))
            nscope=${#scopep[@]}
            echo -e "$varij <.. $pei : has domain\n$pei : ${nomproperty[$pei]}\n$pei : ${translation_index[$pe]} ($nscope mots)" >> $mdfile
            echo "$pei;${translation_index[$pei]};$nscope" >> $milestone
           done
         fi
  #      else
  #           echo "in the already_list $varij"
        fi
      else
        if ! _contains "${already_list}" "$varij";then
           already_there+=($varij)
           already_list=${already_there[@]}
        echo $varij
        scopee=($(scopenote_extract $varj outputxml))
        nscope=${#scopee[@]}
        echo -e "$varij : ${nomentity[$varij]}\n$varij : ${translation_index[$varij]} ($nscope mots)" >> $mdfile
        echo "$varij;${translation_index[$varij]};$nscope" >> $milestone
        edomainp=($(edomain_properties "$varj" output))
        nedomain=${#edomainp[@]}
         if [[ $nedomain -ne 0 ]]; then
           for (( k=0; k < $nedomain; k++)); do
            pei=${edomainp[$k]}
            pe=$(echo "${edomainp[$k]}" | sed 's/i//')
            scopep=($(scopenote_extract $pe outputxml))
            nscope=${#scopep[@]}
            echo -e "$varij <.. $pei : has domain\n$pei : ${nomproperty[$pei]}\n$pei : ${translation_index[$pe]} ($nscope mots)" >> $mdfile
            echo "$pei;${translation_index[$pei]};$nscope" >> $milestone
           done
         fi
   #     else
   #          echo "in the already_list $varij"
        fi
      fi
    done
     echo "\`\`\`" >> $mdfile
  done
  echo "already_there ${#already_there[@]} ${already_there[@]}"
    rm output outputxml outindex
