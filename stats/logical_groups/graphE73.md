```mermaid
classDiagram
direction TB
E73  <|-- E29 : IsA
E29 : Design or Procedure
E29 :  3-Traduit en attente de revision (98 mots)
E29 <.. P68 : has domain
P68 : foresees use of 
P68 :  1-A traduire (84 mots)
E29 <.. P69 : has domain
P69 : has association with 
P69 :  1-A traduire (119 mots)
E73  <|-- E31 : IsA
E31 : Document
E31 :  3-Traduit en attente de revision (57 mots)
E31 <.. P70 : has domain
P70 : documents 
P70 :  1-A traduire (67 mots)
E73  <|-- E33 : IsA
E33 : Linguistic Object
E33 :  2-En cours de traduction (245 mots)
E33 <.. P72 : has domain
P72 : has language 
P72 :  1-A traduire (42 mots)
E33 <.. P73i : has domain
P73i : is translation of
P73i :   (60 mots)
E73  <|-- E36 : IsA
E36 : Visual Item
E36 :  3-Traduit en attente de revision (190 mots)
E36 <.. P138 : has domain
P138 : represents 
P138 :  1-A traduire (152 mots)
```
