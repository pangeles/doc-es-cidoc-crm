```mermaid
classDiagram
direction TB
E70  <|-- E72 : IsA
E72 : Legal Object
E72 :  5-Valide (94 mots)
E72 <.. P104 : has domain
P104 : is subject to 
P104 :  5-Valide (39 mots)
E72 <.. P105 : has domain
P105 : right held by 
P105 :  5-Valide (76 mots)
E70  <|-- E71 : IsA
E71 : HumanMade Thing
E71 :  5-Valide (50 mots)
E71 <.. P102 : has domain
P102 : has title 
P102 :  5-Valide (87 mots)
E71 <.. P103 : has domain
P103 : was intended for 
P103 :  5-Valide (65 mots)
```
