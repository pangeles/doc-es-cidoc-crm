```mermaid
classDiagram
direction TB
E64  <|-- E6 : IsA
E6 : Destruction
E6 :  1-A traduire (71 mots)
E6 <.. P13 : has domain
P13 : destroyed 
P13 :  1-A traduire (81 mots)
E64  <|-- E68 : IsA
E68 : Dissolution
E68 :  5-Valide (38 mots)
E68 <.. P99 : has domain
P99 : dissolved 
P99 :  5-Valide (18 mots)
E64  <|-- E69 : IsA
E69 : Death
E69 :  5-Valide (48 mots)
E69 <.. P100 : has domain
P100 : was death of 
P100 :  5-Valide (49 mots)
E64  <|-- E81 : IsA
```
