```mermaid
classDiagram
direction TB
E65  <|-- E83 : IsA
E83 : Type Creation
E83 :  2-En cours de traduction (105 mots)
E83 <.. P135 : has domain
P135 : created type 
P135 :  3-Traduit en attente de revision (20 mots)
E83 <.. P136 : has domain
P136 : was based on 
P136 :  2-En cours de traduction (78 mots)
```
