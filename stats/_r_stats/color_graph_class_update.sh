#! usr/bin/bash
#--- script de mise à jour des couleurs du graphe svg
# TODO : 
# 1. une routine permettant de vérifier que l'entité ou la propriété existe
# 2. refactoriser le statut
# 2bis. refactoriser les commande sed
# 3. construire la requête API dédiée pour déterminer le nouveau statut
# 4. changer le graphe SVG avec la bonne couleur - fait
# 5. commenter
#---

source .env

function _id_line_color(){
    
    form="id=\"$1\""
    grep -n "$form" $graphsvg | 
	cut -d\: -f 1 > _line
    read line < _line
    bline=$(( line - 2 )); eline=$(( line + 6))
    inter=""$bline,$eline"p"
    sed -n $inter $graphsvg | 
	 grep -n fill |
	cut -d\: -f 1 > _line
    read line < _line
    color_line="$(( $bline + $line - 1))p"
    echo "$color_line"
    return
}

function _get_status(){

case $1 in
  1)
   echo "à traduire"
   ;;
  2)
   echo "en cours de traduction"
   ;;
  3)
   echo "traduit en cours de révision"
   ;;
  4)
   echo "en cours de révision"
   ;;
  5)
   echo "validé"
   ;;
 esac
return
}

echo "entrer la classe ou propriété ayant changé de statut : "
read ep
_id_line_color $ep > _color_line
read color_line < _color_line
echo "La ligne à modifier est la ligne numéro : " $color_line | sed 's/p//'
sed -n $color_line $graphsvg | 
	cut -d\# -f 2 | 
	sed 's/"//g' > _color_actual
 read color < _color_actual
 grep -n $color _color_codes | cut -d\: -f 1 > _statut_actuel
 read statut < _statut_actuel
 echo "Le statut actuel de "$ep" dans le graphe est :" | tr '\n' ' '; _get_status $statut
 echo "Saisir le numéro du nouveau statut de "$ep" dans le graphe : - 1 A traduire - 2 En cours de traduction - 3 Traduit en attente de révision - 4 En cours de révision - 5 Valide -" | tr '-' '\n'
 read num
 sed -n "$num p" _color_codes > _color_new
 read color_new < _color_new
 sed 's/p/s/' _color_line > _line
 read line < _line
 echo "Il s'agit de remplacer au sein de la ligne $line la couleur $color par celle-ci $color_new. Voici le résultat du script :" 
 sed -n $color_line $graphsvg
 sed "$line/$color/$color_new/" $graphsvg | sed -n $color_line
 echo "Est-ce la bonne correction ? (o/n)"
 read answer
 if [[ $answer == "o" ]]; then sed -i "$line/$color/$color_new/" $graphsvg;fi

