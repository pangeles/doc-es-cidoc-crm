#!/bin/bash
########################################################################
#   SCRIPT DE COLLECTE DES DONNEES ASSOCIEES AUX TICKETS PAR TICKET    #
########################################################################
#
#-----------------------------------------------------------------------
# autrice : Raphaëlle Krummeich, raphaelle.krummeich (a) univ-rouen .fr
# Il s'agit d'organiser la collecte des données des tickets du dépôt
# afin de suivre les flux de traduction en lien notamment aux jalons
# mais aussi pour la publication de la traduction sur la plateforme SIG
#-----------------------------------------------------------------------
#
#- SCRIPT
#-- récupération de la variable
#   - $TOKEN_READ_API, jeton (token) d'accès en lecture à l'API
#   - $REPOS_URL_API, adressage de l'API vers les issues
#   - $NB_ISSUES_PER_PAGES, nombre de tockets par page gitlab
source ./.env
#- FONCTION
#-- il s'agit, à partir du nombre de tickets créés dans le dépôt
# de calculer approximativement le nombre de pages gitlab déployées
# en réponse à la requête en fonction du nombre de tickets par page
# $NB_ISSUES_PER_PAGES déterminé dans le fichier .env. Celui-ci est
# optimisé afin de limiter le nombre de requêtes API, c'est à dire,
# dans le cas présenté (avec quelques centaines de tickets), à 100.
nombre_total_de_pages_de_tickets() {

expr \( $@ - $@ % $NB_ISSUES_PER_PAGES + $NB_ISSUES_PER_PAGES \) / $NB_ISSUES_PER_PAGES
return

}
#- PREMIERE REQUETE
#-- interroge l'application pour obtenir le numéro du dernier ticket créé,
# à l'aide du paquet jq (https://jqlang.github.io/jq/download/) qui permet
# l'extraction de données structurées en json (parsing json)
curl -s --header "PRIVATE-TOKEN:$TOKEN_READ_API" "$REPO_URL_API?per_page=1" \
    | jq '.[] | "\(.iid)"' | sed 's/"//g' > nb_issues
< nb_issues read nb_i
#- CALCUL DU NOMBRE DE PAGES GITLAB CONTENANT LES METADONNEES DES TICKETS
nb_p=$(nombre_total_de_pages_de_tickets $nb_i)
#- INFORMATIONS DONNEES A L'UTILISATEURICE DU SCRIPT
#echo "Nombre de pages de tickets pour $NB_ISSUES_PER_PAGES issues par page : \
#    " $nb_p
#echo "Nombre total de tickets numérotés :" $nb_i
#- REQUETES COMPLEMENTAIRES PAR PAGE
#-- pour chaque page gitlab contenant $NB_ISSUES_ER_PAGES tickets, la fonction
# jq extrait les données suivantes :
#   - iid: numéro du ticket permettatnt, par reconstruction de l'url, d'accéder
#   à la note de traduction :
#   - state: statut d'ouverture ou de fermeture du ticket, indicatif de son
#   niveau au sein du flux de traduction. Si le statut est "open", l'indication
#   au niveau des étiquettes (labels) permet d'affiner le statut dans le flux de travail
#   du fragment de texte concerné
#   - title: correspond au CIDOC CRM label de l'entité ou de la propriété
#   - labels: étiquettes utilisées pour qualifier la version du CIDOC CRM,
#   l'état dans le flux de traduction (5 étiquettes) et d'autres informations.
rm nb_issues
rm selected_metadata_issues
#URL=https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/issues/
for i in $(seq 1 $nb_p) ; do
   curl -s --header "PRIVATE-TOKEN:$TOKEN_READ_API" \
       "$REPO_URL_API?per_page=$NB_ISSUES_PER_PAGES&page=$i"\
       | jq '.[] | "\(.iid):\(.state):\(.title)"' \
       | grep [EP][[:digit:]] \
       | sed 's/\[\\//g;s/\\//g;s/\]//g;s/"//g'\ >> selected_metadata_issues
  #     | sed 's,^,'"$URL"',' >> selected_metadata_issues
done
