# Extraction de métadonnées associées au processus de traduction (voir #285)
Dans le cadre du format d'échange des traductions du CIDOC CRM (voir # issue du CRM SIG), le script proposé extrait en temps réel les métadonnées associées à chaque entité ou propriété dont la traduction est en cours ou  a été réalisée.  
Il s'agit d'inclure, dans le modèle XML du CRM SIG permettant l'interopérabilité de la documentation du CIDOC CRM, des éléments spécifiques au processus de traduction de celle-ci.  
Dans le cas présenté ici, les métadonnées choisies sont :
- le numéro du ticket, correspondant à la note de traduction,
- le titre du ticket, correspondant au CIDOC CRM label des entités et propriétés,
- le statut du ticket, permettant d'identifier un premier état (ouvert ou clos) de la traduction de l'entité ou de la propriété,
- les étiquettes associées au ticket, permettant d'afficher la version du CIDOC CRM traduite et un état d'avancée - le cas échéant, lorsque le ticket est ouvert.
## Description du dépôt
Les fichiers utiles sont :
- le fichier .env contenant des variables permettant notamment l'accès en lecture à l'API (s'adresser à @bdavid),  
- le fichier `api_script_work_version_2.sh` : script `bash` permettant l'extraction des métadonnées des tickets sélectionnés (entités, propriétés) :
```
bash api_script_work_version_2.sh
```  
- le fichier produit par le script, `selected_metadata_issues`, qui contient, sous la forme d'un fichier CSV, les données à fournir pour le format XML interchange de la traduction.

## Extraire les métadonnées utiles pour construire le fichier XML interchange
Il s'agit, en lien notamment avec les travaux décrits ici #289, d'intégrer un flux semi-automatique d'informations à destination de la plateforme de publication des traductions du CIDOC CRM.  
Afin d'obtenir les informations relatives à une entité ou propriété, il est possible d'utiliser la commande `grep` de telle manière : 
```
grep "E2 E" | cut -d \; -f $i selected_metadata_issues
```
avec :
- i=1 : le n° du ticket correspondant à la note de traduction à partir duquel on peut définir l’url d’accès,
- i=2 : le statut ouvert ou fermé du ticket, indicatif d'un état binaire de la traduction, ouvert : _dans le flux de traduction_ ou _in progress_, fermé : _validé_ ou _validated_,
- i=3 : le CIDOC CRM label@en de l'entité ou de la propriété,
- [supprimé] i=4 : la ou les étiquettes associées au ticket, séparées par des `,` et précisant :
  - dans le cas d'un ticket au statut *ouvert*, l'étape d'avancement de la traduction correspondant aux étapes définies dans les étiquettes,
  - dans tous les cas, la version du CIDOC CRM traduite (la plus récente mentionnée),
  - la nature de l'objet traduit (entité, propriété),
  - d'autres informations ayant fait l'objet d'une étiquette au cours du processus de mise en place de la plateforme de traduction.
  
