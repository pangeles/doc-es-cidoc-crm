// open/close Accordion sidebar parts
function switchSidebarAccordionPart(element_menu) {
  var x = document.getElementById(element_menu);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
    x.previousElementSibling.className += " w3-blue";
  } else { 
    x.className = x.className.replace(" w3-show", "");
    x.previousElementSibling.className = 
    x.previousElementSibling.className.replace(" w3-blue", "");
  }
}

// open docementation part after loading page
//switchSidebarAccordionPart('doc');
