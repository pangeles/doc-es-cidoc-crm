#!/bin/bash
issues_URL="https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/issues/"

# retrieve files list to process
files=(../properties/p*.md)
echo 'Conversions des propriétés en cours… patientez !'
# loop for each file
for file in "${files[@]}"; do
	filename_with_xml_extension="${file%.md}.xml"
	#echo "$filename_with_xml_extension" 
	sed -f ./toc-md-to-xml-property.sed "$file" > "$filename_with_xml_extension" >> "$filename_with_xml_extension"
	# retrieve property number 
	property_number=$(grep -rn "property id" $filename_with_xml_extension | awk -F'"' '{print $2}')
	# retrieve property gitlab issue number (#)
	issue_number=$(grep -rn "$property_number " ../stats/issues_flow_metadata/selected_metadata_issues | awk -F: '{print $2}')
	# retrieve issue status
	issue_status=$(grep -rn "$property_number " ../stats/issues_flow_metadata/selected_metadata_issues | awk -F: '{print $3}')
	echo "\
	<customElements>
		<translationStatus type=\"enum\">
			<label xml:lang=\"fr\">Status</label>
			<values>
				<label id=\"proposal\" xml:lang=\"fr\">$issue_status</label>
			</values>
		</translationStatus>
		<translationNoteURL type=\"link\">
			<label xml:lang=\"fr\"><![CDATA[<a href=\"$issues_URL$issue_number\">#$issue_number</a>]]></label>
		</translationNoteURL>
	</customElements>
</property>
	" >> $filename_with_xml_extension
done

# check created xml to avoid errors
xml_error=$(xmllint --noout ../properties/p???.xml)
if [ $? != 0 ]; then
	echo "les fichiers XML des propriétés comportent des erreurs, corrigez-les avant de relancer la construction XML"
	exit
fi
