# les commentaires expliquants chaque ligne sont dans le fichier toc-md-to-xml-entity-comments
/^>.*/d;
/^#/s/_/ /g;
/^$/d;
s/^# \(E[0-9]\+\)$/<class id="\1">/;
s/^# \(E[0-9]\+\) \(.*\)/<class id="\1">\n\t<className xml:lang="fr">\2<\/className>/;
/^| *| *|/d;
/^|[ -]*|[ -]*|/d;
/^| S.*/d;
#/^| Note.d['’]application[  ]*: *|[ ]*/d;
s/^| Note.d['’]application[  ]*: *|[ ]*\(.*\) */\t<scopeNote xml:lang="fr"><![CDATA[<p>\1<\/p>]]><\/scopeNote>/g;
/scopeNote/s/<br>/<\/p><p>/g;
#/^| Exemples[  ]*: *|[ ]*|*/d;
s/^| Exemples[  ]*: *|[ ]*\(.\+\) */\t<examples xml:lang="fr"><![CDATA[<ul><li>\1<\/li><\/ul>]]><\/examples>/g;
/examples xml/s/<br>/<\/li><li>/g;
/^| Logique du premier ordre.*/d;
/^| Propriétés.*/d;
s/|//g;
