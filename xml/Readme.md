# Create XML file from markdown sources
initial author : @bdavid
associated gitlab project issue : #289

## Convert markdown to XML
As our source files are in markdown, to generate an XML file, we need to perform a conversion.

The markdown file doesn't have the tags, which is probably why pandoc can't convert to xml.

I've therefore decided to use the linux sed command, already used by the continuous integration process (so to integrate it for an automated update of xml interchange file).

## Files and initial process to do it
I've added this xml directory. toc prefix name toc mean _table of conversions_

- then defined a file `toc-md-to-xml.sed` and its counterpart with comments `toc-md_to-xml-comment` to convert entities
- `toc-md-to-xml-property.sed` for properties
- `toc-md-to-xml-texts.sed` for others texts like introduction, terminology…

To test, we should run (under GNU/Linux CLI) : from xml folder : ```sed -f toc-md-to-xml.sed ../entities/e53.md```

To generate all xml files, there are three associated bash scripts which required xmllint (apt install libxml2-utils), and pandoc (apt install pandoc) for texts conversions :
- md-to-xml-conversions-entities.sh
- md-to-xml-conversions-properties.sh
- md-to-xml-conversions-texts.sh

for texts, it also rename file to title user in full xml_template
xmllint is used to check XML created files are correctly writed

for test, you can run it individualy, first go in xml folder before launch `bash md-to-xml….sh` or `./md-to-xml….sh`

The last script is `build-zip.sh` to create a zip file to group all xml files. It contains call (`source`) to four previous script !

## Issues
 - To support UTF-8, xml file should begin with line `<?xml version='1.0' encoding='utf-8'?>` # since it will be transcripted in Elias template, is it necessary to write this in xml converted md fragments ?
 - As shown in the Elias model, the lines are placed between html tags so that these tags are not taken into account by the xml parser; they must be encoded (while waiting to use [CDATA](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/issues/289#note_80591) XML balise) : 
   - <p>` -> `&lt;p&gt;` and `</p>` -> `&lt;/p&gt;`, 
   - idem for html tags `<ul>`, `<li`>. 
   - For any `<` and `>` characters in the text, they must also be coded, so that they are not taken into account by either the xml parser or the html parser... which gives for one of E53's examples : `Here -> <-` -> `Here -&amp;gt; &amp;lt;-`

