#!/bin/bash
issues_URL="https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/issues/"

# retrieve files list to process
files=(../entities/e*.md)
echo 'Conversions des entités en cours… patientez !'
# loop for each file
for file in "${files[@]}"; do
	filename_with_xml_extension="${file%.md}.xml"
	#echo "$filename_with_xml_extension" 
	sed -f ./toc-md-to-xml-entity.sed "$file" > "$filename_with_xml_extension" 
	# retrieve entity number 
	entity_number=$(grep -rn "class id" $filename_with_xml_extension | awk -F'"' '{print $2}')
	# retrieve entity gitlab issue number (#)
	issue_number=$(grep -rn "$entity_number " ../stats/issues_flow_metadata/selected_metadata_issues | awk -F: '{print $2}')
	# retrieve issue status
	issue_status=$(grep -rn "$entity_number " ../stats/issues_flow_metadata/selected_metadata_issues | awk -F: '{print $3}')
	echo "\
	<customElements>
		<translationStatus type=\"enum\">
			<label xml:lang=\"fr\">Status</label>
			<values>
				<label id=\"proposal\" xml:lang=\"fr\">$issue_status</label>
			</values>
		</translationStatus>
		<translationNoteURL type=\"link\">
			<label xml:lang=\"fr\"><![CDATA[<a href=\"$issues_URL$issue_number\">#$issue_number</a>]]></label>
		</translationNoteURL>
	</customElements>
</class>
	" >> $filename_with_xml_extension
done

# check created xml to avoid errors
xml_error=$(xmllint --noout ../entities/e??.xml)
if [ $? != 0 ]; then
	echo "les fichiers XML des entités comportent des erreurs, corrigez-les avant de relancer la construction XML"
	exit
fi

