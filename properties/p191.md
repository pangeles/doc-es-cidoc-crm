> # P191 had duration (was duration of)

> | | |
> | --- | --- |
> | Domain: | E52 Time-Span
> | Range: | E54_Dimension
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | one to one (1,1:1,1)
> | Scope note: | This property describes the length of time covered by an instance of E52 Time-Span. It allows an instance of E52 Time-Span to be associated with an instance of E54_Dimension representing duration independent from the actual beginning and end. Indeterminacy of the duration value can be expressed by assigning a numerical interval to the property P90 has value of E54_Dimension.
> | Examples: |     • the time span of the Battle of Issos 333 B.C.E. (E52) had duration Battle of Issos duration (E54) (Howard, 2012)
> | In First Order Logic: | P191(x,y) ⇒ E52(x)<br>P191(x,y) ⇒ E54(y)
> | Properties: |

# P191_a_eu_pour_durée_(était_la_durée_de)

| | |
|---|---|
| Domaine | E52_Laps_de_temps
| Co-domaine | E54_Dimension
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification | un-à-un (1,1:1,1)
| Note d'application : | Cette propriété décrit la longueur de temps couverte par une instance de E52_Laps_de_temps. Elle permet à une instance de E52_Laps_de_temps d’être associée avec une instance de E54_Dimension représentant la durée indépendamment du début ou de la fin réels. Le flou de la valeur de durée peut être exprimée en assignant un intervalle numérique à la propriété P90 has valeur de : E54_Dimension.
| Exemples : | le laps de temps de la Bataille d’Issos 333 avant J.-C.(E52) a eu pour durée la durée de la Battle d’Issos(E54) (Howard, 2012)
| Logique du premier ordre: |  |
| Propriétés: | P191(x,y) ⇒ E52(x)<br>P191(x,y) ⇒ E54(y)

