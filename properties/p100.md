> # P100 was death of (died in)

> | | |
> | --- | --- |
> | Domain: | E69 Death
> | Range: | E21 Person
> | Subproperty of: | E64 End of Existence. P93 took out of existence (was taken out of existence by): E77 Persistent Item
> | Superproperty of: |
> | Quantification: | one to many, necessary (1,n:0,1)
> | Scope note: | This property links an E69 instance of E69 Death event to the instance of E21 Person that died.<br>An instance of E69 Death may involve multiple people, for example in the case of a battle or disaster.<br>This is not intended for use with general natural history material, only people.
> | Examples: |     • Mozart’s death (E69) was death of Mozart (E21) (Sitwell, 2017)
> | In First Order Logic: | P100(x,y) ⇒ E69(x)<br>P100(x,y) ⇒ E21(y)<br>P100(x,y) ⇒ P93(x,y)
> | Properties: |

# P100_a_été_la_mort_de_(est_mort_par)

| | |
|---|---|
| Domaine | `E69_Mort`
| Co-domaine | `E21_Personne`
| Sous-propriété de | `E64_Fin_d’existence`. `P93_a_mis_fin_à_l'existence_de_(a_cessé_d'exister_par)` : `E77_Entité_persistante`
| Super-propriété de |  |
| Quantification | un-à-plusieurs, nécessaire (1,n:0,1)
| Note d'application : | Cette propriété lie une instance d’un événement `E69_Mort` à l’instance de `E21_Personne` qui est morte.<br>Une instance de E69 peut impliquer plusieurs personnes, par exemple dans le cas d’une bataille ou d’une catastrophe.<br>Cette propriété n’a pas vocation à être utilisée pour le matériau d’histoire générale, mais seulement pour des gens.
| Exemples : | La mort de Mozart (E69) _a été la mort de_ (P100) Mozart (E21)(Sitwell, 2017)
| Logique du premier ordre: | P100(x,y) ⇒ E69(x)<br>P100(x,y) ⇒ E21(y)<br>P100(x,y) ⇒ P93(x,y)
| Propriétés: |  |

