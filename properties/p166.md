> # P166 was a presence of (had presence)

> | | |
> | --- | --- |
> | Domain: | E93 Presence
> | Range: | E92 Spacetime Volume
> | Subproperty of: | E92 Spacetime Volume. P10 falls within (contains): E92 Spacetime Volume
> | Superproperty of: |
> | Quantification: | many to one, necessary (1,1: 0,n) 
> | Scope note: | This property associates an instance of E93 Presence with the instance of E92 Spacetime Volume of which it represents a temporal restriction (i.e.: a time-slice). Instantiating this property constitutes a necessary part of the identity of the respective instance of E93 Presence.    
> | Examples: |     • The Roman Empire on 19 August AD 14 (E93) was a presence of The Roman Empire (E4) (Clare, 1992)
> | In First Order Logic: | P166(x,y) ⇒ E93(x),<br>P166(x,y) ⇒ E92(y),<br>P166(x,y) ⇒ P10(x,y)	
> | Properties: |

# P166

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

