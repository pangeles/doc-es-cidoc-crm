> # P189 approximates (is approximated by) 

> | | |
> | --- | --- |
> | Domain: | E53 Place
> | Range: | E53 Place
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: |  many to many (0,n:0,n)
> | Scope note: | This property associates an instance of E53 Place with another instance of E53 Place, which is defined in the same reference space, and which is used to approximate the former. The property does not necessarily state the quality or accuracy of this approximation, but rather indicates the use of the first instance of place to approximate the second.<br>In common documentation practice, find or encounter spots e.g., in archaeology, botany or zoology are often related to the closest village, river or other named place without detailing the relation, e.g., if it is located within the village or in a certain distance of the specified place. In this case the stated “phenomenal” place found in the documentation can be seen as approximation of the actual encounter spot without more specific knowledge.<br>In more recent documentation often point coordinate information is provided that originates from GPS measurements or georeferencing from a map. This point coordinate information does not state the actual place of the encounter spot but tries to approximate it with a “declarative” place. The accuracy depends on the methodology used when creating the coordinates. It may be dependent on technical limitations like GPS accuracy but also on the method where the GPS location is taken in relation to the measured feature. If the methodology is known a maximum deviation from the measured point can be calculated and the encounter spot or feature may be related to the resulting circle using an instance of P171 at some place within.<br>This property is not transitive.
> | Examples: |     • [40°31'17.9"N 21°15'48.3"E] approximates Kastoria, Greece, TGN ID: 7010880. (coordinates from https://sws.geonames.org/735927)<br>• [40°31'00.1"N 21°16'00.1"E] approximates Kastoria, Greece, TGN ID: 7010880. (coordinates from http://vocab.getty.edu/page/tgn/7010880)<br>• [40°04'60.0"N 22°21'00.0"E] approximates Mount Olympus National Park, Greece (coordinates from https://www.geonames.org/6941814) 
> | In First Order Logic: |<br>P189(x,y) ⇒ E53(x)<br>P189(x,y) ⇒ E53 (y)<br>P189 (x,y,z) ⇒ [P189(x,y) ∧ E55(z)]
> | Properties: | P189.1 has type: E55_Type

# P189

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

