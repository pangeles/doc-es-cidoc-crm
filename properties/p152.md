> # P152 has parent (is parent of)

> | | |
> | --- | --- |
> | Domain: | E21 Person
> | Range: | E21 Person
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | many to many, necessary (2,n:0,n)
> | Scope note: | This property associates an instance of E21 Person with another instance of E21 Person who plays the role of the first instance’s parent, regardless of whether the relationship is biological parenthood, assumed or pretended biological parenthood or an equivalent legal status of rights and obligations obtained by a social or legal act. <br>This property is, among others, a shortcut of the fully developed paths from E21 Person through P98i was born, E67 Birth, P96 by mother to E21 Person, and from E21 Person through P98i was born, E67 Birth, P97 from father to E21 Person.<br>This property is not transitive. 
> | Examples: |     • Gaius Octavius (E21) has parent Julius Caesar (E21) (Bleicken, 2015)<br>• Steve Jobs (E21) has parent Joanne Simpson [biological mother] (E21) (Isaacson, 2011)<br>• Steve Jobs (E21) has parent Clara Jobs [adoption mother] (E21) (Isaacson, 2011)
> | In First Order Logic: | P152(x,y) ⇒ E21(x)<br>P152(x,y) ⇒ E21(y)<br>P152(x,y) ⇐ (∃z) [E67(z) ˄ P98i(x,z) ˄ P96(z,y)]<br>P152(x,y) ⇐ (∃z) [E67(z) ˄ P98i(x,z) ˄ P97(z,y)]
> | Properties: |

# P152_a_pour_parent_(est_parent_de)

| | |
|---|---|
| Domaine | E21_Personne |
| Co-domaine | E21_Personne |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification | plusieurs-à-plusieurs, nécessaire (2,n:0,n) |
| Note d’application : | Cette propriété associe une instance de E21_Personne à une autre instance de E21_Personne qui joue le rôle de parent de la première instance, que la relation soit une parenté biologique, une parenté biologique supposée ou prétendue ou un statut juridique équivalent de droits et d’obligations obtenu par un acte social ou juridique.<br>Cette propriété est, entre autres, un raccourci des chemins pleinement développés de E21_Personne à travers P98i_est_né·e, E67_Naissance, P96_de_la_mère jusqu'à E21_Personne, et de E21_Personne à travers P98i_est_né·e, E67_Naissance, P97_du_père jusqu'à E21_Personne.<br>Cette propriété n’est pas transitive.  |
| Exemples : | Gaius Octavius (E21) a pour parent Julius Caesar (E21) (Bleicken, 2015)<br>Steve Jobs (E21) a pour parent Joanne Simpson [mère biologique] (E21) (Isaacson, 2011)<br>Steve Jobs (E21) a pour parent Clara Jobs [mère d’adoption] (E21) (Isaacson, 2011) |
| Logique du premier ordre: | P152(x,y) ⇒ E21(x)<br>P152(x,y) ⇒ E21(y)<br>P152(x,y) ⇐ (∃z) [E67(z) ˄ P98i(x,z) ˄ P96(z,y)]<br>P152(x,y) ⇐ (∃z) [E67(z) ˄ P98i(x,z) ˄ P97(z,y)] |
| Propriétés: |  |

