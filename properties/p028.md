> # P28 custody surrendered by (surrendered custody through)

> | | |
> | --- | --- |
> | Domain: |		E10 Transfer of Custody
> | Range: |		E39 Actor
> | Subproperty of: | 	E7 Activity. P14 carried out by (performed): E39 Actor
> | Superproperty of: |
> | Quantification: |	many to many (0,n:0,n)
> | Scope note: |	This property identifies the instance(s) of E39 Actor who surrender custody of an instance of E18 Physical Thing in an instance of E10 Transfer of Custody.<br>The property will typically describe an Actor surrendering custody of an object when it is handed over to someone else’s care. On occasion, physical custody may be surrendered involuntarily – through accident, loss or theft.<br>In reality, custody is either transferred to someone or from someone, or both.
> | Examples: |	<br>the Secure Deliveries Inc. crew (E74) surrendered custody through The delivery of the paintings by Secure Deliveries Inc. to the National Gallery (E10).
> | In First Order Logic: |<br>P28(x,y) ⊃ E10(x)<br>P28(x,y) ⊃ E39(y)<br>P28(x,y) ⊃ P14(x,y)
> | Properties: |

# P28_a_retiré_la_responsabilité_à_(a_perdu_la_responsabilité_du_fait_de)

| | |
|---|---|
| Domaine | E10 Transfert de responsabilité |
| Co-domaine | E39_Acteur·rice·x |
| Sous-propriété de | E7 Activité. P14 a été effectué par (a effectué) : E39 Acteur·rice·x |
| Super-propriété de |  |
| Quantification | plusieurs-à-plusieurs (0,n,0,n) |
| Note d'application : | Cette propriété identifie la ou les instance(s) de E39_Acteur·rice·x qui renonce à la responsabilité d’une instance de E18_Chose_matérielle lors une instance de E10_Transfert_de_la_responsabilité.<br>De manière générale, la propriété décrira un·e·x acteur·rice·x qui cède la responsabilité d'un objet lorsqu'il est remis sous la responsabilité de quelqu'un d'autre. La responsabilité physique peut parfois être abandonnée de manière involontaire - par accident, perte ou vol.<br>En réalité, la responsabilité est transférée à quelqu’un ou de quelqu’un, ou les deux. |
| Exemples : |<br>L’équipe du Secure Deliveries Inc. (E74) a perdu la responsabilité du fait de la livraison de peintures par Secure Deliveries Inc. à la National Gallery (E10). |
| Logique du premier ordre: |<br>P28(x,y) ⇒  E10(x)<br>P28(x,y) ⇒  E39(y) <br>P28(x,y) ⇒  P14(x,y) |
| Propriétés: |  |

