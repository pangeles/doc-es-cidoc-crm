> # P125 used object of type (was type of object used in)

> | | |
> | --- | --- |
> | Domain: | E7 Activity
> | Range: | E55_Type
> | Subproperty of: |
> | Superproperty of: | E7 Activity. P32 used general technique (was technique of): E55_Type
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property associates an instance of E7 Activity to an instance of E55_Type, which defines used in an instance of E7 Activity, when the specific instance is either unknown or not of interest, such as use of "a hammer".
> | Examples: |     • at the Battle of Agincourt (E7), the English archers used object of type long bow (E55) (Curry, 2015)
> | In First Order Logic: | P125(x,y) ⇒ E7(x)<br>P125(x,y) ⇒ E55(y)<br>P125(x,y) ⇔ (∃z) [E70(z) ∧ P16(x,z) ∧  P2(z,y)]
> | Properties: |

# P125_a_mobilisé_un_objet_du_type_(a_été_le_type_d'objet_employé_pour)

| | |
|---|---|
| Domaine | E7_Activité |
| Co-domaine | E55_Type |
| Sous-propriété de |  |
| Super-propriété de |E7_Activité. P32 a mobilisé comme technique générique (a été la technique mise en œuvre dans) : E55_Type |
| Quantification |plusieurs-à-plusieurs (0,n,0,n)  |
| Note d'application : | Cette propriété associe une instance de E7_Activité à une instance de E55_Type qui classifie une instance de E70_Chose mobilisée dans une instance de E7_Activité, lorsque l’instance spécifique est soit inconnue soit sans intérêt, comme dans le cas d’un marteau.<br>Cette propriété est un raccourci pour le chemin plus développé de E7_Activité par P16 a mobilisé comme objet spécifique, E70_Chose, P2_a_pour_type_(est_le_type_de), à E55_Type.  |
| Exemples : |l’activité des archers anglais à la bataille d’Azincourt (E7) a mobilisé un objet du type arc long anglais (E55). (Curry, 2015)  |
| Logique du premier ordre: | P125(x,y) ⇒ E7(x), P125(x,y) ⇒ E55(y), P125(x,y) ⇔ (∃z) [E70(z) ∧ P16(x,z) ∧ P2(z,y)] |
| Propriétés: |  |
