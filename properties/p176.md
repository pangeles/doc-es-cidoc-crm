> # P176 starts before the start of (starts after the start of)

> | | |
> | --- | --- |
> | Domain: | E2 Temporal Entity
> | Range: | E2 Temporal Entity
> | Subproperty of: | E2 Temporal Entity. P175 starts before or with the start of (starts after or with the start of): E2 Temporal Entity 
> | Superproperty of: | E2 Temporal Entity. P182 ends before or at the start of (starts after or with the end of): E2 Temporal Entity
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property specifies that the temporal extent of the domain instance A of E2 Temporal Entity starts definitely before the start of the temporal extent of the range instance B of E2 Temporal Entity.<br>In other words, if A = [Astart, Aend] and B = [Bstart, Bend], we mean Astart < Bstart is true.<br>This property is part of the set of temporal primitives P173 – P176, P182 – P185.<br>This property corresponds to a disjunction (logical OR) of the following Allen temporal relations [Allen, 1983]: {before, meets, overlaps, contains, finished-by}. This property is transitive.<br>Figure 14: Temporal entity A starts before the start of temporal entity B. Here A is longer than B<br>Figure 15: Temporal entity A starts before the start of temporal entity B. Here A is shorter than B 
> | In First Order Logic: |<br>P176(x,y) ⇒ E2(x)<br>P176(x,y) ⇒ E2(y)<br>P176(x,y) ⇒  P175(x,y)<br>[P176(x,y) ∧ P176(y,z)] ⇒ P176(x,z)
> | Properties: |

# P176

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

