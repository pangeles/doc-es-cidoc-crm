> # P172 contains 

> | | |
> | --- | --- |
> | Domain: | E53 Place
> | Range: | E94 Space Primitive
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property describes a minimum spatial extent which is contained within an instance of E53 Place. Since instances of E53 Place may not have precisely known spatial extents, the CIDOC CRM supports statements about minimum spatial extents of instances of E53 Place. This property allows an instance of E53 Places’s minimum spatial extent (i.e., its inner boundary or a point being within a Place) to be assigned an instance of E94 Space Primitive value.<br>This property is a shortcut of the fully developed path:  E53 Place, P89i contains, E53 Place, P168 place is defined by, E94 Space Primitive
> | Examples: |     • the spatial extent of the Acropolis of Athens (E53) contains POINT (37.971431 23.725947) (E94)
> | In First Order Logic: | P172(x,y) ⇒ E53(x)<br>P172(x,y) ⇒ E94(y)<br>P172(x,y) ⇐ (∃z) [E53(z) ˄ P89i(x,z) ˄ P168(z,y)]
> | Properties: |

# P172

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

