> # P130 shows features of (features are also found on)

> | | |
> | --- | --- |
> | Domain: | E70 Thing
> | Range: | E70 Thing
> | Subproperty of: |
> | Superproperty of: | E33 Linguistic Object. P73i is translation of: E33 Linguistic Object <br>E18 Physical Thing. P128 carries (is carried by): E90 Symbolic Object
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property generalises the notions of "copy of" and "similar to" into a directed relationship, where the domain expresses the derivative or influenced item and the range the source or influencing item, if such a direction can be established. The property can also be used to express similarity in cases that can be stated between two objects only, without historical knowledge about its reasons. The property expresses a symmetric relationship in case no direction of influence can be established either from evidence on the item itself or from historical knowledge. This holds in particular for siblings of a derivation process from a common source or non-causal cultural parallels, such as some weaving patterns.<br>The P130.1 kind of similarity property of the P130 shows features of (features are also found on) property enables the relationship between the domain and the range to be further clarified, in the sense from domain to range, if applicable. For example, it may be expressed if both items are product “of the same mould”, or if two texts “contain identical paragraphs”.<br>If the reason for similarity is a sort of derivation process, i.e., that the creator has used or had in mind the form of a particular thing during the creation or production, this process should be explicitly modelled. In these cases, P130 shows features of can be regarded as a shortcut of such a process. However, the current model does not contain any path specific enough to infer this property. Specializations of the CIDOC CRM may however be more explicit, for instance describing the use of moulds etc.<br>This property is not transitive.
> | Examples: |     • Mary Lamb’s Cymbeline [from Charles and Mary Lamb’s Tales from Shakespeare] shows features of William Shakespeare’s Cymbeline (Carrington, 1954)<br>• The audio recording of Dante Alighieri's La divina commedia read by Enrico de Negri shows features of the text of Dante Alighieri's La divina commedia (Alighieri, 1956)
> | In First Order Logic: | P130(x,y) ⇒ E70(x)<br>P130(x,y) ⇒ E70(y)<br>P130(x,y,z) ⇒ [P130(x,y) ∧ E55(z)]
> | Properties: | P130.1 kind of similarity: E55_Type

# P130

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

