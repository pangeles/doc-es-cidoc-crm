> # P187 has production plan (is production plan for)

> | | |
> | --- | --- |
> | Domain: | E99 Product Type
> | Range: | E29 Design or Procedure
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | one to many (1,n:1,1)
> | Scope note: | This property associates an instance of E99 Product Type with an instance of E29 Design or Procedure that completely determines the production of instances of E18 Physical Thing. The resulting instances of E18 Physical Thing are considered exemplars of this instance of E99 Product Type when the process specified is correctly executed. Note that the respective instance of E29 Design or Procedure may not necessarily be fixed in a written/graphical form, and may require the use of tools or models unique to the product type. The same instance of E99 Product Type may be associated with several variant plans.
> | Examples: |     • Volkswagen Type 11 (Beetle) (E99) has production plan the production plans for Volkswagen Type 11 (Beetle) (E29). (Rieger, 2013)
> | In First Order Logic: | P187(x,y) ⇒ E99(x)<br>P187(x,y) ⇒ E29(y)
> | Properties: |

# P187_a_pour_plan_de_production_(est_plan_de_production_de)

| | |
|---|---|
| Domaine | E99_Type_de_produit  |
| Co-domaine | E29_Projet_ou_Procédure |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification | un-à-plusieurs (1,n:1,1) |
| Note d'application : | Cette propriété associe une instance de E99_Type_de_produit à une instance de E29_Projet_ou_Procédure qui détermine complètement la production d’instances de E18_Chose_matérielle. Les instances de E18_Chose_matérielle en résultant sont considérées comme des exemplaires de cette instance de E99_Type_de_produit quand l’opération spécifiée a été correctement exécutée. Remarquez que l’instance de E29_Projet_ou_Procédure en question peut ne pas avoir été notée de manière graphique/écrite, et peut requérir l’utilisation d’outils ou de modèles uniques à ce type de produit. La même instance de E99_Type_de_produit peut être associée à plusieurs plans différents. |
| Exemples : | la Volkswagen type 11 (Coccinelle) (E99) a pour plan de production les plans de production pour la Volkswagen type 11 (Coccinelle) (E29) (Rieger, 2013) |
| Logique du premier ordre: | P187(x,y) ⇒ E99(x)<br>P187(x,y) ⇒ E29(y) |
| Propriétés: | 
