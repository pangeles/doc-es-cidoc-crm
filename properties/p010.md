> # P10 falls within (contains)

> | | |
> | --- | --- |
> | Domain: |		E92 Spacetime Volume
> | Range: |		E92 Spacetime Volume
> | Subproperty of: |    E92 Spacetime Volume. P132  spatiotemporally overlaps with.:E92 Spacetime Volume
> | Superproperty of: | E93 Presence. P166 was a presence of (had presence): E92 Spacetime Volume
> | Quantification: |	many to many (0,n:0,n)
> | Scope note: |	This property associates an instance of E92 Spacetime Volume with another instance of E92 Spacetime Volume that falls within the latter. In other words, all points in the former are also points in the latter.<br>This property is transitive.
> | Examples: |	<br>the Great Plague (E4) falls within The Gothic period (E4)
> | In First Order Logic: |<br>P10(x,y) ⊃ E92(x)<br>P10(x,y) ⊃ E92(y)<br>P10(x,y) ⊃ P132(x,y)
> | Properties: |

# P10

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

