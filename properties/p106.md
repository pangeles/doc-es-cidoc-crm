> # P106 is composed of (forms part of)

> | | |
> | --- | --- |
> | Domain: | E90 Symbolic Object
> | Range: | E90 Symbolic Object
> | Subproperty of: |
> | Superproperty of: | E73 Information Object. P165 incorporates (is incorporated in): E90 Symbolic Object
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property associates an instance of E90 Symbolic Object with a part of it that is by itself an instance of E90 Symbolic Object, such as fragments of texts or clippings from an image.<br>This property is transitive and non-reflexive.
> | Examples: |     • This Scope note P106 (E33) is composed of ‘fragments of texts’ (E33)<br>• ‘recognizable’ P106 (E90) is composed of ‘ecognizabl’ (E90)
> | In First Order Logic: | P106(x,y) ⇒ E90(x)<br>P106(x,y) ⇒ E90(y)<br>[P106(x,y) ∧ P106(y,z)] ⇒ P106(x,z)<br>¬P106(x,x)
> | Properties: |


# P106_est_composé_de_(fait_partie_de)

| | |
|---|---|
| Domaine | E90_Objet_symbolique
| Co-domaine | E90_Objet_symbolique
| Sous-propriété de |  |
| Super-propriété de | E73_Objet_informationnel. P165_incorpore_(est_incorporé·e_dans) : E90_Objet_symbolique
| Quantification | plusieurs-à-plusieurs (0,n:0,n)
| Note d'application : | Cette propriété associe une instance de E90_Objet_symbolique avec une de ses parties qui est elle-même une instance de E90_Objet_symbolique, tels que les fragments de texte ou d’image.<br>Cette propriété est transitive et non-réflexive.
| Exemples : | La note d’application de P106 (E33) est composée de ‘fragments de textes’ (E33).<br>‘reconnaissable’ P106 (E90) est composé de ‘econnaissabl’ (E90).
| Logique du premier ordre: | P106(x,y) ⇒ E90(x)<br>P106(x,y) ⇒ E90(y)<br>[P106(x,y) ∧ P106(y,z)] ⇒ P106(x,z)<br>¬P106(x,x)
| Propriétés: |  |
