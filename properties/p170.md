> # P170 defines time (time is defined by)

> | | |
> | --- | --- |
> | Domain: | E61Time Primitive 
> | Range: | E52 Time Span 
> | Subproperty of: | E41_Appellation. P1i identifies: E1 CRM Entity
> | Superproperty of: |
> | Quantification: |  many to one (0,1:0,n) 
> | Scope note: | This property associates an instance of E61 Time Primitive with the instance of E52 Time-Span that constitutes the interpretation of the terms of the time primitive as an extent in absolute, real time.<br>The quantification allows several instances of E61 Time Primitive that are each expressed in different syntactic forms, to define the same instance of E52 Time Span.
> | Examples: |     • (1800/1/1 0:00:00 – 1899/31/12 23:59:59) (E61) defines time The 19th century (E52)<br>• (1968/1/1 – 2018/1/1) (E61) defines time “1968/1/1 – 2018/1/1” (E52) [an arbitrary time-span during which the Saint Titus reliquary was present in the Saint Titus Church in Heraklion, Crete]
> | In First Order Logic: | P170(x,y) ⇒ E61(x)<br>P170(x,y) ⇒ E52(y)<br>P170(x, y) ⇒ P81i(x, y) ∧ P82i(x, y)
> | Properties: |

# P170

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

