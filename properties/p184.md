> # P184 ends before or with the end of (ends with or after the end of) 

> | | |
> | --- | --- |
> | Domain: | E2 Temporal Entity
> | Range: | E2 Temporal Entity
> | Subproperty of: | E2 Temporal Entity. P174 starts before the end of (ends after the start of): E2 Temporal Entity
> | Superproperty of: | E2 Temporal Entity. P185 ends before the end of (ends after the end of): E2 Temporal Entity
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property specifies that the temporal extent of the domain instance A of E2 Temporal Entity ends before or simultaneously with the end of the temporal extent of the range instance B of E2 Temporal Entity.<br>In other words, if A = [Astart, Aend] and B = [Bstart, Bend], we mean Aend ≤ Bend   is true.<br>This property is part of the set of temporal primitives P173 – P176, P182 – P185.<br>This property corresponds to a disjunction (logical OR) of the following Allen temporal relations [Allen, 1983]: {before, meets, overlaps, finished by, start, equals, during, finishes}<br>Figure 20: Temporal entity A ends before or with the end of temporal entity B. Here A is longer than B<br>Figure 21: Temporal entity A ends before or with the end of temporal entity B. Here A is shorter than B
> | Example: |
> | In First Order Logic: | P184(x,y) ⇒ E2(x)<br>P184(x,y) ⇒ E2(y)<br>P184(x,y) ⇒ P174(x,y)
> | Properties: |

# P184

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

