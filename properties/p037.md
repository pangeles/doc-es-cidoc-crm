> # P37 assigned (was assigned by)

> | | |
> | --- | --- |
> | Domain: |		E15 Identifier Assignment
> | Range: |		E42 Identifier
> | Subproperty of: | 	E13 Attribute Assignment. P141 assigned (was assigned by): E1 CRM Entity
> | Superproperty of: |
> | Quantification: |		many to many (0,n:0,n)
> | Scope note: |	This property records the identifier that was assigned to an item in an instance of P37 Identifier Assignment.<br>The same identifier may be assigned on more than one occasion.<br>An Identifier might be created prior to an assignment.
> | Examples: |	<br>01 June 1997 Identifier Assignment of the silver cup donated by Martin Doerr (E15) assigned “232” (E42)
> | In First Order Logic: |<br>P37(x,y) ⊃ E15(x)<br>P37(x,y) ⊃ E42(y)<br>P37(x,y) ⊃ P141(x,y)
> | Properties: |

# P37_a_attribué_(a_été_attribué_par)

| | |
|---|---|
| Domaine :| E15_Attribution_d’identifiant
| Co-domaine :| E42_Identifiant
| Sous-propriété de :| E13_Affectation_d’un_attribut. P141_a_attribué_(a_été_attribué_par) : E1_Entité_CRM
| Super-propriété de :|
| Quantification :| plusieurs-à-plusieurs (0,n,0,n)
| Note d'application : | Cette propriété enregistre l’identifiant qui a été attribué à une chose lors d’une instance de E15_Attribution_d’identifiant.<br>Le même identifiant peut être attribué à plusieurs occasions.<br>Un identifiant peut être créé avant son attribution.
| Exemples : |<br>• L’attribution d’identifiant le 1er juin 1997 à la coupe d’argent donnée par Marin Doerr (E15) a attribué « 232 » (E42) (exemple fictif)
| Logique du premier ordre: | P37(x,y) ⇒ E15(x),<br>P37(x,y) ⇒ E42(y),<br>P37(x,y) ⇒ P141(x,y)
| Propriétés: |

