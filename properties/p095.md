> # P95 has formed (was formed by)

> | | |
> | --- | --- |
> | Domain: | E66_Formation
> | Range: | E74 Group
> | Subproperty of: | E63 Beginning of Existence. P92 brought into existence (was brought into existence by): E77 Persistent Item
> | Superproperty of: |
> | Quantification: | one to many, necessary, dependent (1,n:1,1)
> | Scope note: | This property associates the instance of E66_Formation with the instance of  E74 Group that it founded.
> | Examples: |     • the formation of the CIDOC CRM SIG at the August 2000 CIDOC Board meeting (E66) has formed the CIDOC CRM Special Interest Group (E74)
> | In First Order Logic: | P95(x,y) ⇒ E66(x)<br>P95(x,y) ⇒ E74(y)<br>P95(x,y) ⇒ P92(x,y)
> | Properties: |

# P95

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

