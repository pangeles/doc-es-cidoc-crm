> # P17 was motivated by (motivated)

> | | |
> | --- | --- |
> | Domain: |		E7 Activity
> | Range: |		E1 CRM Entity
> | Subproperty of: | 	E7 Activity. P15 was influenced by (influenced): E1 CRM Entity
> | Superproperty of: |
> | Quantification: |	many to many (0,n:0,n)
> | Scope note: |	This property describes an item or items that are regarded as a reason for carrying out the instance of E7 Activity.<br>For example, the discovery of a large hoard of treasure may call for a celebration, an order from head quarters can start a military manoeuvre. 
> | Examples: |	<br>• The resignation of the chief executive (E7) was motivated by the collapse of SwissAir (E68).<br>• The coronation of Elizabeth II (E7) was motivated by the death of George VI (E69) (Strong, 2005)
> | In First Order Logic: |<br>P17(x,y) ⇒ E7(x)<br>P17(x,y) ⇒ E1(y)<br>P17 (x,y) ⇒ P15(x,y)
> | Properties: |

# P17_a_été_motivée_par_(a_motivé)
 
| | |
|---|---|
| Domaine | E7_Activité
| Co-domaine | E1_Entité_CRM
| Sous-propriété de | E7_Activité. P15_a_été_influencé·e_par_(a_influencé) : E1_Entité_CRM
| Super-propriété de | 
| Quantification | plusieurs-à-plusieurs (0,n:0,n)
| Note d'application : | Cette propriété décrit une entité ou des entités qui sont considérées comme étant une raison d'effectuer une instance de E7_Activité.<br>Par exemple, la découverte d’un important trésor peut conduire à l’organisation d’une célébration, un ordre du quartier général peut déclencher une manœuvre militaire.
| Exemples : |<br>• La démission du directeur général (E7) a été motivée par l’effondrement de SwissAir (E68).<br>• Le couronnement d’Elizabeth II (E7) a été motivé par la mort de George VI (E69) (Strong, 2005)
| Logique du premier ordre: |<br>P17(x,y) ⇒ E7(x), P17(x,y) ⇒ E1(y), P17(x,y) ⇒ P15(x,y)
| Propriétés: |
