> # P136 was based on (supported type creation)

> | | |
> | --- | --- |
> | Domain: | E83 Type Creation
> | Range: | E1 CRM Entity
> | Subproperty of: | E7 Activity. P15 was influenced by (influenced): E1 CRM Entity
> | Superproperty of: |
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property identifies one or more instances of E1 CRM Entity that were used as evidence to declare a new instance of E55_Type.<br>The examination of these items is often the only objective way to understand the precise characteristics of a new type. Such items should be deposited in a museum or similar institution for that reason. The taxonomic role renders the specific relationship of each item to the type, such as "holotype" or "original element".
> | Examples: |     • the taxon creation of the plant species ‘Serratula glauca Linné, 1753.’ (E83) was based on Object BM000576251 of the Clayton Herbarium (E20) in the taxonomic role original element (E55) (Blake, 1918)
> | In First Order Logic: | P136(x,y) ⇒ E83(x)<br>P136(x,y) ⇒ E1(y)<br>P136(x,y,z) ⇒ [P136(x,y) ∧ E55(z)]<br>P136(x,y) ⇒ P15(x,y)
> | Properties: | P136.1 in the taxonomic role: E55_Type

# P136

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

