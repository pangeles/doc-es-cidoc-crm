> # P65 shows visual item (is shown by)

> | | |
> | --- | --- |
> | Domain: | E24 Physical Human-Made Thing
> | Range: | E36 Visual Item
> | Subproperty of: | E18 Physical Thing. P128 carries (is carried by): E90 Symbolic Object
> | Superproperty of: |
> | Quantification: |  many to many (0,n:0,n)
> | Scope note: | This property documents an instance of E36 Visual Item shown by an instance of E24 Physical Human-Made Thing.<br>This property is similar to P62 depicts (is depicted by) in that it associates an instance of E24 Physical Human-Made Thing with a visual representation. However, P65 shows visual item (is shown by) differs from the P62 depicts (is depicted by) property in that it makes no claims about what the instance of E36 Visual Item is deemed to represent. An instance of E36 Visual Item identifies a recognisable image or visual symbol, regardless of what this image may or may not represent.<br>For example, all recent British coins bear a portrait of Queen Elizabeth II, a fact that is correctly documented using P62 depicts (is depicted by). Different portraits have been used at different periods, however. P65 shows visual item (is shown by) can be used to refer to a particular portrait.<br>P65 shows visual item (is shown by) may also be used for Visual Items such as signs, marks and symbols, for example the 'Maltese Cross' or the 'copyright symbol’ that have no particular representational content.<br>This property is part of the fully developed path E24 Physical Human-Made Thing, P65 shows visual item, E36 Visual Item, P138 represents, E1 CRM Entity which is shortcut by, P62 depicts (is depicted by).
> | Examples: |     • My T-Shirt (E22) shows visual item Mona Lisa (E36) (fictitious)
> | In First Order Logic: | P65(x,y) ⇒ E24(x)<br>P65(x,y) ⇒ E36(y)<br>P65(x,y) ⇒ P128(x,y)
> | Properties: |

# P65_représente_l’entité_visuelle_(est_représenté·e_par)
 
| | |
|---|---|
| Domaine | E24_Chose_matérielle_anthropique
| Co-domaine | E36_Élément_visuel
| Sous-propriété de | E18_Chose_matérielle. P128_est_support_de_(a_pour_support) : E90_Objet_symbolique
| Super-propriété de |
| Quantification | plusieurs-à-plusieurs(0,n:0,n)
| Note d'application : | Cette propriété documente une instance de E36_Élément_visuel portée par une instance de E24_Chose_matérielle_anthropique.<br>Cette propriété est semblable à P62_dépeint_(est_dépeint·e_par) en ce qu’elle associe une instance de E24_Chose_matérielle_anthropique à une représentation visuelle. Cependant, P65_porte_l’entité_visuelle_(est_l’entité_visuelle_portée_par) diffère de la propriété P62_dépeint_(est_dépeint·e_par) en ce qu'elle n’a aucune prise sur ce que l’instance de E36_Élément_visuel est censée porter. Une instance de E36_Élément_visuel identifie une image ou un symbole visuel reconnaissable, indépendamment de ce que cette image peut ou ne peut pas représenter.<br>Par exemple, toutes les pièces de monnaies anglaises récentes portent un portrait de la reine Elizabeth II, ce qui est correctement documenté par P62_dépeint_(est_dépeint·e_par). Cependant, différents portraits ont été utilisés à différentes périodes. P65_porte_l’entité_visuelle_(est_l’entité_visuelle_portée_par) peut être utilisée pour faire référence à un portrait en particulier.<br>P65_porte_l’entité_visuelle_(est_l’entité_visuelle_portée_par) peut aussi être utilisée pour des entités visuelles telles que des signes, des marques et des symboles : comme par exemple, la Croix de Malte ou le symbole du copyright qui n’ont pas de contenu figuratif particulier.<br>Cette propriété est une partie du chemin plus développé de E24_Chose_matérielle_anthropique, P65_porte_l’entité_visuelle, E36_Élément_visuel, P138_représente E1_Entité_CRM dont le raccourci est P62_dépeint_(est_dépeint·e_par).
| Exemples : | Mon T-Shirt (E22) porte l'entité visuelle Mona Lisa (E36) (exemple fictif)
| Logique du premier ordre: | P65(x,y) ⇒ E24(x)<br>P65(x,y) ⇒ E36(y)<br>P65(x,y) ⇒ P128(x,y)
| Propriétés: |
