> # P108 has produced (was produced by)

> | | |
> | --- | --- |
> | Domain: | E12_Production
> | Range: | E24 Physical Human-Made Thing
> | Subproperty of: | E11_Modification. P31 has modified (was modified by): E18 Physical Thing<br>E63 Beginning of Existence. P92 brought into existence (was brought into existence by): E77 Persistent Item
> | Superproperty of: |
> | Quantification: | one to many, necessary, dependent (1,n:1,1)
> | Scope note: | This property identifies the instance of E24 Physical Human-Made Thing that came into existence as a result of the instance of E12_Production.<br>The identity of an instance of E24 Physical Human-Made Thing is not defined by its matter, but by its existence as a subject of documentation. An E12_Production can result in the creation of multiple instances of E24 Physical Human-Made Thing.
> | Examples: |     • The building of Rome (E12) has produced Τhe Colosseum (E22) (Hopkins, 2011)
> | In First Order Logic: | P108(x,y) ⇒ E12(x)<br>P108(x,y) ⇒ E24(y)<br>P108(x,y) ⇒ P31(x,y)<br>P108(x,y) ⇒ P92(x,y)
> | Properties: |

# P108

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

