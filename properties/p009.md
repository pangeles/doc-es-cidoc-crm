> # P9 consists of (forms part of)

> | | |
> | --- | --- |
> | Domain: |		E4 Period
> | Range: |		E4 Period
> | Subproperty of: | 	E92 Spacetime Volume. P10i contains (falls within):E92 Spacetime Volume<br>E92 Spacetime Volume. P132  spatiotemporally overlaps with:E92 Spacetime Volume
> | Superproperty of: |
> | Quantification: |	one to many, (0,n:0,1)
> | Scope note: |	This property associates an instance of E4 Period with another instance of E4 Period that is defined by a subset of the phenomena that define the former. Therefore the spacetime volume of the latter must fall within the spacetime volume of the former.<br>This property is transitive.
> | Examples: |	<br>Cretan Bronze Age (E4) consists of  Middle Minoan (E4)
> | In First Order Logic: |<br>P9(x,y) ⊃ E4(x)<br>P9(x,y) ⊃ E4(y)<br>P9(x,y) ⊃ P10(y,x)
> | Properties: |

# P9

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

