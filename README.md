# Doc es CIDOC CRM

**[Scroll down for English](#english-informations)**

El objetivo de este repositorio es traducir al español el documento principal del estándar CIDOC CRM. Para ello, partimos del documento CIDOC CRM en su versión [version 7.1.1](http://www.cidoc-crm.org/sites/default/files/cidoc_crm_version_7.1.1.docx) presentada para la solicitud de revisión como estándar ISO, antes de la [version 7.1.2](https://cidoc-crm.org/Version/version-7.1.2)

# ¿Cómo participar?

Invitamos a las personas interesadas a participar en esta iniciativa para lo cual, les pedimos comunicarse con Pedro Ángeles Jiménez (@pangeles) o Edurne Uriarte (@euriarte).<br/>

En caso de algún problema por favor contacte a @pangeles ou @euriarte.<br>

## Plan de trabajo 2024
* Inicio del trabajo adaptando la la experiencia previa de la [traducción al francés de CIDOC CRM](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm): junio de 2023
* Invitación a colaboradores: 29 de febrero de 2024
* Elaboración de plan de trabajo: marzo de 2024
* Participación en la Conferencia CIDOC 2024 con la presentación de un taller donde mostrar los resultados

## Plan de trabajo 2025
* Inicio de trabajo de traducción: Viernes 3 de enero de 2025 
* Primera evaluación de resultados: febrero de 2025, a un año de haber iniciado

## Lista de colaboradores registrados y personas que siguen el proyecto
[Miembros registrados en el Gitlab](https://gitlab.huma-num.fr/pangeles/doc-es-cidoc-crm/-/project_members)

| Nombre       | Email              | Organización    | País     | Rol         | Orcid           |Gitlab
|--------------|--------------------|-----------------|----------|-------------|-------------------|-------------------|
|Pedro Ángeles|pedroa@unam.mx| IIE - UNAM |México |Coordinación, Traducción|https://orcid.org/0000-0002-3315-3615|@pangeles|
| Edurne Uriarte | uyulala.edu@gmail.com   | DGRU - UNAM  | México |Traducción, Documentación|https://orcid.org/0009-0004-1537-4950|@euriarte|
|Alejandro Peña Carbonell|alex@render.es| Dedalo |España |Traducción, arquitectura y diseño tecnológico|https://orcid.org/0000-0002-4987-4708|@penacarbonell|
|Cesar Huiza Romero|wilmerchuizar@gmail.com|Museo de Arqueología y Antropología, UNMSM|Perú|Traducción, Documentación|https://orcid.org/0000-0003-0670-1564|@chuizaromero|
|Lucía Sánchez de Bustamante|lulunamorena@gmail.com|CCT - UNAM|México|Traducción, Documentación|https://orcid.org/0000-0001-7447-846X|@sanchezdebustamante|
|Paula Casajús|paula.casajus@gmail.com, paula.casajus@mnba.gob.ar|Museo Nacional de Bellas Artes|Argentina|Traducción, Documentación|-|-|
|Betsabé Miramontes Vidal|betsa.miramontes@gmail.com|UNAM - IIE|México|Traducción, Documentación|https://orcid.org/0000-0002-9080-9561|-|
|||||||

## Résultat
Ce projet met en œuvre de l'intégration continue, c'est à dire que pour chaque modification enregistrée (`git commit`), des mécanismes automatiques s'exécutent pour créer des pages web… accessibles à l'url : https://cidoc-crm-fr.mom.fr/. <br> Pour plus d'informations, consulter le ticket #5.

## Veille scientifique et bibliographique
Le projet collecte une bibliographie en lien avec CIDOC CRM et les recherches associées. <br>
Pour pariciper à la [bibliothèque de groupe Zotero](https://www.zotero.org/groups/4641239), envoyer un email à anais.guillem@gmail.com.

# English informations
<details><summary><strong>Click this to collapse/fold</strong></summary>
<br><strong>Project initiative</strong><br>
The French speaking research community network's initiative started during DONIPAT CNRS thematic school (2019) organized by MASA Consortium.<br>
The training sessions about CIDOC CRM with the Game led by G. Bruseker and A. Guillem facilitated the appropriation of the ontology by different groups of French researchers.<br>
The need for an updated translation came up as future developments for a more efficient dissemination of the know-how in modelling with CIDOC CRM in different scientific fields within French speaking Digital Humanities (DH) communities.<br>
The objective is to collectively provide an updated translation of the CIDOC CRM following the FAIR principles from the start:
<ul>
<li>(F) Findable: a git public repository on the French-speaking HumaNum infrastructure [Gitlab project repository](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm); a continuous integration of the translation in a [HTML page *[WORK IN PROGRESS]*](https://cidoc-crm-fr.mom.fr).</li>
<li>(A) Accessible: methods are described in the wiki and meeting reports are published after weekly open meetings, a published agenda & translation flows organise translation priorities etc.</li>
<li>(I) Interoperable: a markdown syntax is used for all the translation files, published in html, code is shared  for the translation project.</li>
<li>(R) Reusable: repository is public under licences CC-BY-SA 4.0 International & ODbL v1.0.</li>
</ul>
<strong>The method</strong><br>
Institutionally, the translation initiative is supported by the MASA Consortium but the project is managed collectively by individual researchers representing various cultural or research institutions (see [project participants table](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/blob/translate/README.md)). <br/> Regarding the translation/validation method, translations tracked in the labelled issues board are proposed by individuals and pushed asynchronously into the Gitlab repository. The validation is a synchronous collective process held during weekly meetings, based on logical groups of entities and properties (see, for example [E1 logical group](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/blob/translate/stats/graphE1.md)).<br> In July 2022, more than 60% of entities and properties are translated. So far in July 2022, 28 meetings took place on Fridays 3PM (+1GMT or +2GMT), on Renater [Rendez-vous](https://rendez-vous.renater.fr) platform.<br> 
<br><strong>Intended purpose</strong><br>
The collective translation effort gives the opportunity to create a positive learning environment for deepening the knowledge of the CIDOC CRM by the group of translators. <br>The weekly meetings allow the group to share and discuss scientific updates about CIDOC CRM and related topics, and build a French speaking community of CIDOC CRM expert users.
</details>
